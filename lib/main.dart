import 'package:bank_mobile_client/src/injector.dart';
import 'package:bank_mobile_client/src/screens/main_screen.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  setup();
  runApp(MainScreen());
} 

