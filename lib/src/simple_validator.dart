class SimpleValidator {
  static final RegExp _pinRegExp = RegExp(r'^([0-9]){4}$');

  static isPinValid(String pin) {
    return _pinRegExp.hasMatch(pin);
  }
}