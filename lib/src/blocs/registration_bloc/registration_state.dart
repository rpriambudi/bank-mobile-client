part of 'registration_bloc.dart';

@freezed
abstract class RegistrationState with _$RegistrationState {
  const factory RegistrationState({
    @required bool isLoading,
    @required bool isSuccess,
    @required bool isFailed,
    @required String errorMessage,
    @required int loadingCount,
    Saga saga
  }) = _RegistrationState;

  factory RegistrationState.initial() => RegistrationState(
    isLoading: false, 
    isSuccess: false, 
    isFailed: false,
    errorMessage: '',
    loadingCount: 0,
    saga: null
  );
}

// class NotRegistered extends RegistrationState<RegistrationFormScreen> {
//   NotRegistered(): super(null);

//   @override
//   List<Object> get props => [];
  
//   @override
//   String toString() => 'State: Not Registered';

//   @override
//   RegistrationFormScreen getWidget() {
//     return RegistrationFormScreen();
//   }
// }

// class RegisterAPILoading extends RegistrationState<RegistrationStatelessLoadingScreen> {
//   RegisterAPILoading() : super(Saga(state: 'apiLoading', id: 0));

//   @override
//   RegistrationStatelessLoadingScreen getWidget() {
//     return RegistrationStatelessLoadingScreen();
//   }
// }

// class RegisterLoading extends RegistrationState<RegistrationLoadingScreen> {
//   RegisterLoading(Saga registrationSaga) : super(registrationSaga);

//   @override
//   RegistrationLoadingScreen getWidget() {
//     return RegistrationLoadingScreen(data);
//   }
// }

// class Registered extends RegistrationState<RegistrationSuccessScreen> {
//   Registered(Saga registrationSaga): super(registrationSaga);

//   @override
//   RegistrationSuccessScreen getWidget() {
//     return RegistrationSuccessScreen();
//   }
// }

// class RegisterFailure extends RegistrationState<RegistrationFailedScreen> {
//   RegisterFailure(Saga registrationSaga): super(registrationSaga);

//   @override
//   RegistrationFailedScreen getWidget() {
//     return RegistrationFailedScreen();
//   }
// }