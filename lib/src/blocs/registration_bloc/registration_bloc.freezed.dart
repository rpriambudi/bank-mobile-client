// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'registration_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$RegistrationStateTearOff {
  const _$RegistrationStateTearOff();

  _RegistrationState call(
      {@required bool isLoading,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage,
      @required int loadingCount,
      Saga saga}) {
    return _RegistrationState(
      isLoading: isLoading,
      isSuccess: isSuccess,
      isFailed: isFailed,
      errorMessage: errorMessage,
      loadingCount: loadingCount,
      saga: saga,
    );
  }
}

// ignore: unused_element
const $RegistrationState = _$RegistrationStateTearOff();

mixin _$RegistrationState {
  bool get isLoading;
  bool get isSuccess;
  bool get isFailed;
  String get errorMessage;
  int get loadingCount;
  Saga get saga;

  $RegistrationStateCopyWith<RegistrationState> get copyWith;
}

abstract class $RegistrationStateCopyWith<$Res> {
  factory $RegistrationStateCopyWith(
          RegistrationState value, $Res Function(RegistrationState) then) =
      _$RegistrationStateCopyWithImpl<$Res>;
  $Res call(
      {bool isLoading,
      bool isSuccess,
      bool isFailed,
      String errorMessage,
      int loadingCount,
      Saga saga});
}

class _$RegistrationStateCopyWithImpl<$Res>
    implements $RegistrationStateCopyWith<$Res> {
  _$RegistrationStateCopyWithImpl(this._value, this._then);

  final RegistrationState _value;
  // ignore: unused_field
  final $Res Function(RegistrationState) _then;

  @override
  $Res call({
    Object isLoading = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
    Object loadingCount = freezed,
    Object saga = freezed,
  }) {
    return _then(_value.copyWith(
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
      loadingCount:
          loadingCount == freezed ? _value.loadingCount : loadingCount as int,
      saga: saga == freezed ? _value.saga : saga as Saga,
    ));
  }
}

abstract class _$RegistrationStateCopyWith<$Res>
    implements $RegistrationStateCopyWith<$Res> {
  factory _$RegistrationStateCopyWith(
          _RegistrationState value, $Res Function(_RegistrationState) then) =
      __$RegistrationStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isLoading,
      bool isSuccess,
      bool isFailed,
      String errorMessage,
      int loadingCount,
      Saga saga});
}

class __$RegistrationStateCopyWithImpl<$Res>
    extends _$RegistrationStateCopyWithImpl<$Res>
    implements _$RegistrationStateCopyWith<$Res> {
  __$RegistrationStateCopyWithImpl(
      _RegistrationState _value, $Res Function(_RegistrationState) _then)
      : super(_value, (v) => _then(v as _RegistrationState));

  @override
  _RegistrationState get _value => super._value as _RegistrationState;

  @override
  $Res call({
    Object isLoading = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
    Object loadingCount = freezed,
    Object saga = freezed,
  }) {
    return _then(_RegistrationState(
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
      loadingCount:
          loadingCount == freezed ? _value.loadingCount : loadingCount as int,
      saga: saga == freezed ? _value.saga : saga as Saga,
    ));
  }
}

class _$_RegistrationState
    with DiagnosticableTreeMixin
    implements _RegistrationState {
  const _$_RegistrationState(
      {@required this.isLoading,
      @required this.isSuccess,
      @required this.isFailed,
      @required this.errorMessage,
      @required this.loadingCount,
      this.saga})
      : assert(isLoading != null),
        assert(isSuccess != null),
        assert(isFailed != null),
        assert(errorMessage != null),
        assert(loadingCount != null);

  @override
  final bool isLoading;
  @override
  final bool isSuccess;
  @override
  final bool isFailed;
  @override
  final String errorMessage;
  @override
  final int loadingCount;
  @override
  final Saga saga;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RegistrationState(isLoading: $isLoading, isSuccess: $isSuccess, isFailed: $isFailed, errorMessage: $errorMessage, loadingCount: $loadingCount, saga: $saga)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'RegistrationState'))
      ..add(DiagnosticsProperty('isLoading', isLoading))
      ..add(DiagnosticsProperty('isSuccess', isSuccess))
      ..add(DiagnosticsProperty('isFailed', isFailed))
      ..add(DiagnosticsProperty('errorMessage', errorMessage))
      ..add(DiagnosticsProperty('loadingCount', loadingCount))
      ..add(DiagnosticsProperty('saga', saga));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RegistrationState &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)) &&
            (identical(other.isSuccess, isSuccess) ||
                const DeepCollectionEquality()
                    .equals(other.isSuccess, isSuccess)) &&
            (identical(other.isFailed, isFailed) ||
                const DeepCollectionEquality()
                    .equals(other.isFailed, isFailed)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)) &&
            (identical(other.loadingCount, loadingCount) ||
                const DeepCollectionEquality()
                    .equals(other.loadingCount, loadingCount)) &&
            (identical(other.saga, saga) ||
                const DeepCollectionEquality().equals(other.saga, saga)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isLoading) ^
      const DeepCollectionEquality().hash(isSuccess) ^
      const DeepCollectionEquality().hash(isFailed) ^
      const DeepCollectionEquality().hash(errorMessage) ^
      const DeepCollectionEquality().hash(loadingCount) ^
      const DeepCollectionEquality().hash(saga);

  @override
  _$RegistrationStateCopyWith<_RegistrationState> get copyWith =>
      __$RegistrationStateCopyWithImpl<_RegistrationState>(this, _$identity);
}

abstract class _RegistrationState implements RegistrationState {
  const factory _RegistrationState(
      {@required bool isLoading,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage,
      @required int loadingCount,
      Saga saga}) = _$_RegistrationState;

  @override
  bool get isLoading;
  @override
  bool get isSuccess;
  @override
  bool get isFailed;
  @override
  String get errorMessage;
  @override
  int get loadingCount;
  @override
  Saga get saga;
  @override
  _$RegistrationStateCopyWith<_RegistrationState> get copyWith;
}

class _$RegistrationEventTearOff {
  const _$RegistrationEventTearOff();

  SubmittingForm submittingForm(Registration formData) {
    return SubmittingForm(
      formData,
    );
  }

  ProcessingSaga processingSaga() {
    return const ProcessingSaga();
  }
}

// ignore: unused_element
const $RegistrationEvent = _$RegistrationEventTearOff();

mixin _$RegistrationEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result submittingForm(Registration formData),
    @required Result processingSaga(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result submittingForm(Registration formData),
    Result processingSaga(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result submittingForm(SubmittingForm value),
    @required Result processingSaga(ProcessingSaga value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result submittingForm(SubmittingForm value),
    Result processingSaga(ProcessingSaga value),
    @required Result orElse(),
  });
}

abstract class $RegistrationEventCopyWith<$Res> {
  factory $RegistrationEventCopyWith(
          RegistrationEvent value, $Res Function(RegistrationEvent) then) =
      _$RegistrationEventCopyWithImpl<$Res>;
}

class _$RegistrationEventCopyWithImpl<$Res>
    implements $RegistrationEventCopyWith<$Res> {
  _$RegistrationEventCopyWithImpl(this._value, this._then);

  final RegistrationEvent _value;
  // ignore: unused_field
  final $Res Function(RegistrationEvent) _then;
}

abstract class $SubmittingFormCopyWith<$Res> {
  factory $SubmittingFormCopyWith(
          SubmittingForm value, $Res Function(SubmittingForm) then) =
      _$SubmittingFormCopyWithImpl<$Res>;
  $Res call({Registration formData});
}

class _$SubmittingFormCopyWithImpl<$Res>
    extends _$RegistrationEventCopyWithImpl<$Res>
    implements $SubmittingFormCopyWith<$Res> {
  _$SubmittingFormCopyWithImpl(
      SubmittingForm _value, $Res Function(SubmittingForm) _then)
      : super(_value, (v) => _then(v as SubmittingForm));

  @override
  SubmittingForm get _value => super._value as SubmittingForm;

  @override
  $Res call({
    Object formData = freezed,
  }) {
    return _then(SubmittingForm(
      formData == freezed ? _value.formData : formData as Registration,
    ));
  }
}

class _$SubmittingForm with DiagnosticableTreeMixin implements SubmittingForm {
  const _$SubmittingForm(this.formData) : assert(formData != null);

  @override
  final Registration formData;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RegistrationEvent.submittingForm(formData: $formData)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'RegistrationEvent.submittingForm'))
      ..add(DiagnosticsProperty('formData', formData));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SubmittingForm &&
            (identical(other.formData, formData) ||
                const DeepCollectionEquality()
                    .equals(other.formData, formData)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(formData);

  @override
  $SubmittingFormCopyWith<SubmittingForm> get copyWith =>
      _$SubmittingFormCopyWithImpl<SubmittingForm>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result submittingForm(Registration formData),
    @required Result processingSaga(),
  }) {
    assert(submittingForm != null);
    assert(processingSaga != null);
    return submittingForm(formData);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result submittingForm(Registration formData),
    Result processingSaga(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (submittingForm != null) {
      return submittingForm(formData);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result submittingForm(SubmittingForm value),
    @required Result processingSaga(ProcessingSaga value),
  }) {
    assert(submittingForm != null);
    assert(processingSaga != null);
    return submittingForm(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result submittingForm(SubmittingForm value),
    Result processingSaga(ProcessingSaga value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (submittingForm != null) {
      return submittingForm(this);
    }
    return orElse();
  }
}

abstract class SubmittingForm implements RegistrationEvent {
  const factory SubmittingForm(Registration formData) = _$SubmittingForm;

  Registration get formData;
  $SubmittingFormCopyWith<SubmittingForm> get copyWith;
}

abstract class $ProcessingSagaCopyWith<$Res> {
  factory $ProcessingSagaCopyWith(
          ProcessingSaga value, $Res Function(ProcessingSaga) then) =
      _$ProcessingSagaCopyWithImpl<$Res>;
}

class _$ProcessingSagaCopyWithImpl<$Res>
    extends _$RegistrationEventCopyWithImpl<$Res>
    implements $ProcessingSagaCopyWith<$Res> {
  _$ProcessingSagaCopyWithImpl(
      ProcessingSaga _value, $Res Function(ProcessingSaga) _then)
      : super(_value, (v) => _then(v as ProcessingSaga));

  @override
  ProcessingSaga get _value => super._value as ProcessingSaga;
}

class _$ProcessingSaga with DiagnosticableTreeMixin implements ProcessingSaga {
  const _$ProcessingSaga();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RegistrationEvent.processingSaga()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'RegistrationEvent.processingSaga'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ProcessingSaga);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result submittingForm(Registration formData),
    @required Result processingSaga(),
  }) {
    assert(submittingForm != null);
    assert(processingSaga != null);
    return processingSaga();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result submittingForm(Registration formData),
    Result processingSaga(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (processingSaga != null) {
      return processingSaga();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result submittingForm(SubmittingForm value),
    @required Result processingSaga(ProcessingSaga value),
  }) {
    assert(submittingForm != null);
    assert(processingSaga != null);
    return processingSaga(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result submittingForm(SubmittingForm value),
    Result processingSaga(ProcessingSaga value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (processingSaga != null) {
      return processingSaga(this);
    }
    return orElse();
  }
}

abstract class ProcessingSaga implements RegistrationEvent {
  const factory ProcessingSaga() = _$ProcessingSaga;
}
