import 'dart:async';

import 'package:bank_mobile_client/src/injector.dart';
import 'package:bank_mobile_client/src/models/registration.dart';
import 'package:bank_mobile_client/src/models/saga.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/registration_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'registration_bloc.freezed.dart';
part 'registration_state.dart';
part 'registration_event.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  @override
  RegistrationState get initialState => RegistrationState.initial();

  @override
  Stream<RegistrationState> mapEventToState(RegistrationEvent events) async* {
    yield* events.map(
      submittingForm: (e) async* {
        yield* _submittingForm(e.formData);
      }, 
      processingSaga: (e) async* {
        yield* _processingForm();
      }
    );
  }

  Stream<RegistrationState> _submittingForm(Registration formData) async* {
    yield state.copyWith(
      isLoading: true
    );

    try {
      final Saga saga = await injector.get<RegistrationRepository>().submitRegistration(formData);
      yield state.copyWith(
        saga: saga
      );
    } catch (error) {
      yield state.copyWith(
        isLoading: false, 
        isSuccess: false, 
        isFailed: true,
        errorMessage: error.message != null ? error.message : 'Unknown Error',
        loadingCount: 0,
        saga: null
      );
    }
  }

  Stream<RegistrationState> _processingForm() async* {
    try {
      final Saga saga = await injector.get<RegistrationRepository>().getRegistrationState(state.saga.id);
      if (saga.state == 'sagaFinished') {
        yield state.copyWith(
          isLoading: false, 
          isSuccess: true, 
          isFailed: false,
          errorMessage: '',
          loadingCount: 0,
          saga: null
        );
        return;
      }

      if (saga.state.contains('Failed') || saga.state.contains('Rollbacked')) {
        yield state.copyWith(
          isLoading: false, 
          isSuccess: false, 
          isFailed: true,
          errorMessage: saga.description != null ? saga.description : 'Unknown Error',
          loadingCount: 0,
          saga: null
        );
        return;
      }

      yield state.copyWith(
        isLoading: true,
        loadingCount: state.loadingCount + 1
      );
    } catch(error) {
      yield state.copyWith(
        isLoading: false, 
        isSuccess: false, 
        isFailed: true,
        errorMessage: error.message != null ? error.message : 'Unknown Error',
        loadingCount: 0,
        saga: null
      );
    }
  }
}