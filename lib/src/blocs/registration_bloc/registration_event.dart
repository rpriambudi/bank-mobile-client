part of 'registration_bloc.dart';

@freezed
abstract class RegistrationEvent with _$RegistrationEvent {
  const factory RegistrationEvent.submittingForm(Registration formData) = SubmittingForm;
  const factory RegistrationEvent.processingSaga() = ProcessingSaga;
}

// class RegistrationStarted extends RegistrationEvent<Registration> {
//   final RegistrationRepository _repository = RegistrationRepository();
//   RegistrationStarted(Registration eventData) : super(eventData);

//   @override
//   Stream<RegistrationState> mapEventToState(Registration data) async* {
//     yield RegisterAPILoading();
//     try {
//       final saga = await _repository.submitRegistration(data);
//       yield RegisterLoading(saga);
//     } catch (error) {
//       yield RegisterFailure(new Saga(state: error.message));
//     }
//   }
// }

// class RegistrationPending extends RegistrationEvent<int> {
//   final RegistrationRepository _repository = RegistrationRepository();
//   RegistrationPending(int eventData) : super(eventData);

//   @override
//   Stream<RegistrationState> mapEventToState(int data) async* {
//     final saga = await _repository.getRegistrationState(data);
//     if (saga.state == 'sagaFinished') {
//       yield Registered(saga);
//       return;
//     }

//     if (saga.state.contains('Failed')) {
//       yield RegisterFailure(saga);
//       return;
//     }

//     yield RegisterLoading(saga);
//   }
// }

// // currently not used, since the success page only showing stateless widget, not calling any event
// class RegistrationSuccess extends RegistrationEvent<int> {
//   final RegistrationRepository _repository = RegistrationRepository();
//   RegistrationSuccess(int eventData) : super(eventData);

//   @override
//   Stream<RegistrationState> mapEventToState(int data) async* {
//     final saga = await _repository.getRegistrationState(data);
//     yield Registered(saga);
//   }
// }

// // currently not used, sunce the failed page only showing stateless page, not calling any event
// class RegistrationFailed extends RegistrationEvent<int> {
//   final RegistrationRepository _repository = RegistrationRepository();
//   RegistrationFailed(int eventData) : super(eventData);

//   @override
//   Stream<RegistrationState> mapEventToState(int data) async* {
//     final saga = await _repository.getRegistrationState(data);
//     yield Registered(saga);
//   }
// }