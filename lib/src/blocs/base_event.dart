import 'package:bank_mobile_client/src/blocs/base_state.dart';
import 'package:equatable/equatable.dart';

abstract class BaseEvent<DataType, SagaState> extends Equatable {
  final DataType data;
  BaseEvent(DataType eventData): data = eventData;

  @override
  List<Object> get props => [data];

  Stream<SagaState> mapEventToState(DataType data);
}