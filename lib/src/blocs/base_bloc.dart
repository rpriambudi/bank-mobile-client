import 'dart:async';
import 'package:bank_mobile_client/src/blocs/base_event.dart';
import 'package:bank_mobile_client/src/blocs/base_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class BaseBloc<T extends BaseEvent, U extends BaseState> extends Bloc<T, U> {
  @override
  Stream<U> mapEventToState(T event) async* {
    yield* event.mapEventToState(event.data);
  }

  BlocBuilder<BaseBloc, BaseState> getBlocBuilder() {
    return BlocBuilder<BaseBloc, BaseState>(
      builder: (context, state) {
          return state.getWidget();
        },
    );
  }

  BlocProvider getBlocProvider(BaseBloc implementations, Widget mainWidget) {
    return BlocProvider(
      create: (context) => implementations,
      child: mainWidget,
    );
  }
}