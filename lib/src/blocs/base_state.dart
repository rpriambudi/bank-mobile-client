import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class BaseState<DataType, T extends Widget> extends Equatable {
  final DataType data;

  BaseState(DataType stateData): data = stateData;

  @override
  List<Object> get props => [data];

  @override
  String toString() => 'Data: ${data}';

  T getWidget();
}