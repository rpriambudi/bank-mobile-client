part of 'set_verification_bloc.dart';

@freezed
abstract class SetVerificationEvent with _$SetVerificationEvent {
  const factory SetVerificationEvent.setAsPin() = SetAsPin;
  const factory SetVerificationEvent.setAsOtp() = SetAsOtp;
}