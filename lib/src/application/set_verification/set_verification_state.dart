part of 'set_verification_bloc.dart';

@freezed
abstract class SetVerificationState with _$SetVerificationState {
  const factory SetVerificationState({
    @required bool isSubmitted,
    @required bool isSuccess,
    @required bool isFailed,
    @required String errorMessage,
  }) = _SetVerificationState;

  factory SetVerificationState.initial() => SetVerificationState(
    isSubmitted: false,
    isSuccess: false, 
    isFailed: false, 
    errorMessage: '',
  );
}