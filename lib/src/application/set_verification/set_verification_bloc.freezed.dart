// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'set_verification_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$SetVerificationEventTearOff {
  const _$SetVerificationEventTearOff();

  SetAsPin setAsPin() {
    return const SetAsPin();
  }

  SetAsOtp setAsOtp() {
    return const SetAsOtp();
  }
}

// ignore: unused_element
const $SetVerificationEvent = _$SetVerificationEventTearOff();

mixin _$SetVerificationEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result setAsPin(),
    @required Result setAsOtp(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result setAsPin(),
    Result setAsOtp(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result setAsPin(SetAsPin value),
    @required Result setAsOtp(SetAsOtp value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result setAsPin(SetAsPin value),
    Result setAsOtp(SetAsOtp value),
    @required Result orElse(),
  });
}

abstract class $SetVerificationEventCopyWith<$Res> {
  factory $SetVerificationEventCopyWith(SetVerificationEvent value,
          $Res Function(SetVerificationEvent) then) =
      _$SetVerificationEventCopyWithImpl<$Res>;
}

class _$SetVerificationEventCopyWithImpl<$Res>
    implements $SetVerificationEventCopyWith<$Res> {
  _$SetVerificationEventCopyWithImpl(this._value, this._then);

  final SetVerificationEvent _value;
  // ignore: unused_field
  final $Res Function(SetVerificationEvent) _then;
}

abstract class $SetAsPinCopyWith<$Res> {
  factory $SetAsPinCopyWith(SetAsPin value, $Res Function(SetAsPin) then) =
      _$SetAsPinCopyWithImpl<$Res>;
}

class _$SetAsPinCopyWithImpl<$Res>
    extends _$SetVerificationEventCopyWithImpl<$Res>
    implements $SetAsPinCopyWith<$Res> {
  _$SetAsPinCopyWithImpl(SetAsPin _value, $Res Function(SetAsPin) _then)
      : super(_value, (v) => _then(v as SetAsPin));

  @override
  SetAsPin get _value => super._value as SetAsPin;
}

class _$SetAsPin with DiagnosticableTreeMixin implements SetAsPin {
  const _$SetAsPin();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SetVerificationEvent.setAsPin()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SetVerificationEvent.setAsPin'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is SetAsPin);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result setAsPin(),
    @required Result setAsOtp(),
  }) {
    assert(setAsPin != null);
    assert(setAsOtp != null);
    return setAsPin();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result setAsPin(),
    Result setAsOtp(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (setAsPin != null) {
      return setAsPin();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result setAsPin(SetAsPin value),
    @required Result setAsOtp(SetAsOtp value),
  }) {
    assert(setAsPin != null);
    assert(setAsOtp != null);
    return setAsPin(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result setAsPin(SetAsPin value),
    Result setAsOtp(SetAsOtp value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (setAsPin != null) {
      return setAsPin(this);
    }
    return orElse();
  }
}

abstract class SetAsPin implements SetVerificationEvent {
  const factory SetAsPin() = _$SetAsPin;
}

abstract class $SetAsOtpCopyWith<$Res> {
  factory $SetAsOtpCopyWith(SetAsOtp value, $Res Function(SetAsOtp) then) =
      _$SetAsOtpCopyWithImpl<$Res>;
}

class _$SetAsOtpCopyWithImpl<$Res>
    extends _$SetVerificationEventCopyWithImpl<$Res>
    implements $SetAsOtpCopyWith<$Res> {
  _$SetAsOtpCopyWithImpl(SetAsOtp _value, $Res Function(SetAsOtp) _then)
      : super(_value, (v) => _then(v as SetAsOtp));

  @override
  SetAsOtp get _value => super._value as SetAsOtp;
}

class _$SetAsOtp with DiagnosticableTreeMixin implements SetAsOtp {
  const _$SetAsOtp();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SetVerificationEvent.setAsOtp()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SetVerificationEvent.setAsOtp'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is SetAsOtp);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result setAsPin(),
    @required Result setAsOtp(),
  }) {
    assert(setAsPin != null);
    assert(setAsOtp != null);
    return setAsOtp();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result setAsPin(),
    Result setAsOtp(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (setAsOtp != null) {
      return setAsOtp();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result setAsPin(SetAsPin value),
    @required Result setAsOtp(SetAsOtp value),
  }) {
    assert(setAsPin != null);
    assert(setAsOtp != null);
    return setAsOtp(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result setAsPin(SetAsPin value),
    Result setAsOtp(SetAsOtp value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (setAsOtp != null) {
      return setAsOtp(this);
    }
    return orElse();
  }
}

abstract class SetAsOtp implements SetVerificationEvent {
  const factory SetAsOtp() = _$SetAsOtp;
}

class _$SetVerificationStateTearOff {
  const _$SetVerificationStateTearOff();

  _SetVerificationState call(
      {@required bool isSubmitted,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage}) {
    return _SetVerificationState(
      isSubmitted: isSubmitted,
      isSuccess: isSuccess,
      isFailed: isFailed,
      errorMessage: errorMessage,
    );
  }
}

// ignore: unused_element
const $SetVerificationState = _$SetVerificationStateTearOff();

mixin _$SetVerificationState {
  bool get isSubmitted;
  bool get isSuccess;
  bool get isFailed;
  String get errorMessage;

  $SetVerificationStateCopyWith<SetVerificationState> get copyWith;
}

abstract class $SetVerificationStateCopyWith<$Res> {
  factory $SetVerificationStateCopyWith(SetVerificationState value,
          $Res Function(SetVerificationState) then) =
      _$SetVerificationStateCopyWithImpl<$Res>;
  $Res call(
      {bool isSubmitted, bool isSuccess, bool isFailed, String errorMessage});
}

class _$SetVerificationStateCopyWithImpl<$Res>
    implements $SetVerificationStateCopyWith<$Res> {
  _$SetVerificationStateCopyWithImpl(this._value, this._then);

  final SetVerificationState _value;
  // ignore: unused_field
  final $Res Function(SetVerificationState) _then;

  @override
  $Res call({
    Object isSubmitted = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      isSubmitted:
          isSubmitted == freezed ? _value.isSubmitted : isSubmitted as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
    ));
  }
}

abstract class _$SetVerificationStateCopyWith<$Res>
    implements $SetVerificationStateCopyWith<$Res> {
  factory _$SetVerificationStateCopyWith(_SetVerificationState value,
          $Res Function(_SetVerificationState) then) =
      __$SetVerificationStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isSubmitted, bool isSuccess, bool isFailed, String errorMessage});
}

class __$SetVerificationStateCopyWithImpl<$Res>
    extends _$SetVerificationStateCopyWithImpl<$Res>
    implements _$SetVerificationStateCopyWith<$Res> {
  __$SetVerificationStateCopyWithImpl(
      _SetVerificationState _value, $Res Function(_SetVerificationState) _then)
      : super(_value, (v) => _then(v as _SetVerificationState));

  @override
  _SetVerificationState get _value => super._value as _SetVerificationState;

  @override
  $Res call({
    Object isSubmitted = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
  }) {
    return _then(_SetVerificationState(
      isSubmitted:
          isSubmitted == freezed ? _value.isSubmitted : isSubmitted as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
    ));
  }
}

class _$_SetVerificationState
    with DiagnosticableTreeMixin
    implements _SetVerificationState {
  const _$_SetVerificationState(
      {@required this.isSubmitted,
      @required this.isSuccess,
      @required this.isFailed,
      @required this.errorMessage})
      : assert(isSubmitted != null),
        assert(isSuccess != null),
        assert(isFailed != null),
        assert(errorMessage != null);

  @override
  final bool isSubmitted;
  @override
  final bool isSuccess;
  @override
  final bool isFailed;
  @override
  final String errorMessage;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SetVerificationState(isSubmitted: $isSubmitted, isSuccess: $isSuccess, isFailed: $isFailed, errorMessage: $errorMessage)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SetVerificationState'))
      ..add(DiagnosticsProperty('isSubmitted', isSubmitted))
      ..add(DiagnosticsProperty('isSuccess', isSuccess))
      ..add(DiagnosticsProperty('isFailed', isFailed))
      ..add(DiagnosticsProperty('errorMessage', errorMessage));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SetVerificationState &&
            (identical(other.isSubmitted, isSubmitted) ||
                const DeepCollectionEquality()
                    .equals(other.isSubmitted, isSubmitted)) &&
            (identical(other.isSuccess, isSuccess) ||
                const DeepCollectionEquality()
                    .equals(other.isSuccess, isSuccess)) &&
            (identical(other.isFailed, isFailed) ||
                const DeepCollectionEquality()
                    .equals(other.isFailed, isFailed)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isSubmitted) ^
      const DeepCollectionEquality().hash(isSuccess) ^
      const DeepCollectionEquality().hash(isFailed) ^
      const DeepCollectionEquality().hash(errorMessage);

  @override
  _$SetVerificationStateCopyWith<_SetVerificationState> get copyWith =>
      __$SetVerificationStateCopyWithImpl<_SetVerificationState>(
          this, _$identity);
}

abstract class _SetVerificationState implements SetVerificationState {
  const factory _SetVerificationState(
      {@required bool isSubmitted,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage}) = _$_SetVerificationState;

  @override
  bool get isSubmitted;
  @override
  bool get isSuccess;
  @override
  bool get isFailed;
  @override
  String get errorMessage;
  @override
  _$SetVerificationStateCopyWith<_SetVerificationState> get copyWith;
}
