import 'package:bank_mobile_client/src/injector.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/verification_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'set_verification_event.dart';
part 'set_verification_state.dart';
part 'set_verification_bloc.freezed.dart';

class SetVerificationBloc extends Bloc<SetVerificationEvent, SetVerificationState> {
  @override
  SetVerificationState get initialState => SetVerificationState.initial();

  @override
  Stream<SetVerificationState> mapEventToState(SetVerificationEvent event) async* {
    yield* event.map(
      setAsPin: (e) async* {
        yield* _setAsPin();
      },
      setAsOtp: (e) async* {
        yield* _setAsOtp();
      },
    );
  }

  Stream<SetVerificationState> _setAsPin() async* {
    yield state.copyWith(
      isSubmitted: true,
      isSuccess: false,
      isFailed: false,
      errorMessage: ''
    );

    try {
      await injector.get<VerificationRepository>().setVerification('PIN');
      yield state.copyWith(
        isSubmitted: false,
        isSuccess: true,
        isFailed: false,
        errorMessage: ''
      );
    } catch(error) {
      yield state.copyWith(
        isSubmitted: false,
        isSuccess: false,
        isFailed: true,
        errorMessage: error.message
      );
    }
  }

  Stream<SetVerificationState> _setAsOtp() async* {
    yield state.copyWith(
      isSubmitted: true,
      isSuccess: false,
      isFailed: false,
      errorMessage: ''
    );

    try {
      await injector.get<VerificationRepository>().setVerification('OTP');
      yield state.copyWith(
        isSubmitted: false,
        isSuccess: true,
        isFailed: false,
        errorMessage: ''
      );
    } catch(error) {
      yield state.copyWith(
        isSubmitted: false,
        isSuccess: false,
        isFailed: true,
        errorMessage: error.message
      );
    }
  }
}