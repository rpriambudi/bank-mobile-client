part of 'check_balance_bloc.dart';

@freezed
abstract class CheckBalanceEvent with _$CheckBalanceEvent {
  const factory CheckBalanceEvent.initialization() = Initialization;
  const factory CheckBalanceEvent.requestBalance() = RequestBalance;
}