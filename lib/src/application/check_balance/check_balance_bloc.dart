import 'package:bank_mobile_client/src/injector.dart';
import 'package:bank_mobile_client/src/models/account.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/account_repository.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/verification_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'check_balance_event.dart';
part 'check_balance_state.dart';
part 'check_balance_bloc.freezed.dart';

class CheckBalanceBloc extends Bloc<CheckBalanceEvent, CheckBalanceState> {
  @override
  CheckBalanceState get initialState => CheckBalanceState.initial();

  @override
  Stream<CheckBalanceState> mapEventToState(CheckBalanceEvent event) async* {
    yield* event.map(
      initialization: (e) async* {
        yield* _initialization();
      }, 
      requestBalance: (e) async* {
        yield* _getAccountBalance();
      }
    );
  }

  Stream<CheckBalanceState> _initialization() async* {
    yield state.copyWith(
      isLoading: true
    );

    try {
      final String verificationType = await injector.get<VerificationRepository>().getVerificationType();

      yield state.copyWith(
        isLoading: false,
        isSuccess: false,
        isFailed: false,
        verificationType: verificationType,
        errorMessage: '',
        accounts: []
      );
    } catch (error) {
      yield state.copyWith(
        isLoading: false, 
        isSuccess: false, 
        isFailed: true, 
        verificationType: '',
        errorMessage: error.message != null ? error.message : 'Unknown Error', 
        accounts: []
      );
    }
  }

  Stream<CheckBalanceState> _getAccountBalance() async* {
    yield state.copyWith(
      isLoading: true,
      verificationType: '',
    );

    try {
      final List<Account> accounts = await injector.get<AccountRepository>().getAccounts();

      yield state.copyWith(
        isLoading: false,
        isSuccess: true,
        isFailed: false,
        errorMessage: '',
        verificationType: '',
        accounts: accounts
      );
    } catch(error) {
      yield state.copyWith(
        isLoading: false,  
        isSuccess: false, 
        isFailed: true, 
        errorMessage: error.message != null ? error.message : 'Unkown Error',
        verificationType: '',
        accounts: []
      );
    }
  }

}