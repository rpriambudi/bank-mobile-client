// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'check_balance_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$CheckBalanceEventTearOff {
  const _$CheckBalanceEventTearOff();

  Initialization initialization() {
    return const Initialization();
  }

  RequestBalance requestBalance() {
    return const RequestBalance();
  }
}

// ignore: unused_element
const $CheckBalanceEvent = _$CheckBalanceEventTearOff();

mixin _$CheckBalanceEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initialization(),
    @required Result requestBalance(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initialization(),
    Result requestBalance(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initialization(Initialization value),
    @required Result requestBalance(RequestBalance value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initialization(Initialization value),
    Result requestBalance(RequestBalance value),
    @required Result orElse(),
  });
}

abstract class $CheckBalanceEventCopyWith<$Res> {
  factory $CheckBalanceEventCopyWith(
          CheckBalanceEvent value, $Res Function(CheckBalanceEvent) then) =
      _$CheckBalanceEventCopyWithImpl<$Res>;
}

class _$CheckBalanceEventCopyWithImpl<$Res>
    implements $CheckBalanceEventCopyWith<$Res> {
  _$CheckBalanceEventCopyWithImpl(this._value, this._then);

  final CheckBalanceEvent _value;
  // ignore: unused_field
  final $Res Function(CheckBalanceEvent) _then;
}

abstract class $InitializationCopyWith<$Res> {
  factory $InitializationCopyWith(
          Initialization value, $Res Function(Initialization) then) =
      _$InitializationCopyWithImpl<$Res>;
}

class _$InitializationCopyWithImpl<$Res>
    extends _$CheckBalanceEventCopyWithImpl<$Res>
    implements $InitializationCopyWith<$Res> {
  _$InitializationCopyWithImpl(
      Initialization _value, $Res Function(Initialization) _then)
      : super(_value, (v) => _then(v as Initialization));

  @override
  Initialization get _value => super._value as Initialization;
}

class _$Initialization with DiagnosticableTreeMixin implements Initialization {
  const _$Initialization();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CheckBalanceEvent.initialization()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CheckBalanceEvent.initialization'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Initialization);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initialization(),
    @required Result requestBalance(),
  }) {
    assert(initialization != null);
    assert(requestBalance != null);
    return initialization();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initialization(),
    Result requestBalance(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initialization != null) {
      return initialization();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initialization(Initialization value),
    @required Result requestBalance(RequestBalance value),
  }) {
    assert(initialization != null);
    assert(requestBalance != null);
    return initialization(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initialization(Initialization value),
    Result requestBalance(RequestBalance value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initialization != null) {
      return initialization(this);
    }
    return orElse();
  }
}

abstract class Initialization implements CheckBalanceEvent {
  const factory Initialization() = _$Initialization;
}

abstract class $RequestBalanceCopyWith<$Res> {
  factory $RequestBalanceCopyWith(
          RequestBalance value, $Res Function(RequestBalance) then) =
      _$RequestBalanceCopyWithImpl<$Res>;
}

class _$RequestBalanceCopyWithImpl<$Res>
    extends _$CheckBalanceEventCopyWithImpl<$Res>
    implements $RequestBalanceCopyWith<$Res> {
  _$RequestBalanceCopyWithImpl(
      RequestBalance _value, $Res Function(RequestBalance) _then)
      : super(_value, (v) => _then(v as RequestBalance));

  @override
  RequestBalance get _value => super._value as RequestBalance;
}

class _$RequestBalance with DiagnosticableTreeMixin implements RequestBalance {
  const _$RequestBalance();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CheckBalanceEvent.requestBalance()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CheckBalanceEvent.requestBalance'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is RequestBalance);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initialization(),
    @required Result requestBalance(),
  }) {
    assert(initialization != null);
    assert(requestBalance != null);
    return requestBalance();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initialization(),
    Result requestBalance(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (requestBalance != null) {
      return requestBalance();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initialization(Initialization value),
    @required Result requestBalance(RequestBalance value),
  }) {
    assert(initialization != null);
    assert(requestBalance != null);
    return requestBalance(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initialization(Initialization value),
    Result requestBalance(RequestBalance value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (requestBalance != null) {
      return requestBalance(this);
    }
    return orElse();
  }
}

abstract class RequestBalance implements CheckBalanceEvent {
  const factory RequestBalance() = _$RequestBalance;
}

class _$CheckBalanceStateTearOff {
  const _$CheckBalanceStateTearOff();

  _CheckBalanceState call(
      {@required bool isLoading,
      @required bool isSuccess,
      @required bool isFailed,
      @required String verificationType,
      @required String errorMessage,
      @required List<Account> accounts}) {
    return _CheckBalanceState(
      isLoading: isLoading,
      isSuccess: isSuccess,
      isFailed: isFailed,
      verificationType: verificationType,
      errorMessage: errorMessage,
      accounts: accounts,
    );
  }
}

// ignore: unused_element
const $CheckBalanceState = _$CheckBalanceStateTearOff();

mixin _$CheckBalanceState {
  bool get isLoading;
  bool get isSuccess;
  bool get isFailed;
  String get verificationType;
  String get errorMessage;
  List<Account> get accounts;

  $CheckBalanceStateCopyWith<CheckBalanceState> get copyWith;
}

abstract class $CheckBalanceStateCopyWith<$Res> {
  factory $CheckBalanceStateCopyWith(
          CheckBalanceState value, $Res Function(CheckBalanceState) then) =
      _$CheckBalanceStateCopyWithImpl<$Res>;
  $Res call(
      {bool isLoading,
      bool isSuccess,
      bool isFailed,
      String verificationType,
      String errorMessage,
      List<Account> accounts});
}

class _$CheckBalanceStateCopyWithImpl<$Res>
    implements $CheckBalanceStateCopyWith<$Res> {
  _$CheckBalanceStateCopyWithImpl(this._value, this._then);

  final CheckBalanceState _value;
  // ignore: unused_field
  final $Res Function(CheckBalanceState) _then;

  @override
  $Res call({
    Object isLoading = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object verificationType = freezed,
    Object errorMessage = freezed,
    Object accounts = freezed,
  }) {
    return _then(_value.copyWith(
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      verificationType: verificationType == freezed
          ? _value.verificationType
          : verificationType as String,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
      accounts:
          accounts == freezed ? _value.accounts : accounts as List<Account>,
    ));
  }
}

abstract class _$CheckBalanceStateCopyWith<$Res>
    implements $CheckBalanceStateCopyWith<$Res> {
  factory _$CheckBalanceStateCopyWith(
          _CheckBalanceState value, $Res Function(_CheckBalanceState) then) =
      __$CheckBalanceStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isLoading,
      bool isSuccess,
      bool isFailed,
      String verificationType,
      String errorMessage,
      List<Account> accounts});
}

class __$CheckBalanceStateCopyWithImpl<$Res>
    extends _$CheckBalanceStateCopyWithImpl<$Res>
    implements _$CheckBalanceStateCopyWith<$Res> {
  __$CheckBalanceStateCopyWithImpl(
      _CheckBalanceState _value, $Res Function(_CheckBalanceState) _then)
      : super(_value, (v) => _then(v as _CheckBalanceState));

  @override
  _CheckBalanceState get _value => super._value as _CheckBalanceState;

  @override
  $Res call({
    Object isLoading = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object verificationType = freezed,
    Object errorMessage = freezed,
    Object accounts = freezed,
  }) {
    return _then(_CheckBalanceState(
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      verificationType: verificationType == freezed
          ? _value.verificationType
          : verificationType as String,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
      accounts:
          accounts == freezed ? _value.accounts : accounts as List<Account>,
    ));
  }
}

class _$_CheckBalanceState
    with DiagnosticableTreeMixin
    implements _CheckBalanceState {
  const _$_CheckBalanceState(
      {@required this.isLoading,
      @required this.isSuccess,
      @required this.isFailed,
      @required this.verificationType,
      @required this.errorMessage,
      @required this.accounts})
      : assert(isLoading != null),
        assert(isSuccess != null),
        assert(isFailed != null),
        assert(verificationType != null),
        assert(errorMessage != null),
        assert(accounts != null);

  @override
  final bool isLoading;
  @override
  final bool isSuccess;
  @override
  final bool isFailed;
  @override
  final String verificationType;
  @override
  final String errorMessage;
  @override
  final List<Account> accounts;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CheckBalanceState(isLoading: $isLoading, isSuccess: $isSuccess, isFailed: $isFailed, verificationType: $verificationType, errorMessage: $errorMessage, accounts: $accounts)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CheckBalanceState'))
      ..add(DiagnosticsProperty('isLoading', isLoading))
      ..add(DiagnosticsProperty('isSuccess', isSuccess))
      ..add(DiagnosticsProperty('isFailed', isFailed))
      ..add(DiagnosticsProperty('verificationType', verificationType))
      ..add(DiagnosticsProperty('errorMessage', errorMessage))
      ..add(DiagnosticsProperty('accounts', accounts));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CheckBalanceState &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)) &&
            (identical(other.isSuccess, isSuccess) ||
                const DeepCollectionEquality()
                    .equals(other.isSuccess, isSuccess)) &&
            (identical(other.isFailed, isFailed) ||
                const DeepCollectionEquality()
                    .equals(other.isFailed, isFailed)) &&
            (identical(other.verificationType, verificationType) ||
                const DeepCollectionEquality()
                    .equals(other.verificationType, verificationType)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)) &&
            (identical(other.accounts, accounts) ||
                const DeepCollectionEquality()
                    .equals(other.accounts, accounts)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isLoading) ^
      const DeepCollectionEquality().hash(isSuccess) ^
      const DeepCollectionEquality().hash(isFailed) ^
      const DeepCollectionEquality().hash(verificationType) ^
      const DeepCollectionEquality().hash(errorMessage) ^
      const DeepCollectionEquality().hash(accounts);

  @override
  _$CheckBalanceStateCopyWith<_CheckBalanceState> get copyWith =>
      __$CheckBalanceStateCopyWithImpl<_CheckBalanceState>(this, _$identity);
}

abstract class _CheckBalanceState implements CheckBalanceState {
  const factory _CheckBalanceState(
      {@required bool isLoading,
      @required bool isSuccess,
      @required bool isFailed,
      @required String verificationType,
      @required String errorMessage,
      @required List<Account> accounts}) = _$_CheckBalanceState;

  @override
  bool get isLoading;
  @override
  bool get isSuccess;
  @override
  bool get isFailed;
  @override
  String get verificationType;
  @override
  String get errorMessage;
  @override
  List<Account> get accounts;
  @override
  _$CheckBalanceStateCopyWith<_CheckBalanceState> get copyWith;
}
