part of 'check_balance_bloc.dart';

@freezed
abstract class CheckBalanceState with _$CheckBalanceState {
  const factory CheckBalanceState({
    @required bool isLoading,
    @required bool isSuccess,
    @required bool isFailed,
    @required String verificationType,
    @required String errorMessage,
    @required List<Account> accounts,
  }) = _CheckBalanceState;

  factory CheckBalanceState.initial() => CheckBalanceState(
    isLoading: false,
    isSuccess: false, 
    isFailed: false,
    verificationType: '',
    errorMessage: '', 
    accounts: []
  );
}