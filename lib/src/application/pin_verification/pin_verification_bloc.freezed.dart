// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'pin_verification_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$PinVerificationEventTearOff {
  const _$PinVerificationEventTearOff();

  ShowForm showForm() {
    return const ShowForm();
  }

  SubmitForm submitForm(String pin) {
    return SubmitForm(
      pin,
    );
  }
}

// ignore: unused_element
const $PinVerificationEvent = _$PinVerificationEventTearOff();

mixin _$PinVerificationEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result showForm(),
    @required Result submitForm(String pin),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result showForm(),
    Result submitForm(String pin),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result showForm(ShowForm value),
    @required Result submitForm(SubmitForm value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result showForm(ShowForm value),
    Result submitForm(SubmitForm value),
    @required Result orElse(),
  });
}

abstract class $PinVerificationEventCopyWith<$Res> {
  factory $PinVerificationEventCopyWith(PinVerificationEvent value,
          $Res Function(PinVerificationEvent) then) =
      _$PinVerificationEventCopyWithImpl<$Res>;
}

class _$PinVerificationEventCopyWithImpl<$Res>
    implements $PinVerificationEventCopyWith<$Res> {
  _$PinVerificationEventCopyWithImpl(this._value, this._then);

  final PinVerificationEvent _value;
  // ignore: unused_field
  final $Res Function(PinVerificationEvent) _then;
}

abstract class $ShowFormCopyWith<$Res> {
  factory $ShowFormCopyWith(ShowForm value, $Res Function(ShowForm) then) =
      _$ShowFormCopyWithImpl<$Res>;
}

class _$ShowFormCopyWithImpl<$Res>
    extends _$PinVerificationEventCopyWithImpl<$Res>
    implements $ShowFormCopyWith<$Res> {
  _$ShowFormCopyWithImpl(ShowForm _value, $Res Function(ShowForm) _then)
      : super(_value, (v) => _then(v as ShowForm));

  @override
  ShowForm get _value => super._value as ShowForm;
}

class _$ShowForm implements ShowForm {
  const _$ShowForm();

  @override
  String toString() {
    return 'PinVerificationEvent.showForm()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ShowForm);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result showForm(),
    @required Result submitForm(String pin),
  }) {
    assert(showForm != null);
    assert(submitForm != null);
    return showForm();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result showForm(),
    Result submitForm(String pin),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (showForm != null) {
      return showForm();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result showForm(ShowForm value),
    @required Result submitForm(SubmitForm value),
  }) {
    assert(showForm != null);
    assert(submitForm != null);
    return showForm(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result showForm(ShowForm value),
    Result submitForm(SubmitForm value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (showForm != null) {
      return showForm(this);
    }
    return orElse();
  }
}

abstract class ShowForm implements PinVerificationEvent {
  const factory ShowForm() = _$ShowForm;
}

abstract class $SubmitFormCopyWith<$Res> {
  factory $SubmitFormCopyWith(
          SubmitForm value, $Res Function(SubmitForm) then) =
      _$SubmitFormCopyWithImpl<$Res>;
  $Res call({String pin});
}

class _$SubmitFormCopyWithImpl<$Res>
    extends _$PinVerificationEventCopyWithImpl<$Res>
    implements $SubmitFormCopyWith<$Res> {
  _$SubmitFormCopyWithImpl(SubmitForm _value, $Res Function(SubmitForm) _then)
      : super(_value, (v) => _then(v as SubmitForm));

  @override
  SubmitForm get _value => super._value as SubmitForm;

  @override
  $Res call({
    Object pin = freezed,
  }) {
    return _then(SubmitForm(
      pin == freezed ? _value.pin : pin as String,
    ));
  }
}

class _$SubmitForm implements SubmitForm {
  const _$SubmitForm(this.pin) : assert(pin != null);

  @override
  final String pin;

  @override
  String toString() {
    return 'PinVerificationEvent.submitForm(pin: $pin)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SubmitForm &&
            (identical(other.pin, pin) ||
                const DeepCollectionEquality().equals(other.pin, pin)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(pin);

  @override
  $SubmitFormCopyWith<SubmitForm> get copyWith =>
      _$SubmitFormCopyWithImpl<SubmitForm>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result showForm(),
    @required Result submitForm(String pin),
  }) {
    assert(showForm != null);
    assert(submitForm != null);
    return submitForm(pin);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result showForm(),
    Result submitForm(String pin),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (submitForm != null) {
      return submitForm(pin);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result showForm(ShowForm value),
    @required Result submitForm(SubmitForm value),
  }) {
    assert(showForm != null);
    assert(submitForm != null);
    return submitForm(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result showForm(ShowForm value),
    Result submitForm(SubmitForm value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (submitForm != null) {
      return submitForm(this);
    }
    return orElse();
  }
}

abstract class SubmitForm implements PinVerificationEvent {
  const factory SubmitForm(String pin) = _$SubmitForm;

  String get pin;
  $SubmitFormCopyWith<SubmitForm> get copyWith;
}

class _$PinVerificationStateTearOff {
  const _$PinVerificationStateTearOff();

  _PinVerificationState call(
      {@required bool isFormShown,
      @required bool isSubmitted,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage}) {
    return _PinVerificationState(
      isFormShown: isFormShown,
      isSubmitted: isSubmitted,
      isSuccess: isSuccess,
      isFailed: isFailed,
      errorMessage: errorMessage,
    );
  }
}

// ignore: unused_element
const $PinVerificationState = _$PinVerificationStateTearOff();

mixin _$PinVerificationState {
  bool get isFormShown;
  bool get isSubmitted;
  bool get isSuccess;
  bool get isFailed;
  String get errorMessage;

  $PinVerificationStateCopyWith<PinVerificationState> get copyWith;
}

abstract class $PinVerificationStateCopyWith<$Res> {
  factory $PinVerificationStateCopyWith(PinVerificationState value,
          $Res Function(PinVerificationState) then) =
      _$PinVerificationStateCopyWithImpl<$Res>;
  $Res call(
      {bool isFormShown,
      bool isSubmitted,
      bool isSuccess,
      bool isFailed,
      String errorMessage});
}

class _$PinVerificationStateCopyWithImpl<$Res>
    implements $PinVerificationStateCopyWith<$Res> {
  _$PinVerificationStateCopyWithImpl(this._value, this._then);

  final PinVerificationState _value;
  // ignore: unused_field
  final $Res Function(PinVerificationState) _then;

  @override
  $Res call({
    Object isFormShown = freezed,
    Object isSubmitted = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      isFormShown:
          isFormShown == freezed ? _value.isFormShown : isFormShown as bool,
      isSubmitted:
          isSubmitted == freezed ? _value.isSubmitted : isSubmitted as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
    ));
  }
}

abstract class _$PinVerificationStateCopyWith<$Res>
    implements $PinVerificationStateCopyWith<$Res> {
  factory _$PinVerificationStateCopyWith(_PinVerificationState value,
          $Res Function(_PinVerificationState) then) =
      __$PinVerificationStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isFormShown,
      bool isSubmitted,
      bool isSuccess,
      bool isFailed,
      String errorMessage});
}

class __$PinVerificationStateCopyWithImpl<$Res>
    extends _$PinVerificationStateCopyWithImpl<$Res>
    implements _$PinVerificationStateCopyWith<$Res> {
  __$PinVerificationStateCopyWithImpl(
      _PinVerificationState _value, $Res Function(_PinVerificationState) _then)
      : super(_value, (v) => _then(v as _PinVerificationState));

  @override
  _PinVerificationState get _value => super._value as _PinVerificationState;

  @override
  $Res call({
    Object isFormShown = freezed,
    Object isSubmitted = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
  }) {
    return _then(_PinVerificationState(
      isFormShown:
          isFormShown == freezed ? _value.isFormShown : isFormShown as bool,
      isSubmitted:
          isSubmitted == freezed ? _value.isSubmitted : isSubmitted as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
    ));
  }
}

class _$_PinVerificationState implements _PinVerificationState {
  const _$_PinVerificationState(
      {@required this.isFormShown,
      @required this.isSubmitted,
      @required this.isSuccess,
      @required this.isFailed,
      @required this.errorMessage})
      : assert(isFormShown != null),
        assert(isSubmitted != null),
        assert(isSuccess != null),
        assert(isFailed != null),
        assert(errorMessage != null);

  @override
  final bool isFormShown;
  @override
  final bool isSubmitted;
  @override
  final bool isSuccess;
  @override
  final bool isFailed;
  @override
  final String errorMessage;

  @override
  String toString() {
    return 'PinVerificationState(isFormShown: $isFormShown, isSubmitted: $isSubmitted, isSuccess: $isSuccess, isFailed: $isFailed, errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PinVerificationState &&
            (identical(other.isFormShown, isFormShown) ||
                const DeepCollectionEquality()
                    .equals(other.isFormShown, isFormShown)) &&
            (identical(other.isSubmitted, isSubmitted) ||
                const DeepCollectionEquality()
                    .equals(other.isSubmitted, isSubmitted)) &&
            (identical(other.isSuccess, isSuccess) ||
                const DeepCollectionEquality()
                    .equals(other.isSuccess, isSuccess)) &&
            (identical(other.isFailed, isFailed) ||
                const DeepCollectionEquality()
                    .equals(other.isFailed, isFailed)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isFormShown) ^
      const DeepCollectionEquality().hash(isSubmitted) ^
      const DeepCollectionEquality().hash(isSuccess) ^
      const DeepCollectionEquality().hash(isFailed) ^
      const DeepCollectionEquality().hash(errorMessage);

  @override
  _$PinVerificationStateCopyWith<_PinVerificationState> get copyWith =>
      __$PinVerificationStateCopyWithImpl<_PinVerificationState>(
          this, _$identity);
}

abstract class _PinVerificationState implements PinVerificationState {
  const factory _PinVerificationState(
      {@required bool isFormShown,
      @required bool isSubmitted,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage}) = _$_PinVerificationState;

  @override
  bool get isFormShown;
  @override
  bool get isSubmitted;
  @override
  bool get isSuccess;
  @override
  bool get isFailed;
  @override
  String get errorMessage;
  @override
  _$PinVerificationStateCopyWith<_PinVerificationState> get copyWith;
}
