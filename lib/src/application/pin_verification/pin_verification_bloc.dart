import 'package:bank_mobile_client/src/injector.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/verification_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'pin_verification_event.dart';
part 'pin_verification_state.dart';
part 'pin_verification_bloc.freezed.dart';

class PinVerificationBloc extends Bloc<PinVerificationEvent, PinVerificationState>{
  @override
  PinVerificationState get initialState => PinVerificationState.initial();

  @override
  Stream<PinVerificationState> mapEventToState(PinVerificationEvent event) async* {
    yield* event.map(
      showForm: (e) async* {
        yield state.copyWith(
          isFormShown: true
        );
      }, 
      submitForm: (e) async* {
        yield* _submitForm(e.pin);
      }
    );
  }

  Stream<PinVerificationState> _submitForm(String pin) async* {
    yield state.copyWith(
      isFormShown: false,
      isSubmitted: true,
      isSuccess: false,
      isFailed: false,
      errorMessage: ''
    );

    try {
      await injector.get<VerificationRepository>().validatePin(pin);
      yield state.copyWith(
        isFormShown: false,
        isSubmitted: false,
        isSuccess: true,
        isFailed: false,
        errorMessage: ''
      );
    } catch(error) {
      yield state.copyWith(
        isFormShown: false,
        isSubmitted: false,
        isSuccess: false,
        isFailed: true,
        errorMessage: error.message
      );
    }
  }
}