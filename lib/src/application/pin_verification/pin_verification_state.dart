part of 'pin_verification_bloc.dart';

@freezed
abstract class PinVerificationState with _$PinVerificationState {
  const factory PinVerificationState({
    @required bool isFormShown,
    @required bool isSubmitted,
    @required bool isSuccess,
    @required bool isFailed,
    @required String errorMessage,
  }) = _PinVerificationState;

  factory PinVerificationState.initial() => PinVerificationState(
    isFormShown: false, 
    isSubmitted: false, 
    isSuccess: false, 
    isFailed: false, 
    errorMessage: ''
  );
}