part of 'pin_verification_bloc.dart';

@freezed
abstract class PinVerificationEvent with _$PinVerificationEvent {
  const factory PinVerificationEvent.showForm() = ShowForm;
  const factory PinVerificationEvent.submitForm(String pin) = SubmitForm;
}