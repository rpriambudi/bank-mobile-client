import 'package:bank_mobile_client/src/injector.dart';
import 'package:bank_mobile_client/src/models/forms/pin.dart';
import 'package:bank_mobile_client/src/simple_validator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/verification_repository.dart';

part 'set_pin_state.dart';
part 'set_pin_event.dart';
part 'set_pin_bloc.freezed.dart';

class SetPinBloc extends Bloc<SetPinEvent, SetPinState>{

  @override
  SetPinState get initialState => SetPinState.initial();

  @override
  Stream<SetPinState> mapEventToState(SetPinEvent event) async* {
    yield* event.map(
      pinChanged: (e) async* {
        yield* _validatePin(e.pin);
      },
      submittingForm: (e) async* {
        yield* _submitPinForm(e.pin);
      },
    );
  }

  Stream<SetPinState> _submitPinForm(String pin) async* {
    yield state.copyWith(
      isSubmitted: true
    );

    /**
     * TODO: username later get from oauth client if login PR is merged
     */
    final PinForm  pinForm = PinForm(username: null, verificationValue: pin);
    try {
      await injector.get<VerificationRepository>().submitPin(pinForm);
      yield state.copyWith(
        isPinValid: false,
        isSubmitted: false,
        isSuccess: true,
        isFailed: false,
        errorMessage: ''
      );
    } catch (error) {
      yield state.copyWith(
        isSubmitted: false,
        isSuccess: false,
        isFailed: true,
        errorMessage: 'Failed to set pin. ' + error.message
      );
    }
  }

  Stream<SetPinState> _validatePin(String pin) async* {
    final bool isValid = SimpleValidator.isPinValid(pin);
    yield state.copyWith(
      isPinValid: isValid
    );
  }
}