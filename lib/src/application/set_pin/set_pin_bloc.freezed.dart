// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'set_pin_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$SetPinStateTearOff {
  const _$SetPinStateTearOff();

  _SetPinState call(
      {@required bool isPinValid,
      @required bool isSubmitted,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage}) {
    return _SetPinState(
      isPinValid: isPinValid,
      isSubmitted: isSubmitted,
      isSuccess: isSuccess,
      isFailed: isFailed,
      errorMessage: errorMessage,
    );
  }
}

// ignore: unused_element
const $SetPinState = _$SetPinStateTearOff();

mixin _$SetPinState {
  bool get isPinValid;
  bool get isSubmitted;
  bool get isSuccess;
  bool get isFailed;
  String get errorMessage;

  $SetPinStateCopyWith<SetPinState> get copyWith;
}

abstract class $SetPinStateCopyWith<$Res> {
  factory $SetPinStateCopyWith(
          SetPinState value, $Res Function(SetPinState) then) =
      _$SetPinStateCopyWithImpl<$Res>;
  $Res call(
      {bool isPinValid,
      bool isSubmitted,
      bool isSuccess,
      bool isFailed,
      String errorMessage});
}

class _$SetPinStateCopyWithImpl<$Res> implements $SetPinStateCopyWith<$Res> {
  _$SetPinStateCopyWithImpl(this._value, this._then);

  final SetPinState _value;
  // ignore: unused_field
  final $Res Function(SetPinState) _then;

  @override
  $Res call({
    Object isPinValid = freezed,
    Object isSubmitted = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      isPinValid:
          isPinValid == freezed ? _value.isPinValid : isPinValid as bool,
      isSubmitted:
          isSubmitted == freezed ? _value.isSubmitted : isSubmitted as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
    ));
  }
}

abstract class _$SetPinStateCopyWith<$Res>
    implements $SetPinStateCopyWith<$Res> {
  factory _$SetPinStateCopyWith(
          _SetPinState value, $Res Function(_SetPinState) then) =
      __$SetPinStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isPinValid,
      bool isSubmitted,
      bool isSuccess,
      bool isFailed,
      String errorMessage});
}

class __$SetPinStateCopyWithImpl<$Res> extends _$SetPinStateCopyWithImpl<$Res>
    implements _$SetPinStateCopyWith<$Res> {
  __$SetPinStateCopyWithImpl(
      _SetPinState _value, $Res Function(_SetPinState) _then)
      : super(_value, (v) => _then(v as _SetPinState));

  @override
  _SetPinState get _value => super._value as _SetPinState;

  @override
  $Res call({
    Object isPinValid = freezed,
    Object isSubmitted = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
  }) {
    return _then(_SetPinState(
      isPinValid:
          isPinValid == freezed ? _value.isPinValid : isPinValid as bool,
      isSubmitted:
          isSubmitted == freezed ? _value.isSubmitted : isSubmitted as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
    ));
  }
}

class _$_SetPinState with DiagnosticableTreeMixin implements _SetPinState {
  const _$_SetPinState(
      {@required this.isPinValid,
      @required this.isSubmitted,
      @required this.isSuccess,
      @required this.isFailed,
      @required this.errorMessage})
      : assert(isPinValid != null),
        assert(isSubmitted != null),
        assert(isSuccess != null),
        assert(isFailed != null),
        assert(errorMessage != null);

  @override
  final bool isPinValid;
  @override
  final bool isSubmitted;
  @override
  final bool isSuccess;
  @override
  final bool isFailed;
  @override
  final String errorMessage;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SetPinState(isPinValid: $isPinValid, isSubmitted: $isSubmitted, isSuccess: $isSuccess, isFailed: $isFailed, errorMessage: $errorMessage)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SetPinState'))
      ..add(DiagnosticsProperty('isPinValid', isPinValid))
      ..add(DiagnosticsProperty('isSubmitted', isSubmitted))
      ..add(DiagnosticsProperty('isSuccess', isSuccess))
      ..add(DiagnosticsProperty('isFailed', isFailed))
      ..add(DiagnosticsProperty('errorMessage', errorMessage));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SetPinState &&
            (identical(other.isPinValid, isPinValid) ||
                const DeepCollectionEquality()
                    .equals(other.isPinValid, isPinValid)) &&
            (identical(other.isSubmitted, isSubmitted) ||
                const DeepCollectionEquality()
                    .equals(other.isSubmitted, isSubmitted)) &&
            (identical(other.isSuccess, isSuccess) ||
                const DeepCollectionEquality()
                    .equals(other.isSuccess, isSuccess)) &&
            (identical(other.isFailed, isFailed) ||
                const DeepCollectionEquality()
                    .equals(other.isFailed, isFailed)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isPinValid) ^
      const DeepCollectionEquality().hash(isSubmitted) ^
      const DeepCollectionEquality().hash(isSuccess) ^
      const DeepCollectionEquality().hash(isFailed) ^
      const DeepCollectionEquality().hash(errorMessage);

  @override
  _$SetPinStateCopyWith<_SetPinState> get copyWith =>
      __$SetPinStateCopyWithImpl<_SetPinState>(this, _$identity);
}

abstract class _SetPinState implements SetPinState {
  const factory _SetPinState(
      {@required bool isPinValid,
      @required bool isSubmitted,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage}) = _$_SetPinState;

  @override
  bool get isPinValid;
  @override
  bool get isSubmitted;
  @override
  bool get isSuccess;
  @override
  bool get isFailed;
  @override
  String get errorMessage;
  @override
  _$SetPinStateCopyWith<_SetPinState> get copyWith;
}

class _$SetPinEventTearOff {
  const _$SetPinEventTearOff();

  PinChanged pinChanged(String pin) {
    return PinChanged(
      pin,
    );
  }

  SubmittingForm submittingForm(String pin) {
    return SubmittingForm(
      pin,
    );
  }
}

// ignore: unused_element
const $SetPinEvent = _$SetPinEventTearOff();

mixin _$SetPinEvent {
  String get pin;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result pinChanged(String pin),
    @required Result submittingForm(String pin),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result pinChanged(String pin),
    Result submittingForm(String pin),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result pinChanged(PinChanged value),
    @required Result submittingForm(SubmittingForm value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result pinChanged(PinChanged value),
    Result submittingForm(SubmittingForm value),
    @required Result orElse(),
  });

  $SetPinEventCopyWith<SetPinEvent> get copyWith;
}

abstract class $SetPinEventCopyWith<$Res> {
  factory $SetPinEventCopyWith(
          SetPinEvent value, $Res Function(SetPinEvent) then) =
      _$SetPinEventCopyWithImpl<$Res>;
  $Res call({String pin});
}

class _$SetPinEventCopyWithImpl<$Res> implements $SetPinEventCopyWith<$Res> {
  _$SetPinEventCopyWithImpl(this._value, this._then);

  final SetPinEvent _value;
  // ignore: unused_field
  final $Res Function(SetPinEvent) _then;

  @override
  $Res call({
    Object pin = freezed,
  }) {
    return _then(_value.copyWith(
      pin: pin == freezed ? _value.pin : pin as String,
    ));
  }
}

abstract class $PinChangedCopyWith<$Res> implements $SetPinEventCopyWith<$Res> {
  factory $PinChangedCopyWith(
          PinChanged value, $Res Function(PinChanged) then) =
      _$PinChangedCopyWithImpl<$Res>;
  @override
  $Res call({String pin});
}

class _$PinChangedCopyWithImpl<$Res> extends _$SetPinEventCopyWithImpl<$Res>
    implements $PinChangedCopyWith<$Res> {
  _$PinChangedCopyWithImpl(PinChanged _value, $Res Function(PinChanged) _then)
      : super(_value, (v) => _then(v as PinChanged));

  @override
  PinChanged get _value => super._value as PinChanged;

  @override
  $Res call({
    Object pin = freezed,
  }) {
    return _then(PinChanged(
      pin == freezed ? _value.pin : pin as String,
    ));
  }
}

class _$PinChanged with DiagnosticableTreeMixin implements PinChanged {
  const _$PinChanged(this.pin) : assert(pin != null);

  @override
  final String pin;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SetPinEvent.pinChanged(pin: $pin)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SetPinEvent.pinChanged'))
      ..add(DiagnosticsProperty('pin', pin));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PinChanged &&
            (identical(other.pin, pin) ||
                const DeepCollectionEquality().equals(other.pin, pin)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(pin);

  @override
  $PinChangedCopyWith<PinChanged> get copyWith =>
      _$PinChangedCopyWithImpl<PinChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result pinChanged(String pin),
    @required Result submittingForm(String pin),
  }) {
    assert(pinChanged != null);
    assert(submittingForm != null);
    return pinChanged(pin);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result pinChanged(String pin),
    Result submittingForm(String pin),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (pinChanged != null) {
      return pinChanged(pin);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result pinChanged(PinChanged value),
    @required Result submittingForm(SubmittingForm value),
  }) {
    assert(pinChanged != null);
    assert(submittingForm != null);
    return pinChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result pinChanged(PinChanged value),
    Result submittingForm(SubmittingForm value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (pinChanged != null) {
      return pinChanged(this);
    }
    return orElse();
  }
}

abstract class PinChanged implements SetPinEvent {
  const factory PinChanged(String pin) = _$PinChanged;

  @override
  String get pin;
  @override
  $PinChangedCopyWith<PinChanged> get copyWith;
}

abstract class $SubmittingFormCopyWith<$Res>
    implements $SetPinEventCopyWith<$Res> {
  factory $SubmittingFormCopyWith(
          SubmittingForm value, $Res Function(SubmittingForm) then) =
      _$SubmittingFormCopyWithImpl<$Res>;
  @override
  $Res call({String pin});
}

class _$SubmittingFormCopyWithImpl<$Res> extends _$SetPinEventCopyWithImpl<$Res>
    implements $SubmittingFormCopyWith<$Res> {
  _$SubmittingFormCopyWithImpl(
      SubmittingForm _value, $Res Function(SubmittingForm) _then)
      : super(_value, (v) => _then(v as SubmittingForm));

  @override
  SubmittingForm get _value => super._value as SubmittingForm;

  @override
  $Res call({
    Object pin = freezed,
  }) {
    return _then(SubmittingForm(
      pin == freezed ? _value.pin : pin as String,
    ));
  }
}

class _$SubmittingForm with DiagnosticableTreeMixin implements SubmittingForm {
  const _$SubmittingForm(this.pin) : assert(pin != null);

  @override
  final String pin;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SetPinEvent.submittingForm(pin: $pin)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SetPinEvent.submittingForm'))
      ..add(DiagnosticsProperty('pin', pin));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SubmittingForm &&
            (identical(other.pin, pin) ||
                const DeepCollectionEquality().equals(other.pin, pin)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(pin);

  @override
  $SubmittingFormCopyWith<SubmittingForm> get copyWith =>
      _$SubmittingFormCopyWithImpl<SubmittingForm>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result pinChanged(String pin),
    @required Result submittingForm(String pin),
  }) {
    assert(pinChanged != null);
    assert(submittingForm != null);
    return submittingForm(pin);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result pinChanged(String pin),
    Result submittingForm(String pin),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (submittingForm != null) {
      return submittingForm(pin);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result pinChanged(PinChanged value),
    @required Result submittingForm(SubmittingForm value),
  }) {
    assert(pinChanged != null);
    assert(submittingForm != null);
    return submittingForm(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result pinChanged(PinChanged value),
    Result submittingForm(SubmittingForm value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (submittingForm != null) {
      return submittingForm(this);
    }
    return orElse();
  }
}

abstract class SubmittingForm implements SetPinEvent {
  const factory SubmittingForm(String pin) = _$SubmittingForm;

  @override
  String get pin;
  @override
  $SubmittingFormCopyWith<SubmittingForm> get copyWith;
}
