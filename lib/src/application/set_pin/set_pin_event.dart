part of 'set_pin_bloc.dart';

@freezed
abstract class SetPinEvent with _$SetPinEvent {
  const factory SetPinEvent.pinChanged(String pin) = PinChanged;
  const factory SetPinEvent.submittingForm(String pin) = SubmittingForm;
}