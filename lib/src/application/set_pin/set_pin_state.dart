part of 'set_pin_bloc.dart';

@freezed
abstract class SetPinState with _$SetPinState {
  const factory SetPinState({
    @required bool isPinValid,
    @required bool isSubmitted,
    @required bool isSuccess,
    @required bool isFailed,
    @required String errorMessage
  }) = _SetPinState;
  
  factory SetPinState.initial() => SetPinState(
    isPinValid: false, 
    isSubmitted: false,
    isSuccess: false, 
    isFailed: false, 
    errorMessage: ''
  );
}