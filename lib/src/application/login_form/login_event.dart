part of 'login_bloc.dart';

@freezed
abstract class LoginEvent with _$LoginEvent {
  const factory LoginEvent.initializeForm() = InitializeForm;
  const factory LoginEvent.requestAuthentication() = RequestAuthentication;
}

