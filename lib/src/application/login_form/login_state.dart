part of 'login_bloc.dart';

@freezed
abstract class LoginState with _$LoginState {  
  const factory LoginState.formNotInitialized() = FormNotInitialized;
  const factory LoginState.formInitialized(String authorizationUrl) = FormInitialized;
  const factory LoginState.authSuccessful() = AuthSuccessful;
}
