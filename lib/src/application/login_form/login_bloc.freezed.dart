// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'login_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$LoginEventTearOff {
  const _$LoginEventTearOff();

  InitializeForm initializeForm() {
    return const InitializeForm();
  }

  RequestAuthentication requestAuthentication() {
    return const RequestAuthentication();
  }
}

// ignore: unused_element
const $LoginEvent = _$LoginEventTearOff();

mixin _$LoginEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initializeForm(),
    @required Result requestAuthentication(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initializeForm(),
    Result requestAuthentication(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initializeForm(InitializeForm value),
    @required Result requestAuthentication(RequestAuthentication value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initializeForm(InitializeForm value),
    Result requestAuthentication(RequestAuthentication value),
    @required Result orElse(),
  });
}

abstract class $LoginEventCopyWith<$Res> {
  factory $LoginEventCopyWith(
          LoginEvent value, $Res Function(LoginEvent) then) =
      _$LoginEventCopyWithImpl<$Res>;
}

class _$LoginEventCopyWithImpl<$Res> implements $LoginEventCopyWith<$Res> {
  _$LoginEventCopyWithImpl(this._value, this._then);

  final LoginEvent _value;
  // ignore: unused_field
  final $Res Function(LoginEvent) _then;
}

abstract class $InitializeFormCopyWith<$Res> {
  factory $InitializeFormCopyWith(
          InitializeForm value, $Res Function(InitializeForm) then) =
      _$InitializeFormCopyWithImpl<$Res>;
}

class _$InitializeFormCopyWithImpl<$Res> extends _$LoginEventCopyWithImpl<$Res>
    implements $InitializeFormCopyWith<$Res> {
  _$InitializeFormCopyWithImpl(
      InitializeForm _value, $Res Function(InitializeForm) _then)
      : super(_value, (v) => _then(v as InitializeForm));

  @override
  InitializeForm get _value => super._value as InitializeForm;
}

class _$InitializeForm with DiagnosticableTreeMixin implements InitializeForm {
  const _$InitializeForm();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'LoginEvent.initializeForm()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'LoginEvent.initializeForm'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is InitializeForm);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initializeForm(),
    @required Result requestAuthentication(),
  }) {
    assert(initializeForm != null);
    assert(requestAuthentication != null);
    return initializeForm();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initializeForm(),
    Result requestAuthentication(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initializeForm != null) {
      return initializeForm();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initializeForm(InitializeForm value),
    @required Result requestAuthentication(RequestAuthentication value),
  }) {
    assert(initializeForm != null);
    assert(requestAuthentication != null);
    return initializeForm(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initializeForm(InitializeForm value),
    Result requestAuthentication(RequestAuthentication value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initializeForm != null) {
      return initializeForm(this);
    }
    return orElse();
  }
}

abstract class InitializeForm implements LoginEvent {
  const factory InitializeForm() = _$InitializeForm;
}

abstract class $RequestAuthenticationCopyWith<$Res> {
  factory $RequestAuthenticationCopyWith(RequestAuthentication value,
          $Res Function(RequestAuthentication) then) =
      _$RequestAuthenticationCopyWithImpl<$Res>;
}

class _$RequestAuthenticationCopyWithImpl<$Res>
    extends _$LoginEventCopyWithImpl<$Res>
    implements $RequestAuthenticationCopyWith<$Res> {
  _$RequestAuthenticationCopyWithImpl(
      RequestAuthentication _value, $Res Function(RequestAuthentication) _then)
      : super(_value, (v) => _then(v as RequestAuthentication));

  @override
  RequestAuthentication get _value => super._value as RequestAuthentication;
}

class _$RequestAuthentication
    with DiagnosticableTreeMixin
    implements RequestAuthentication {
  const _$RequestAuthentication();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'LoginEvent.requestAuthentication()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'LoginEvent.requestAuthentication'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is RequestAuthentication);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initializeForm(),
    @required Result requestAuthentication(),
  }) {
    assert(initializeForm != null);
    assert(requestAuthentication != null);
    return requestAuthentication();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initializeForm(),
    Result requestAuthentication(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (requestAuthentication != null) {
      return requestAuthentication();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initializeForm(InitializeForm value),
    @required Result requestAuthentication(RequestAuthentication value),
  }) {
    assert(initializeForm != null);
    assert(requestAuthentication != null);
    return requestAuthentication(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initializeForm(InitializeForm value),
    Result requestAuthentication(RequestAuthentication value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (requestAuthentication != null) {
      return requestAuthentication(this);
    }
    return orElse();
  }
}

abstract class RequestAuthentication implements LoginEvent {
  const factory RequestAuthentication() = _$RequestAuthentication;
}

class _$LoginStateTearOff {
  const _$LoginStateTearOff();

  FormNotInitialized formNotInitialized() {
    return const FormNotInitialized();
  }

  FormInitialized formInitialized(String authorizationUrl) {
    return FormInitialized(
      authorizationUrl,
    );
  }

  AuthSuccessful authSuccessful() {
    return const AuthSuccessful();
  }
}

// ignore: unused_element
const $LoginState = _$LoginStateTearOff();

mixin _$LoginState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result formNotInitialized(),
    @required Result formInitialized(String authorizationUrl),
    @required Result authSuccessful(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result formNotInitialized(),
    Result formInitialized(String authorizationUrl),
    Result authSuccessful(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result formNotInitialized(FormNotInitialized value),
    @required Result formInitialized(FormInitialized value),
    @required Result authSuccessful(AuthSuccessful value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result formNotInitialized(FormNotInitialized value),
    Result formInitialized(FormInitialized value),
    Result authSuccessful(AuthSuccessful value),
    @required Result orElse(),
  });
}

abstract class $LoginStateCopyWith<$Res> {
  factory $LoginStateCopyWith(
          LoginState value, $Res Function(LoginState) then) =
      _$LoginStateCopyWithImpl<$Res>;
}

class _$LoginStateCopyWithImpl<$Res> implements $LoginStateCopyWith<$Res> {
  _$LoginStateCopyWithImpl(this._value, this._then);

  final LoginState _value;
  // ignore: unused_field
  final $Res Function(LoginState) _then;
}

abstract class $FormNotInitializedCopyWith<$Res> {
  factory $FormNotInitializedCopyWith(
          FormNotInitialized value, $Res Function(FormNotInitialized) then) =
      _$FormNotInitializedCopyWithImpl<$Res>;
}

class _$FormNotInitializedCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res>
    implements $FormNotInitializedCopyWith<$Res> {
  _$FormNotInitializedCopyWithImpl(
      FormNotInitialized _value, $Res Function(FormNotInitialized) _then)
      : super(_value, (v) => _then(v as FormNotInitialized));

  @override
  FormNotInitialized get _value => super._value as FormNotInitialized;
}

class _$FormNotInitialized
    with DiagnosticableTreeMixin
    implements FormNotInitialized {
  const _$FormNotInitialized();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'LoginState.formNotInitialized()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'LoginState.formNotInitialized'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FormNotInitialized);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result formNotInitialized(),
    @required Result formInitialized(String authorizationUrl),
    @required Result authSuccessful(),
  }) {
    assert(formNotInitialized != null);
    assert(formInitialized != null);
    assert(authSuccessful != null);
    return formNotInitialized();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result formNotInitialized(),
    Result formInitialized(String authorizationUrl),
    Result authSuccessful(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (formNotInitialized != null) {
      return formNotInitialized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result formNotInitialized(FormNotInitialized value),
    @required Result formInitialized(FormInitialized value),
    @required Result authSuccessful(AuthSuccessful value),
  }) {
    assert(formNotInitialized != null);
    assert(formInitialized != null);
    assert(authSuccessful != null);
    return formNotInitialized(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result formNotInitialized(FormNotInitialized value),
    Result formInitialized(FormInitialized value),
    Result authSuccessful(AuthSuccessful value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (formNotInitialized != null) {
      return formNotInitialized(this);
    }
    return orElse();
  }
}

abstract class FormNotInitialized implements LoginState {
  const factory FormNotInitialized() = _$FormNotInitialized;
}

abstract class $FormInitializedCopyWith<$Res> {
  factory $FormInitializedCopyWith(
          FormInitialized value, $Res Function(FormInitialized) then) =
      _$FormInitializedCopyWithImpl<$Res>;
  $Res call({String authorizationUrl});
}

class _$FormInitializedCopyWithImpl<$Res> extends _$LoginStateCopyWithImpl<$Res>
    implements $FormInitializedCopyWith<$Res> {
  _$FormInitializedCopyWithImpl(
      FormInitialized _value, $Res Function(FormInitialized) _then)
      : super(_value, (v) => _then(v as FormInitialized));

  @override
  FormInitialized get _value => super._value as FormInitialized;

  @override
  $Res call({
    Object authorizationUrl = freezed,
  }) {
    return _then(FormInitialized(
      authorizationUrl == freezed
          ? _value.authorizationUrl
          : authorizationUrl as String,
    ));
  }
}

class _$FormInitialized
    with DiagnosticableTreeMixin
    implements FormInitialized {
  const _$FormInitialized(this.authorizationUrl)
      : assert(authorizationUrl != null);

  @override
  final String authorizationUrl;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'LoginState.formInitialized(authorizationUrl: $authorizationUrl)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'LoginState.formInitialized'))
      ..add(DiagnosticsProperty('authorizationUrl', authorizationUrl));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is FormInitialized &&
            (identical(other.authorizationUrl, authorizationUrl) ||
                const DeepCollectionEquality()
                    .equals(other.authorizationUrl, authorizationUrl)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(authorizationUrl);

  @override
  $FormInitializedCopyWith<FormInitialized> get copyWith =>
      _$FormInitializedCopyWithImpl<FormInitialized>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result formNotInitialized(),
    @required Result formInitialized(String authorizationUrl),
    @required Result authSuccessful(),
  }) {
    assert(formNotInitialized != null);
    assert(formInitialized != null);
    assert(authSuccessful != null);
    return formInitialized(authorizationUrl);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result formNotInitialized(),
    Result formInitialized(String authorizationUrl),
    Result authSuccessful(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (formInitialized != null) {
      return formInitialized(authorizationUrl);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result formNotInitialized(FormNotInitialized value),
    @required Result formInitialized(FormInitialized value),
    @required Result authSuccessful(AuthSuccessful value),
  }) {
    assert(formNotInitialized != null);
    assert(formInitialized != null);
    assert(authSuccessful != null);
    return formInitialized(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result formNotInitialized(FormNotInitialized value),
    Result formInitialized(FormInitialized value),
    Result authSuccessful(AuthSuccessful value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (formInitialized != null) {
      return formInitialized(this);
    }
    return orElse();
  }
}

abstract class FormInitialized implements LoginState {
  const factory FormInitialized(String authorizationUrl) = _$FormInitialized;

  String get authorizationUrl;
  $FormInitializedCopyWith<FormInitialized> get copyWith;
}

abstract class $AuthSuccessfulCopyWith<$Res> {
  factory $AuthSuccessfulCopyWith(
          AuthSuccessful value, $Res Function(AuthSuccessful) then) =
      _$AuthSuccessfulCopyWithImpl<$Res>;
}

class _$AuthSuccessfulCopyWithImpl<$Res> extends _$LoginStateCopyWithImpl<$Res>
    implements $AuthSuccessfulCopyWith<$Res> {
  _$AuthSuccessfulCopyWithImpl(
      AuthSuccessful _value, $Res Function(AuthSuccessful) _then)
      : super(_value, (v) => _then(v as AuthSuccessful));

  @override
  AuthSuccessful get _value => super._value as AuthSuccessful;
}

class _$AuthSuccessful with DiagnosticableTreeMixin implements AuthSuccessful {
  const _$AuthSuccessful();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'LoginState.authSuccessful()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'LoginState.authSuccessful'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is AuthSuccessful);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result formNotInitialized(),
    @required Result formInitialized(String authorizationUrl),
    @required Result authSuccessful(),
  }) {
    assert(formNotInitialized != null);
    assert(formInitialized != null);
    assert(authSuccessful != null);
    return authSuccessful();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result formNotInitialized(),
    Result formInitialized(String authorizationUrl),
    Result authSuccessful(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (authSuccessful != null) {
      return authSuccessful();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result formNotInitialized(FormNotInitialized value),
    @required Result formInitialized(FormInitialized value),
    @required Result authSuccessful(AuthSuccessful value),
  }) {
    assert(formNotInitialized != null);
    assert(formInitialized != null);
    assert(authSuccessful != null);
    return authSuccessful(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result formNotInitialized(FormNotInitialized value),
    Result formInitialized(FormInitialized value),
    Result authSuccessful(AuthSuccessful value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (authSuccessful != null) {
      return authSuccessful(this);
    }
    return orElse();
  }
}

abstract class AuthSuccessful implements LoginState {
  const factory AuthSuccessful() = _$AuthSuccessful;
}
