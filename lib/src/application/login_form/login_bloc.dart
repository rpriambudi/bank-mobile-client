import 'package:bank_mobile_client/src/providers/auth.dart';
import 'package:bank_mobile_client/src/shared/constants.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'login_event.dart';
part 'login_state.dart';
part 'login_bloc.freezed.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthProvider _authProvider = AuthProvider();
  final Constants _constants = Constants();

  @override
  LoginState get initialState => FormNotInitialized();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    yield* event.map(
      initializeForm: (e) async* {
        yield* _initializeForm();
      },  
      requestAuthentication: (e) async* {
        yield* _requestAuthentication();
      }
    );
  }

  Stream<LoginState> _initializeForm() async* {
    _authProvider.initializeGrant();
    yield FormInitialized(_authProvider.getAuthorizationUrl());
  }

  Stream<LoginState> _requestAuthentication() async* {
    final webview = FlutterWebviewPlugin();
    bool authenticationSuccess = false;

    if (webview != null) {
      await for (String url in webview.onUrlChanged) {
        if (url.contains(_constants.authCallbackUrl.toString())) {
          final codeUri = Uri.parse(url);
          if (codeUri.queryParameters['code'] != null) {
            webview.dispose();
            await _authProvider.setClientFromCode(codeUri.queryParameters['code']);
            authenticationSuccess = true;
          }
        }
      }
    }

    if (authenticationSuccess) {
      yield AuthSuccessful();
    }
  }
}
