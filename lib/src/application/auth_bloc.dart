import 'package:bank_mobile_client/src/models/token.dart';
import 'package:bank_mobile_client/src/providers/auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'auth_event.dart';
part 'auth_state.dart';

part 'auth_bloc.freezed.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthProvider _authProvider = AuthProvider();

  @override
  AuthState get initialState => AuthState.initial();

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    yield* event.map(
      checkUserSignedIn: (e) async* {
        yield* _getUserData();
      },
      signedOut: (e) async* {
        yield* _signoffUser();
      },
      closeClient: (e) async* {
        yield* _closingClient();
      }
    );
  }

  Stream<AuthState> _getUserData() async* {
    final Token userData = await _authProvider.getTokenData();
    if (userData != null) {
      yield Authenticated(userData);
    } else {
      yield Unauthenticated();
    }
  }

  Stream<AuthState> _signoffUser() async* {
    await _authProvider.signOff();
    yield Unauthenticated();
  }

  Stream<AuthState> _closingClient() async* {
    _authProvider.close();
    yield Unauthenticated();
  }
}