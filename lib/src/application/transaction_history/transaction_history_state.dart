part of 'transaction_history_bloc.dart';

@freezed
abstract class TransactionHistoryState with _$TransactionHistoryState {
  const factory TransactionHistoryState({
    @required bool isLoading,
    @required bool isSuccess,
    @required bool isFailed,
    @required String errorMessage,
    @required String verificationType,
    @required List<Transaction> transactions,
  }) = _TransactionHistoryState;

  factory TransactionHistoryState.initial() => TransactionHistoryState(
    isLoading: false, 
    isSuccess: false, 
    isFailed: false, 
    errorMessage: '', 
    verificationType: '',
    transactions: [],
  );
}