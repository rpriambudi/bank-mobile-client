// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'transaction_history_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$TransactionHistoryEventTearOff {
  const _$TransactionHistoryEventTearOff();

  Initialization initialization() {
    return const Initialization();
  }

  RequestHistory requestHistory(String type) {
    return RequestHistory(
      type,
    );
  }
}

// ignore: unused_element
const $TransactionHistoryEvent = _$TransactionHistoryEventTearOff();

mixin _$TransactionHistoryEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initialization(),
    @required Result requestHistory(String type),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initialization(),
    Result requestHistory(String type),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initialization(Initialization value),
    @required Result requestHistory(RequestHistory value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initialization(Initialization value),
    Result requestHistory(RequestHistory value),
    @required Result orElse(),
  });
}

abstract class $TransactionHistoryEventCopyWith<$Res> {
  factory $TransactionHistoryEventCopyWith(TransactionHistoryEvent value,
          $Res Function(TransactionHistoryEvent) then) =
      _$TransactionHistoryEventCopyWithImpl<$Res>;
}

class _$TransactionHistoryEventCopyWithImpl<$Res>
    implements $TransactionHistoryEventCopyWith<$Res> {
  _$TransactionHistoryEventCopyWithImpl(this._value, this._then);

  final TransactionHistoryEvent _value;
  // ignore: unused_field
  final $Res Function(TransactionHistoryEvent) _then;
}

abstract class $InitializationCopyWith<$Res> {
  factory $InitializationCopyWith(
          Initialization value, $Res Function(Initialization) then) =
      _$InitializationCopyWithImpl<$Res>;
}

class _$InitializationCopyWithImpl<$Res>
    extends _$TransactionHistoryEventCopyWithImpl<$Res>
    implements $InitializationCopyWith<$Res> {
  _$InitializationCopyWithImpl(
      Initialization _value, $Res Function(Initialization) _then)
      : super(_value, (v) => _then(v as Initialization));

  @override
  Initialization get _value => super._value as Initialization;
}

class _$Initialization with DiagnosticableTreeMixin implements Initialization {
  const _$Initialization();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'TransactionHistoryEvent.initialization()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty(
          'type', 'TransactionHistoryEvent.initialization'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Initialization);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initialization(),
    @required Result requestHistory(String type),
  }) {
    assert(initialization != null);
    assert(requestHistory != null);
    return initialization();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initialization(),
    Result requestHistory(String type),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initialization != null) {
      return initialization();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initialization(Initialization value),
    @required Result requestHistory(RequestHistory value),
  }) {
    assert(initialization != null);
    assert(requestHistory != null);
    return initialization(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initialization(Initialization value),
    Result requestHistory(RequestHistory value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initialization != null) {
      return initialization(this);
    }
    return orElse();
  }
}

abstract class Initialization implements TransactionHistoryEvent {
  const factory Initialization() = _$Initialization;
}

abstract class $RequestHistoryCopyWith<$Res> {
  factory $RequestHistoryCopyWith(
          RequestHistory value, $Res Function(RequestHistory) then) =
      _$RequestHistoryCopyWithImpl<$Res>;
  $Res call({String type});
}

class _$RequestHistoryCopyWithImpl<$Res>
    extends _$TransactionHistoryEventCopyWithImpl<$Res>
    implements $RequestHistoryCopyWith<$Res> {
  _$RequestHistoryCopyWithImpl(
      RequestHistory _value, $Res Function(RequestHistory) _then)
      : super(_value, (v) => _then(v as RequestHistory));

  @override
  RequestHistory get _value => super._value as RequestHistory;

  @override
  $Res call({
    Object type = freezed,
  }) {
    return _then(RequestHistory(
      type == freezed ? _value.type : type as String,
    ));
  }
}

class _$RequestHistory with DiagnosticableTreeMixin implements RequestHistory {
  const _$RequestHistory(this.type) : assert(type != null);

  @override
  final String type;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'TransactionHistoryEvent.requestHistory(type: $type)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(
          DiagnosticsProperty('type', 'TransactionHistoryEvent.requestHistory'))
      ..add(DiagnosticsProperty('type', type));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is RequestHistory &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(type);

  @override
  $RequestHistoryCopyWith<RequestHistory> get copyWith =>
      _$RequestHistoryCopyWithImpl<RequestHistory>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initialization(),
    @required Result requestHistory(String type),
  }) {
    assert(initialization != null);
    assert(requestHistory != null);
    return requestHistory(type);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initialization(),
    Result requestHistory(String type),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (requestHistory != null) {
      return requestHistory(type);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initialization(Initialization value),
    @required Result requestHistory(RequestHistory value),
  }) {
    assert(initialization != null);
    assert(requestHistory != null);
    return requestHistory(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initialization(Initialization value),
    Result requestHistory(RequestHistory value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (requestHistory != null) {
      return requestHistory(this);
    }
    return orElse();
  }
}

abstract class RequestHistory implements TransactionHistoryEvent {
  const factory RequestHistory(String type) = _$RequestHistory;

  String get type;
  $RequestHistoryCopyWith<RequestHistory> get copyWith;
}

class _$TransactionHistoryStateTearOff {
  const _$TransactionHistoryStateTearOff();

  _TransactionHistoryState call(
      {@required bool isLoading,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage,
      @required String verificationType,
      @required List<Transaction> transactions}) {
    return _TransactionHistoryState(
      isLoading: isLoading,
      isSuccess: isSuccess,
      isFailed: isFailed,
      errorMessage: errorMessage,
      verificationType: verificationType,
      transactions: transactions,
    );
  }
}

// ignore: unused_element
const $TransactionHistoryState = _$TransactionHistoryStateTearOff();

mixin _$TransactionHistoryState {
  bool get isLoading;
  bool get isSuccess;
  bool get isFailed;
  String get errorMessage;
  String get verificationType;
  List<Transaction> get transactions;

  $TransactionHistoryStateCopyWith<TransactionHistoryState> get copyWith;
}

abstract class $TransactionHistoryStateCopyWith<$Res> {
  factory $TransactionHistoryStateCopyWith(TransactionHistoryState value,
          $Res Function(TransactionHistoryState) then) =
      _$TransactionHistoryStateCopyWithImpl<$Res>;
  $Res call(
      {bool isLoading,
      bool isSuccess,
      bool isFailed,
      String errorMessage,
      String verificationType,
      List<Transaction> transactions});
}

class _$TransactionHistoryStateCopyWithImpl<$Res>
    implements $TransactionHistoryStateCopyWith<$Res> {
  _$TransactionHistoryStateCopyWithImpl(this._value, this._then);

  final TransactionHistoryState _value;
  // ignore: unused_field
  final $Res Function(TransactionHistoryState) _then;

  @override
  $Res call({
    Object isLoading = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
    Object verificationType = freezed,
    Object transactions = freezed,
  }) {
    return _then(_value.copyWith(
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
      verificationType: verificationType == freezed
          ? _value.verificationType
          : verificationType as String,
      transactions: transactions == freezed
          ? _value.transactions
          : transactions as List<Transaction>,
    ));
  }
}

abstract class _$TransactionHistoryStateCopyWith<$Res>
    implements $TransactionHistoryStateCopyWith<$Res> {
  factory _$TransactionHistoryStateCopyWith(_TransactionHistoryState value,
          $Res Function(_TransactionHistoryState) then) =
      __$TransactionHistoryStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isLoading,
      bool isSuccess,
      bool isFailed,
      String errorMessage,
      String verificationType,
      List<Transaction> transactions});
}

class __$TransactionHistoryStateCopyWithImpl<$Res>
    extends _$TransactionHistoryStateCopyWithImpl<$Res>
    implements _$TransactionHistoryStateCopyWith<$Res> {
  __$TransactionHistoryStateCopyWithImpl(_TransactionHistoryState _value,
      $Res Function(_TransactionHistoryState) _then)
      : super(_value, (v) => _then(v as _TransactionHistoryState));

  @override
  _TransactionHistoryState get _value =>
      super._value as _TransactionHistoryState;

  @override
  $Res call({
    Object isLoading = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
    Object verificationType = freezed,
    Object transactions = freezed,
  }) {
    return _then(_TransactionHistoryState(
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
      verificationType: verificationType == freezed
          ? _value.verificationType
          : verificationType as String,
      transactions: transactions == freezed
          ? _value.transactions
          : transactions as List<Transaction>,
    ));
  }
}

class _$_TransactionHistoryState
    with DiagnosticableTreeMixin
    implements _TransactionHistoryState {
  const _$_TransactionHistoryState(
      {@required this.isLoading,
      @required this.isSuccess,
      @required this.isFailed,
      @required this.errorMessage,
      @required this.verificationType,
      @required this.transactions})
      : assert(isLoading != null),
        assert(isSuccess != null),
        assert(isFailed != null),
        assert(errorMessage != null),
        assert(verificationType != null),
        assert(transactions != null);

  @override
  final bool isLoading;
  @override
  final bool isSuccess;
  @override
  final bool isFailed;
  @override
  final String errorMessage;
  @override
  final String verificationType;
  @override
  final List<Transaction> transactions;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'TransactionHistoryState(isLoading: $isLoading, isSuccess: $isSuccess, isFailed: $isFailed, errorMessage: $errorMessage, verificationType: $verificationType, transactions: $transactions)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'TransactionHistoryState'))
      ..add(DiagnosticsProperty('isLoading', isLoading))
      ..add(DiagnosticsProperty('isSuccess', isSuccess))
      ..add(DiagnosticsProperty('isFailed', isFailed))
      ..add(DiagnosticsProperty('errorMessage', errorMessage))
      ..add(DiagnosticsProperty('verificationType', verificationType))
      ..add(DiagnosticsProperty('transactions', transactions));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _TransactionHistoryState &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)) &&
            (identical(other.isSuccess, isSuccess) ||
                const DeepCollectionEquality()
                    .equals(other.isSuccess, isSuccess)) &&
            (identical(other.isFailed, isFailed) ||
                const DeepCollectionEquality()
                    .equals(other.isFailed, isFailed)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)) &&
            (identical(other.verificationType, verificationType) ||
                const DeepCollectionEquality()
                    .equals(other.verificationType, verificationType)) &&
            (identical(other.transactions, transactions) ||
                const DeepCollectionEquality()
                    .equals(other.transactions, transactions)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isLoading) ^
      const DeepCollectionEquality().hash(isSuccess) ^
      const DeepCollectionEquality().hash(isFailed) ^
      const DeepCollectionEquality().hash(errorMessage) ^
      const DeepCollectionEquality().hash(verificationType) ^
      const DeepCollectionEquality().hash(transactions);

  @override
  _$TransactionHistoryStateCopyWith<_TransactionHistoryState> get copyWith =>
      __$TransactionHistoryStateCopyWithImpl<_TransactionHistoryState>(
          this, _$identity);
}

abstract class _TransactionHistoryState implements TransactionHistoryState {
  const factory _TransactionHistoryState(
      {@required bool isLoading,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage,
      @required String verificationType,
      @required List<Transaction> transactions}) = _$_TransactionHistoryState;

  @override
  bool get isLoading;
  @override
  bool get isSuccess;
  @override
  bool get isFailed;
  @override
  String get errorMessage;
  @override
  String get verificationType;
  @override
  List<Transaction> get transactions;
  @override
  _$TransactionHistoryStateCopyWith<_TransactionHistoryState> get copyWith;
}
