part of 'transaction_history_bloc.dart';

@freezed
abstract class TransactionHistoryEvent with _$TransactionHistoryEvent {
  const factory TransactionHistoryEvent.initialization() = Initialization;
  const factory TransactionHistoryEvent.requestHistory(String type) = RequestHistory;
}