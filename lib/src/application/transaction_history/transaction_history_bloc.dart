import 'package:bank_mobile_client/src/injector.dart';
import 'package:bank_mobile_client/src/models/transaction.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/transaction_repository.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/verification_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'transaction_history_event.dart';
part 'transaction_history_state.dart';
part 'transaction_history_bloc.freezed.dart';

class TransactionHistoryBloc extends Bloc<TransactionHistoryEvent, TransactionHistoryState>{
  @override
  TransactionHistoryState get initialState => TransactionHistoryState.initial();

  @override
  Stream<TransactionHistoryState> mapEventToState(TransactionHistoryEvent event) async* {
    yield* event.map(
      initialization: (e) async* {
        yield* _initialization();
      }, 
      requestHistory: (e) async* {
        yield* _getHistoryData(e.type);
      }
    );
  }

  Stream<TransactionHistoryState> _initialization() async* {
    yield state.copyWith(
      isLoading: true
    );

    try {
      final String verificationType = await injector.get<VerificationRepository>().getVerificationType();

      yield state.copyWith(
        isLoading: false,
        isSuccess: false,
        isFailed: false,
        verificationType: verificationType,
        errorMessage: '',
        transactions: []
      );
    } catch (error) {
      yield state.copyWith(
        isLoading: false, 
        isSuccess: false, 
        isFailed: true, 
        verificationType: '',
        errorMessage: error.message != null ? error.message : 'Unknown Error', 
        transactions: []
      );
    }
  }

  Stream<TransactionHistoryState> _getHistoryData(String type) async *{
    yield state.copyWith(
      isLoading: true
    );

    try {
      final List<Transaction> transactions = await injector.get<TransactionRepository>().getHistories(transactionType: type);

      yield state.copyWith(
        isLoading: false,
        isSuccess: true,
        isFailed: false,
        errorMessage: '',
        transactions: transactions
      );
    } catch (error) {
      yield state.copyWith(
        isLoading: false, 
        isSuccess: false, 
        isFailed: true, 
        verificationType: '',
        errorMessage: error.message != null ? error.message : 'Unknown Error', 
        transactions: []
      );
    }
  }
}