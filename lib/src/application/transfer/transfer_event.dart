part of 'transfer_bloc.dart';

@freezed
abstract class TransferEvent with _$TransferEvent {
  const factory TransferEvent.initialization() = Initialization;
  const factory TransferEvent.submitTransfer(Transaction transaction) = SubmitTransfer;
  const factory TransferEvent.processTransfer() = ProcessTransfer;
}