part of 'transfer_bloc.dart';

@freezed
abstract class TransferState with _$TransferState {
  const factory TransferState({
    @required bool isLoading,
    @required bool isSuccess,
    @required bool isFailed,
    @required int loadingCount,
    @required String errorMessage,
    @required String verificationType,
    @required List<Account> accounts,
    Saga saga,
  }) = _TransferState;

  factory TransferState.initial() => TransferState(
    isLoading: false, 
    isSuccess: false, 
    isFailed: false, 
    loadingCount: 0,
    errorMessage: '', 
    verificationType: '', 
    accounts: [],
    saga: null
  );
}