// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'transfer_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$TransferEventTearOff {
  const _$TransferEventTearOff();

  Initialization initialization() {
    return const Initialization();
  }

  SubmitTransfer submitTransfer(Transaction transaction) {
    return SubmitTransfer(
      transaction,
    );
  }

  ProcessTransfer processTransfer() {
    return const ProcessTransfer();
  }
}

// ignore: unused_element
const $TransferEvent = _$TransferEventTearOff();

mixin _$TransferEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initialization(),
    @required Result submitTransfer(Transaction transaction),
    @required Result processTransfer(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initialization(),
    Result submitTransfer(Transaction transaction),
    Result processTransfer(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initialization(Initialization value),
    @required Result submitTransfer(SubmitTransfer value),
    @required Result processTransfer(ProcessTransfer value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initialization(Initialization value),
    Result submitTransfer(SubmitTransfer value),
    Result processTransfer(ProcessTransfer value),
    @required Result orElse(),
  });
}

abstract class $TransferEventCopyWith<$Res> {
  factory $TransferEventCopyWith(
          TransferEvent value, $Res Function(TransferEvent) then) =
      _$TransferEventCopyWithImpl<$Res>;
}

class _$TransferEventCopyWithImpl<$Res>
    implements $TransferEventCopyWith<$Res> {
  _$TransferEventCopyWithImpl(this._value, this._then);

  final TransferEvent _value;
  // ignore: unused_field
  final $Res Function(TransferEvent) _then;
}

abstract class $InitializationCopyWith<$Res> {
  factory $InitializationCopyWith(
          Initialization value, $Res Function(Initialization) then) =
      _$InitializationCopyWithImpl<$Res>;
}

class _$InitializationCopyWithImpl<$Res>
    extends _$TransferEventCopyWithImpl<$Res>
    implements $InitializationCopyWith<$Res> {
  _$InitializationCopyWithImpl(
      Initialization _value, $Res Function(Initialization) _then)
      : super(_value, (v) => _then(v as Initialization));

  @override
  Initialization get _value => super._value as Initialization;
}

class _$Initialization implements Initialization {
  const _$Initialization();

  @override
  String toString() {
    return 'TransferEvent.initialization()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Initialization);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initialization(),
    @required Result submitTransfer(Transaction transaction),
    @required Result processTransfer(),
  }) {
    assert(initialization != null);
    assert(submitTransfer != null);
    assert(processTransfer != null);
    return initialization();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initialization(),
    Result submitTransfer(Transaction transaction),
    Result processTransfer(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initialization != null) {
      return initialization();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initialization(Initialization value),
    @required Result submitTransfer(SubmitTransfer value),
    @required Result processTransfer(ProcessTransfer value),
  }) {
    assert(initialization != null);
    assert(submitTransfer != null);
    assert(processTransfer != null);
    return initialization(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initialization(Initialization value),
    Result submitTransfer(SubmitTransfer value),
    Result processTransfer(ProcessTransfer value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initialization != null) {
      return initialization(this);
    }
    return orElse();
  }
}

abstract class Initialization implements TransferEvent {
  const factory Initialization() = _$Initialization;
}

abstract class $SubmitTransferCopyWith<$Res> {
  factory $SubmitTransferCopyWith(
          SubmitTransfer value, $Res Function(SubmitTransfer) then) =
      _$SubmitTransferCopyWithImpl<$Res>;
  $Res call({Transaction transaction});
}

class _$SubmitTransferCopyWithImpl<$Res>
    extends _$TransferEventCopyWithImpl<$Res>
    implements $SubmitTransferCopyWith<$Res> {
  _$SubmitTransferCopyWithImpl(
      SubmitTransfer _value, $Res Function(SubmitTransfer) _then)
      : super(_value, (v) => _then(v as SubmitTransfer));

  @override
  SubmitTransfer get _value => super._value as SubmitTransfer;

  @override
  $Res call({
    Object transaction = freezed,
  }) {
    return _then(SubmitTransfer(
      transaction == freezed ? _value.transaction : transaction as Transaction,
    ));
  }
}

class _$SubmitTransfer implements SubmitTransfer {
  const _$SubmitTransfer(this.transaction) : assert(transaction != null);

  @override
  final Transaction transaction;

  @override
  String toString() {
    return 'TransferEvent.submitTransfer(transaction: $transaction)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SubmitTransfer &&
            (identical(other.transaction, transaction) ||
                const DeepCollectionEquality()
                    .equals(other.transaction, transaction)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(transaction);

  @override
  $SubmitTransferCopyWith<SubmitTransfer> get copyWith =>
      _$SubmitTransferCopyWithImpl<SubmitTransfer>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initialization(),
    @required Result submitTransfer(Transaction transaction),
    @required Result processTransfer(),
  }) {
    assert(initialization != null);
    assert(submitTransfer != null);
    assert(processTransfer != null);
    return submitTransfer(transaction);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initialization(),
    Result submitTransfer(Transaction transaction),
    Result processTransfer(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (submitTransfer != null) {
      return submitTransfer(transaction);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initialization(Initialization value),
    @required Result submitTransfer(SubmitTransfer value),
    @required Result processTransfer(ProcessTransfer value),
  }) {
    assert(initialization != null);
    assert(submitTransfer != null);
    assert(processTransfer != null);
    return submitTransfer(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initialization(Initialization value),
    Result submitTransfer(SubmitTransfer value),
    Result processTransfer(ProcessTransfer value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (submitTransfer != null) {
      return submitTransfer(this);
    }
    return orElse();
  }
}

abstract class SubmitTransfer implements TransferEvent {
  const factory SubmitTransfer(Transaction transaction) = _$SubmitTransfer;

  Transaction get transaction;
  $SubmitTransferCopyWith<SubmitTransfer> get copyWith;
}

abstract class $ProcessTransferCopyWith<$Res> {
  factory $ProcessTransferCopyWith(
          ProcessTransfer value, $Res Function(ProcessTransfer) then) =
      _$ProcessTransferCopyWithImpl<$Res>;
}

class _$ProcessTransferCopyWithImpl<$Res>
    extends _$TransferEventCopyWithImpl<$Res>
    implements $ProcessTransferCopyWith<$Res> {
  _$ProcessTransferCopyWithImpl(
      ProcessTransfer _value, $Res Function(ProcessTransfer) _then)
      : super(_value, (v) => _then(v as ProcessTransfer));

  @override
  ProcessTransfer get _value => super._value as ProcessTransfer;
}

class _$ProcessTransfer implements ProcessTransfer {
  const _$ProcessTransfer();

  @override
  String toString() {
    return 'TransferEvent.processTransfer()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ProcessTransfer);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initialization(),
    @required Result submitTransfer(Transaction transaction),
    @required Result processTransfer(),
  }) {
    assert(initialization != null);
    assert(submitTransfer != null);
    assert(processTransfer != null);
    return processTransfer();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initialization(),
    Result submitTransfer(Transaction transaction),
    Result processTransfer(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (processTransfer != null) {
      return processTransfer();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initialization(Initialization value),
    @required Result submitTransfer(SubmitTransfer value),
    @required Result processTransfer(ProcessTransfer value),
  }) {
    assert(initialization != null);
    assert(submitTransfer != null);
    assert(processTransfer != null);
    return processTransfer(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initialization(Initialization value),
    Result submitTransfer(SubmitTransfer value),
    Result processTransfer(ProcessTransfer value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (processTransfer != null) {
      return processTransfer(this);
    }
    return orElse();
  }
}

abstract class ProcessTransfer implements TransferEvent {
  const factory ProcessTransfer() = _$ProcessTransfer;
}

class _$TransferStateTearOff {
  const _$TransferStateTearOff();

  _TransferState call(
      {@required bool isLoading,
      @required bool isSuccess,
      @required bool isFailed,
      @required int loadingCount,
      @required String errorMessage,
      @required String verificationType,
      @required List<Account> accounts,
      Saga saga}) {
    return _TransferState(
      isLoading: isLoading,
      isSuccess: isSuccess,
      isFailed: isFailed,
      loadingCount: loadingCount,
      errorMessage: errorMessage,
      verificationType: verificationType,
      accounts: accounts,
      saga: saga,
    );
  }
}

// ignore: unused_element
const $TransferState = _$TransferStateTearOff();

mixin _$TransferState {
  bool get isLoading;
  bool get isSuccess;
  bool get isFailed;
  int get loadingCount;
  String get errorMessage;
  String get verificationType;
  List<Account> get accounts;
  Saga get saga;

  $TransferStateCopyWith<TransferState> get copyWith;
}

abstract class $TransferStateCopyWith<$Res> {
  factory $TransferStateCopyWith(
          TransferState value, $Res Function(TransferState) then) =
      _$TransferStateCopyWithImpl<$Res>;
  $Res call(
      {bool isLoading,
      bool isSuccess,
      bool isFailed,
      int loadingCount,
      String errorMessage,
      String verificationType,
      List<Account> accounts,
      Saga saga});
}

class _$TransferStateCopyWithImpl<$Res>
    implements $TransferStateCopyWith<$Res> {
  _$TransferStateCopyWithImpl(this._value, this._then);

  final TransferState _value;
  // ignore: unused_field
  final $Res Function(TransferState) _then;

  @override
  $Res call({
    Object isLoading = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object loadingCount = freezed,
    Object errorMessage = freezed,
    Object verificationType = freezed,
    Object accounts = freezed,
    Object saga = freezed,
  }) {
    return _then(_value.copyWith(
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      loadingCount:
          loadingCount == freezed ? _value.loadingCount : loadingCount as int,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
      verificationType: verificationType == freezed
          ? _value.verificationType
          : verificationType as String,
      accounts:
          accounts == freezed ? _value.accounts : accounts as List<Account>,
      saga: saga == freezed ? _value.saga : saga as Saga,
    ));
  }
}

abstract class _$TransferStateCopyWith<$Res>
    implements $TransferStateCopyWith<$Res> {
  factory _$TransferStateCopyWith(
          _TransferState value, $Res Function(_TransferState) then) =
      __$TransferStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isLoading,
      bool isSuccess,
      bool isFailed,
      int loadingCount,
      String errorMessage,
      String verificationType,
      List<Account> accounts,
      Saga saga});
}

class __$TransferStateCopyWithImpl<$Res>
    extends _$TransferStateCopyWithImpl<$Res>
    implements _$TransferStateCopyWith<$Res> {
  __$TransferStateCopyWithImpl(
      _TransferState _value, $Res Function(_TransferState) _then)
      : super(_value, (v) => _then(v as _TransferState));

  @override
  _TransferState get _value => super._value as _TransferState;

  @override
  $Res call({
    Object isLoading = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object loadingCount = freezed,
    Object errorMessage = freezed,
    Object verificationType = freezed,
    Object accounts = freezed,
    Object saga = freezed,
  }) {
    return _then(_TransferState(
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      loadingCount:
          loadingCount == freezed ? _value.loadingCount : loadingCount as int,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
      verificationType: verificationType == freezed
          ? _value.verificationType
          : verificationType as String,
      accounts:
          accounts == freezed ? _value.accounts : accounts as List<Account>,
      saga: saga == freezed ? _value.saga : saga as Saga,
    ));
  }
}

class _$_TransferState implements _TransferState {
  const _$_TransferState(
      {@required this.isLoading,
      @required this.isSuccess,
      @required this.isFailed,
      @required this.loadingCount,
      @required this.errorMessage,
      @required this.verificationType,
      @required this.accounts,
      this.saga})
      : assert(isLoading != null),
        assert(isSuccess != null),
        assert(isFailed != null),
        assert(loadingCount != null),
        assert(errorMessage != null),
        assert(verificationType != null),
        assert(accounts != null);

  @override
  final bool isLoading;
  @override
  final bool isSuccess;
  @override
  final bool isFailed;
  @override
  final int loadingCount;
  @override
  final String errorMessage;
  @override
  final String verificationType;
  @override
  final List<Account> accounts;
  @override
  final Saga saga;

  @override
  String toString() {
    return 'TransferState(isLoading: $isLoading, isSuccess: $isSuccess, isFailed: $isFailed, loadingCount: $loadingCount, errorMessage: $errorMessage, verificationType: $verificationType, accounts: $accounts, saga: $saga)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _TransferState &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)) &&
            (identical(other.isSuccess, isSuccess) ||
                const DeepCollectionEquality()
                    .equals(other.isSuccess, isSuccess)) &&
            (identical(other.isFailed, isFailed) ||
                const DeepCollectionEquality()
                    .equals(other.isFailed, isFailed)) &&
            (identical(other.loadingCount, loadingCount) ||
                const DeepCollectionEquality()
                    .equals(other.loadingCount, loadingCount)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)) &&
            (identical(other.verificationType, verificationType) ||
                const DeepCollectionEquality()
                    .equals(other.verificationType, verificationType)) &&
            (identical(other.accounts, accounts) ||
                const DeepCollectionEquality()
                    .equals(other.accounts, accounts)) &&
            (identical(other.saga, saga) ||
                const DeepCollectionEquality().equals(other.saga, saga)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isLoading) ^
      const DeepCollectionEquality().hash(isSuccess) ^
      const DeepCollectionEquality().hash(isFailed) ^
      const DeepCollectionEquality().hash(loadingCount) ^
      const DeepCollectionEquality().hash(errorMessage) ^
      const DeepCollectionEquality().hash(verificationType) ^
      const DeepCollectionEquality().hash(accounts) ^
      const DeepCollectionEquality().hash(saga);

  @override
  _$TransferStateCopyWith<_TransferState> get copyWith =>
      __$TransferStateCopyWithImpl<_TransferState>(this, _$identity);
}

abstract class _TransferState implements TransferState {
  const factory _TransferState(
      {@required bool isLoading,
      @required bool isSuccess,
      @required bool isFailed,
      @required int loadingCount,
      @required String errorMessage,
      @required String verificationType,
      @required List<Account> accounts,
      Saga saga}) = _$_TransferState;

  @override
  bool get isLoading;
  @override
  bool get isSuccess;
  @override
  bool get isFailed;
  @override
  int get loadingCount;
  @override
  String get errorMessage;
  @override
  String get verificationType;
  @override
  List<Account> get accounts;
  @override
  Saga get saga;
  @override
  _$TransferStateCopyWith<_TransferState> get copyWith;
}
