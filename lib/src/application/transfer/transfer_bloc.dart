import 'package:bank_mobile_client/src/injector.dart';
import 'package:bank_mobile_client/src/models/account.dart';
import 'package:bank_mobile_client/src/models/saga.dart';
import 'package:bank_mobile_client/src/models/transaction.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/account_repository.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/transaction_repository.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/verification_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'transfer_event.dart';
part 'transfer_state.dart';
part 'transfer_bloc.freezed.dart';

class TransferBloc extends Bloc<TransferEvent, TransferState>{
  @override
  TransferState get initialState => TransferState.initial();

  @override
  Stream<TransferState> mapEventToState(TransferEvent event) async* {
    yield* event.map(
      initialization: (e) async* {
        yield* _initialization();
      },
      submitTransfer: (e) async* {
        yield* _submitTransfer(e.transaction);
      }, 
      processTransfer: (e) async* {
        yield* _processTransfer();
      }
    );
  }

  Stream<TransferState> _initialization() async* {
    yield state.copyWith(
      isLoading: true,
      loadingCount: state.loadingCount + 1
    );

    try {
      final String verificationType = await injector.get<VerificationRepository>().getVerificationType();
      final List<Account> accounts = await injector.get<AccountRepository>().getAccounts();

      yield state.copyWith(
        isLoading: false,
        loadingCount: 0,
        verificationType: verificationType,
        accounts: accounts
      );
    } catch (error) {
      yield state.copyWith(
        isLoading: false, 
        isSuccess: false, 
        isFailed: true, 
        errorMessage: error.message, 
        verificationType: '', 
        accounts: []
      );
    }
  }

  Stream<TransferState> _submitTransfer(Transaction transaction) async* {
    yield state.copyWith(
      isLoading: true, 
      loadingCount: state.loadingCount + 1
    );

    try {
      final Saga saga = await injector.get<TransactionRepository>().requestTransfer(transaction);
      yield state.copyWith(
        saga: saga
      );
    } catch (error) {
      yield state.copyWith(
        isLoading: false, 
        isSuccess: false, 
        isFailed: true, 
        errorMessage: error.message, 
        verificationType: '', 
        accounts: []
      );
    }
  }

  Stream<TransferState> _processTransfer() async* {
    try {
      await Future.delayed(Duration(milliseconds: 50));
      final Saga saga = await injector.get<TransactionRepository>().checkTransferSaga(state.saga.id);
      if (saga.state == 'sagaFinished') {
        yield state.copyWith(
          isLoading: false, 
          isSuccess: true, 
          isFailed: false, 
          errorMessage: '', 
          verificationType: '', 
          accounts: []
        );
        return;
      }

      if (saga.state.contains('Failed') || saga.state.contains('Rollbacked')) {
        yield state.copyWith(
          isLoading: false, 
          isSuccess: false, 
          isFailed: true, 
          errorMessage: saga.description != null ? saga.description : 'Unknown Error.', 
          verificationType: '', 
          accounts: []
        );
        return;
      }

      yield state.copyWith(
        isLoading: true,
        loadingCount: state.loadingCount + 1
      );
    } catch (error) {
      yield state.copyWith(
        isLoading: false, 
        isSuccess: false, 
        isFailed: true, 
        errorMessage: error.message != null ? error.message : 'Unknown Error', 
        verificationType: '', 
        accounts: []
      );
    }
  }
}