part of 'otp_verification_bloc.dart';

@freezed
abstract class OtpVerificationState with _$OtpVerificationState {
  const factory OtpVerificationState({
    @required bool isFormShown,
    @required bool isSubmitted,
    @required bool isSuccess,
    @required bool isFailed,
    @required String errorMessage,
  }) = _OtpVerificationState;

  factory OtpVerificationState.initial() => OtpVerificationState(
    isFormShown: false, 
    isSubmitted: false, 
    isSuccess: false, 
    isFailed: false,
    errorMessage: ''
  );
}