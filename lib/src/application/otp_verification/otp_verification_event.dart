part of 'otp_verification_bloc.dart';

@freezed
abstract class OtpVerificationEvent with _$OtpVerificationEvent {
  const factory OtpVerificationEvent.requestingToken() = RequestingToken;
  const factory OtpVerificationEvent.submittingChallenge(String otpToken) = SubmittingChallenge;
}