import 'package:bank_mobile_client/src/injector.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/verification_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'otp_verification_event.dart';
part 'otp_verification_state.dart';
part 'otp_verification_bloc.freezed.dart';

class OtpVerificationBloc extends Bloc<OtpVerificationEvent, OtpVerificationState>{
  @override
  OtpVerificationState get initialState => OtpVerificationState.initial();

  @override
  Stream<OtpVerificationState> mapEventToState(OtpVerificationEvent event) async* {
    yield* event.map(
      requestingToken: (e) async* {
        yield* _requestingToken();
      }, 
      submittingChallenge: (e) async* {
        yield* _submittingChallenge(e.otpToken);
      }
    );
  }

  Stream<OtpVerificationState> _requestingToken() async* {
    yield state.copyWith(
      isFormShown: false, 
      isSubmitted: true, 
      isSuccess: false, 
      isFailed: false, 
      errorMessage: ''
    );

    try {
      await injector.get<VerificationRepository>().generateOtpToken();
      yield state.copyWith(
        isFormShown: true, 
        isSubmitted: false, 
        isSuccess: false, 
        isFailed: false, 
        errorMessage: ''
      );
    } catch(error) {
      yield state.copyWith(
        isFormShown: false, 
        isSubmitted: false, 
        isSuccess: false, 
        isFailed: true, 
        errorMessage: error.message
      );
    }
  }

  Stream<OtpVerificationState> _submittingChallenge(String otpToken) async* {
    yield state.copyWith(
      isFormShown: false, 
      isSubmitted: true, 
      isSuccess: false, 
      isFailed: false, 
      errorMessage: ''
    );

    try {
      await injector.get<VerificationRepository>().validateOtp(otpToken);
      yield state.copyWith(
        isFormShown: false, 
        isSubmitted: false, 
        isSuccess: true, 
        isFailed: false, 
        errorMessage: ''
      );
    } catch (error) {
      yield state.copyWith(
        isFormShown: false, 
        isSubmitted: false, 
        isSuccess: false, 
        isFailed: true,
        errorMessage: error.message
      );
    }
  }
}