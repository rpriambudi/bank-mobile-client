// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'otp_verification_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$OtpVerificationEventTearOff {
  const _$OtpVerificationEventTearOff();

  RequestingToken requestingToken() {
    return const RequestingToken();
  }

  SubmittingChallenge submittingChallenge(String otpToken) {
    return SubmittingChallenge(
      otpToken,
    );
  }
}

// ignore: unused_element
const $OtpVerificationEvent = _$OtpVerificationEventTearOff();

mixin _$OtpVerificationEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result requestingToken(),
    @required Result submittingChallenge(String otpToken),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result requestingToken(),
    Result submittingChallenge(String otpToken),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result requestingToken(RequestingToken value),
    @required Result submittingChallenge(SubmittingChallenge value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result requestingToken(RequestingToken value),
    Result submittingChallenge(SubmittingChallenge value),
    @required Result orElse(),
  });
}

abstract class $OtpVerificationEventCopyWith<$Res> {
  factory $OtpVerificationEventCopyWith(OtpVerificationEvent value,
          $Res Function(OtpVerificationEvent) then) =
      _$OtpVerificationEventCopyWithImpl<$Res>;
}

class _$OtpVerificationEventCopyWithImpl<$Res>
    implements $OtpVerificationEventCopyWith<$Res> {
  _$OtpVerificationEventCopyWithImpl(this._value, this._then);

  final OtpVerificationEvent _value;
  // ignore: unused_field
  final $Res Function(OtpVerificationEvent) _then;
}

abstract class $RequestingTokenCopyWith<$Res> {
  factory $RequestingTokenCopyWith(
          RequestingToken value, $Res Function(RequestingToken) then) =
      _$RequestingTokenCopyWithImpl<$Res>;
}

class _$RequestingTokenCopyWithImpl<$Res>
    extends _$OtpVerificationEventCopyWithImpl<$Res>
    implements $RequestingTokenCopyWith<$Res> {
  _$RequestingTokenCopyWithImpl(
      RequestingToken _value, $Res Function(RequestingToken) _then)
      : super(_value, (v) => _then(v as RequestingToken));

  @override
  RequestingToken get _value => super._value as RequestingToken;
}

class _$RequestingToken
    with DiagnosticableTreeMixin
    implements RequestingToken {
  const _$RequestingToken();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'OtpVerificationEvent.requestingToken()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(
          DiagnosticsProperty('type', 'OtpVerificationEvent.requestingToken'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is RequestingToken);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result requestingToken(),
    @required Result submittingChallenge(String otpToken),
  }) {
    assert(requestingToken != null);
    assert(submittingChallenge != null);
    return requestingToken();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result requestingToken(),
    Result submittingChallenge(String otpToken),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (requestingToken != null) {
      return requestingToken();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result requestingToken(RequestingToken value),
    @required Result submittingChallenge(SubmittingChallenge value),
  }) {
    assert(requestingToken != null);
    assert(submittingChallenge != null);
    return requestingToken(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result requestingToken(RequestingToken value),
    Result submittingChallenge(SubmittingChallenge value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (requestingToken != null) {
      return requestingToken(this);
    }
    return orElse();
  }
}

abstract class RequestingToken implements OtpVerificationEvent {
  const factory RequestingToken() = _$RequestingToken;
}

abstract class $SubmittingChallengeCopyWith<$Res> {
  factory $SubmittingChallengeCopyWith(
          SubmittingChallenge value, $Res Function(SubmittingChallenge) then) =
      _$SubmittingChallengeCopyWithImpl<$Res>;
  $Res call({String otpToken});
}

class _$SubmittingChallengeCopyWithImpl<$Res>
    extends _$OtpVerificationEventCopyWithImpl<$Res>
    implements $SubmittingChallengeCopyWith<$Res> {
  _$SubmittingChallengeCopyWithImpl(
      SubmittingChallenge _value, $Res Function(SubmittingChallenge) _then)
      : super(_value, (v) => _then(v as SubmittingChallenge));

  @override
  SubmittingChallenge get _value => super._value as SubmittingChallenge;

  @override
  $Res call({
    Object otpToken = freezed,
  }) {
    return _then(SubmittingChallenge(
      otpToken == freezed ? _value.otpToken : otpToken as String,
    ));
  }
}

class _$SubmittingChallenge
    with DiagnosticableTreeMixin
    implements SubmittingChallenge {
  const _$SubmittingChallenge(this.otpToken) : assert(otpToken != null);

  @override
  final String otpToken;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'OtpVerificationEvent.submittingChallenge(otpToken: $otpToken)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty(
          'type', 'OtpVerificationEvent.submittingChallenge'))
      ..add(DiagnosticsProperty('otpToken', otpToken));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SubmittingChallenge &&
            (identical(other.otpToken, otpToken) ||
                const DeepCollectionEquality()
                    .equals(other.otpToken, otpToken)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(otpToken);

  @override
  $SubmittingChallengeCopyWith<SubmittingChallenge> get copyWith =>
      _$SubmittingChallengeCopyWithImpl<SubmittingChallenge>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result requestingToken(),
    @required Result submittingChallenge(String otpToken),
  }) {
    assert(requestingToken != null);
    assert(submittingChallenge != null);
    return submittingChallenge(otpToken);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result requestingToken(),
    Result submittingChallenge(String otpToken),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (submittingChallenge != null) {
      return submittingChallenge(otpToken);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result requestingToken(RequestingToken value),
    @required Result submittingChallenge(SubmittingChallenge value),
  }) {
    assert(requestingToken != null);
    assert(submittingChallenge != null);
    return submittingChallenge(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result requestingToken(RequestingToken value),
    Result submittingChallenge(SubmittingChallenge value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (submittingChallenge != null) {
      return submittingChallenge(this);
    }
    return orElse();
  }
}

abstract class SubmittingChallenge implements OtpVerificationEvent {
  const factory SubmittingChallenge(String otpToken) = _$SubmittingChallenge;

  String get otpToken;
  $SubmittingChallengeCopyWith<SubmittingChallenge> get copyWith;
}

class _$OtpVerificationStateTearOff {
  const _$OtpVerificationStateTearOff();

  _OtpVerificationState call(
      {@required bool isFormShown,
      @required bool isSubmitted,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage}) {
    return _OtpVerificationState(
      isFormShown: isFormShown,
      isSubmitted: isSubmitted,
      isSuccess: isSuccess,
      isFailed: isFailed,
      errorMessage: errorMessage,
    );
  }
}

// ignore: unused_element
const $OtpVerificationState = _$OtpVerificationStateTearOff();

mixin _$OtpVerificationState {
  bool get isFormShown;
  bool get isSubmitted;
  bool get isSuccess;
  bool get isFailed;
  String get errorMessage;

  $OtpVerificationStateCopyWith<OtpVerificationState> get copyWith;
}

abstract class $OtpVerificationStateCopyWith<$Res> {
  factory $OtpVerificationStateCopyWith(OtpVerificationState value,
          $Res Function(OtpVerificationState) then) =
      _$OtpVerificationStateCopyWithImpl<$Res>;
  $Res call(
      {bool isFormShown,
      bool isSubmitted,
      bool isSuccess,
      bool isFailed,
      String errorMessage});
}

class _$OtpVerificationStateCopyWithImpl<$Res>
    implements $OtpVerificationStateCopyWith<$Res> {
  _$OtpVerificationStateCopyWithImpl(this._value, this._then);

  final OtpVerificationState _value;
  // ignore: unused_field
  final $Res Function(OtpVerificationState) _then;

  @override
  $Res call({
    Object isFormShown = freezed,
    Object isSubmitted = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      isFormShown:
          isFormShown == freezed ? _value.isFormShown : isFormShown as bool,
      isSubmitted:
          isSubmitted == freezed ? _value.isSubmitted : isSubmitted as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
    ));
  }
}

abstract class _$OtpVerificationStateCopyWith<$Res>
    implements $OtpVerificationStateCopyWith<$Res> {
  factory _$OtpVerificationStateCopyWith(_OtpVerificationState value,
          $Res Function(_OtpVerificationState) then) =
      __$OtpVerificationStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isFormShown,
      bool isSubmitted,
      bool isSuccess,
      bool isFailed,
      String errorMessage});
}

class __$OtpVerificationStateCopyWithImpl<$Res>
    extends _$OtpVerificationStateCopyWithImpl<$Res>
    implements _$OtpVerificationStateCopyWith<$Res> {
  __$OtpVerificationStateCopyWithImpl(
      _OtpVerificationState _value, $Res Function(_OtpVerificationState) _then)
      : super(_value, (v) => _then(v as _OtpVerificationState));

  @override
  _OtpVerificationState get _value => super._value as _OtpVerificationState;

  @override
  $Res call({
    Object isFormShown = freezed,
    Object isSubmitted = freezed,
    Object isSuccess = freezed,
    Object isFailed = freezed,
    Object errorMessage = freezed,
  }) {
    return _then(_OtpVerificationState(
      isFormShown:
          isFormShown == freezed ? _value.isFormShown : isFormShown as bool,
      isSubmitted:
          isSubmitted == freezed ? _value.isSubmitted : isSubmitted as bool,
      isSuccess: isSuccess == freezed ? _value.isSuccess : isSuccess as bool,
      isFailed: isFailed == freezed ? _value.isFailed : isFailed as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage as String,
    ));
  }
}

class _$_OtpVerificationState
    with DiagnosticableTreeMixin
    implements _OtpVerificationState {
  const _$_OtpVerificationState(
      {@required this.isFormShown,
      @required this.isSubmitted,
      @required this.isSuccess,
      @required this.isFailed,
      @required this.errorMessage})
      : assert(isFormShown != null),
        assert(isSubmitted != null),
        assert(isSuccess != null),
        assert(isFailed != null),
        assert(errorMessage != null);

  @override
  final bool isFormShown;
  @override
  final bool isSubmitted;
  @override
  final bool isSuccess;
  @override
  final bool isFailed;
  @override
  final String errorMessage;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'OtpVerificationState(isFormShown: $isFormShown, isSubmitted: $isSubmitted, isSuccess: $isSuccess, isFailed: $isFailed, errorMessage: $errorMessage)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'OtpVerificationState'))
      ..add(DiagnosticsProperty('isFormShown', isFormShown))
      ..add(DiagnosticsProperty('isSubmitted', isSubmitted))
      ..add(DiagnosticsProperty('isSuccess', isSuccess))
      ..add(DiagnosticsProperty('isFailed', isFailed))
      ..add(DiagnosticsProperty('errorMessage', errorMessage));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _OtpVerificationState &&
            (identical(other.isFormShown, isFormShown) ||
                const DeepCollectionEquality()
                    .equals(other.isFormShown, isFormShown)) &&
            (identical(other.isSubmitted, isSubmitted) ||
                const DeepCollectionEquality()
                    .equals(other.isSubmitted, isSubmitted)) &&
            (identical(other.isSuccess, isSuccess) ||
                const DeepCollectionEquality()
                    .equals(other.isSuccess, isSuccess)) &&
            (identical(other.isFailed, isFailed) ||
                const DeepCollectionEquality()
                    .equals(other.isFailed, isFailed)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isFormShown) ^
      const DeepCollectionEquality().hash(isSubmitted) ^
      const DeepCollectionEquality().hash(isSuccess) ^
      const DeepCollectionEquality().hash(isFailed) ^
      const DeepCollectionEquality().hash(errorMessage);

  @override
  _$OtpVerificationStateCopyWith<_OtpVerificationState> get copyWith =>
      __$OtpVerificationStateCopyWithImpl<_OtpVerificationState>(
          this, _$identity);
}

abstract class _OtpVerificationState implements OtpVerificationState {
  const factory _OtpVerificationState(
      {@required bool isFormShown,
      @required bool isSubmitted,
      @required bool isSuccess,
      @required bool isFailed,
      @required String errorMessage}) = _$_OtpVerificationState;

  @override
  bool get isFormShown;
  @override
  bool get isSubmitted;
  @override
  bool get isSuccess;
  @override
  bool get isFailed;
  @override
  String get errorMessage;
  @override
  _$OtpVerificationStateCopyWith<_OtpVerificationState> get copyWith;
}
