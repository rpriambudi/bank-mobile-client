import 'package:bank_mobile_client/src/screens/home_screen.dart';
import 'package:bank_mobile_client/src/screens/login/login_page.dart';
import 'package:bank_mobile_client/src/screens/registrations/registration_page.dart';
import 'package:bank_mobile_client/src/screens/startup/start_page.dart';
import 'package:flutter/material.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/' : return MaterialPageRoute(builder: (_) => StartPage());
      case '/login': return MaterialPageRoute(builder: (_) => LoginPage());
      case '/home': return MaterialPageRoute(builder: (_) => HomePage());
      case '/register': return MaterialPageRoute(builder: (_) => RegistrationPage());
      default: return MaterialPageRoute(
        builder: (_) => Scaffold(
          body: Center(
            child: Text('No suitable page found.'),
          ),
        )
      );
    }
  }
}