import 'package:bank_mobile_client/src/application/pin_verification/pin_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/set_verification/set_verification_bloc.dart';
import 'package:bank_mobile_client/src/screens/set_verification/set_pin_verification_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SetVerificationPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<PinVerificationBloc>(
          create: (BuildContext context) => PinVerificationBloc(),
        ),
        BlocProvider<SetVerificationBloc>(
          create: (BuildContext context) => SetVerificationBloc(),
        ),
      ],
      child: SetPinVerificationForm()
    );
  }

}