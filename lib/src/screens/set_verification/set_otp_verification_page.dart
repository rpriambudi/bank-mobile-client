import 'package:bank_mobile_client/src/application/otp_verification/otp_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/set_verification/set_verification_bloc.dart';
import 'package:bank_mobile_client/src/screens/set_verification/set_otp_verification_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SetOtpVerificationPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<OtpVerificationBloc>(
          create: (BuildContext context) => OtpVerificationBloc(),
        ),
        BlocProvider<SetVerificationBloc>(
          create: (BuildContext context) => SetVerificationBloc(),
        ),
      ],
      child: SetOtpVerificationForm()
    );
  }

}