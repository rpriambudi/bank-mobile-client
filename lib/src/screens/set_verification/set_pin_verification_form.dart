import 'package:bank_mobile_client/src/application/pin_verification/pin_verification_bloc.dart';
import 'package:bank_mobile_client/src/screens/set_verification/set_verification_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bank_mobile_client/src/application/set_verification/set_verification_bloc.dart';

class SetPinVerificationForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SetPinVerificationFormState();
  
}

class SetPinVerificationFormState extends State<SetPinVerificationForm> {
  final TextEditingController _pinController = TextEditingController();
  PinVerificationState _pinVerificationState = PinVerificationState.initial();

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<PinVerificationBloc, PinVerificationState>(
          listener: (context, state) {
            setState(() {
              _pinVerificationState = state;
            });
          },
        ),
        BlocListener<SetVerificationBloc, SetVerificationState> (
          listener: (context, state) {
            if (state.isSubmitted) {
              setState(() {
                _pinVerificationState = PinVerificationState.initial();
              });
            }
          },
        ),
      ], 
      child: BlocBuilder<SetVerificationBloc, SetVerificationState>(
        builder: (context, state) {
          final Widget _pinVerificationWidget = _handlerPinVerificationState(context);
          if (_pinVerificationWidget != null) {
            return _pinVerificationWidget;
          }

          if (state.isSubmitted) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          if (state.isSuccess) {
            return AlertDialog(
              title: Text('Set Verification'),
              content: Text('Successfully set PIN as default verification'),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => SetVerificationPage()));
                  },
                )
              ],
            );
          }

          if (state.isFailed) {
            return AlertDialog(
              title: Text('Set Verification'),
              content: Text('Failed to set verification. ' + state.errorMessage),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => SetVerificationPage()));
                  },
                )
              ],
            );
          }
          
          return RaisedButton(
            child: Text('PIN'),
            onPressed: () {
              BlocProvider.of<PinVerificationBloc>(context).add(PinVerificationEvent.showForm());
            },
          );
        },
      )
    );
  }

  Widget _handlerPinVerificationState(BuildContext context) {
    if (_pinVerificationState.isFormShown) {
      return AlertDialog(
        title: Text('PIN'),
        content: TextField(
          controller: _pinController,
          maxLength: 4,
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              BlocProvider.of<PinVerificationBloc>(context).add(PinVerificationEvent.submitForm(_pinController.text));
            },
          )
        ],
      );
    }

    if (_pinVerificationState.isSubmitted) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if (_pinVerificationState.isFailed) {
      return AlertDialog(
        title: Text('PIN Validation failed'),
        content: Text(_pinVerificationState.errorMessage),
        actions: <Widget>[
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => SetVerificationPage()));
            },
          )
        ],
      );
    }

    if (_pinVerificationState.isSuccess) {
      BlocProvider.of<SetVerificationBloc>(context).add(SetVerificationEvent.setAsPin());
      return null;
    }
  }
}