import 'package:bank_mobile_client/src/application/otp_verification/otp_verification_bloc.dart';
import 'package:bank_mobile_client/src/screens/set_verification/set_otp_verification_page.dart';
import 'package:bank_mobile_client/src/screens/set_verification/set_verification_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bank_mobile_client/src/application/set_verification/set_verification_bloc.dart';

class SetOtpVerificationForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SetOtpVerificationFormState();
  
}

class SetOtpVerificationFormState extends State<SetOtpVerificationForm> {
  final TextEditingController _otpController = TextEditingController();
  OtpVerificationState _otpVerificationState = OtpVerificationState.initial();

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<OtpVerificationBloc, OtpVerificationState>(
          listener: (context, state) {
            setState(() {
              _otpVerificationState = state;
            });
          },
        ),
        BlocListener<SetVerificationBloc, SetVerificationState> (
          listener: (context, state) {
            if (state.isSubmitted) {
              setState(() {
                _otpVerificationState = OtpVerificationState.initial();
              });
            }
          },
        ),
      ], 
      child: BlocBuilder<SetVerificationBloc, SetVerificationState>(
        builder: (context, state) {
          final Widget _otpVerificationWidget = _handleOtpVerificationState(context);
          if (_otpVerificationWidget != null) {
            return _otpVerificationWidget;
          }

          if (state.isSubmitted) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          if (state.isSuccess) {
            return AlertDialog(
              title: Text('Set Verification'),
              content: Text('Successfully set OTP as default verification'),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => SetOtpVerificationPage()));
                  },
                )
              ],
            );
          }

          if (state.isFailed) {
            return AlertDialog(
              title: Text('Set Verification'),
              content: Text('Failed to set verification. ' + state.errorMessage),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => SetOtpVerificationPage()));
                  },
                )
              ],
            );
          }
          
          return RaisedButton(
            child: Text('OTP'),
            onPressed: () {
              BlocProvider.of<OtpVerificationBloc>(context).add(OtpVerificationEvent.requestingToken());
            },
          );
        },
      )
    );
  }

  Widget _handleOtpVerificationState(BuildContext context) {
    if (_otpVerificationState.isFormShown) {
      return AlertDialog(
        title: Text('OTP'),
        content: Column(
          children: <Widget>[
            Text('Please insert the OTP code which is sent to you'),
            TextField(
              controller: _otpController,
              keyboardType: TextInputType.phone,
            ),
          ],
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              BlocProvider.of<OtpVerificationBloc>(context).add(OtpVerificationEvent.submittingChallenge(_otpController.text));
            },
          )
        ],
      );
    }

    if (_otpVerificationState.isSubmitted) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if (_otpVerificationState.isFailed) {
      return AlertDialog(
        title: Text('OTP Validation failed'),
        content: Text(_otpVerificationState.errorMessage),
        actions: <Widget>[
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => SetOtpVerificationPage()));
            },
          )
        ],
      );
    }

    if (_otpVerificationState.isSuccess) {
      BlocProvider.of<SetVerificationBloc>(context).add(SetVerificationEvent.setAsOtp());
      return null;
    }
  }
}