import 'package:bank_mobile_client/src/models/customer.dart';
import 'package:bank_mobile_client/src/models/user.dart';
import 'package:flutter/material.dart';

class RegistrationFormWidget {
  static Widget getFullNameFormField(Customer customer) {
    return TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return 'Not Empty';
        }
        return null;
      },
      decoration: InputDecoration(
        hintText: 'Full Name'
      ),
      onSaved: (String value) {
        customer.setName = value;
      },
    );
  }

  static Widget getAddressFormField(Customer customer) {
    return TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return 'Not Empty';
        }
        return null;
      },
      decoration: InputDecoration(
        hintText: 'Address'
      ),
      maxLines: 5,
      onSaved: (String value) {
        customer.setAddress = value;
      },
    );
  }

  static Widget getIdNumberFormField(Customer customer) {
    return TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return 'Not Empty';
        }
        return null;
      },
      decoration: InputDecoration(
        hintText: 'ID Number'
      ),
      onSaved: (String value) {
        customer.setIdNumber = value;
      },
    );
  }

  static Widget getMotherMaidenNameFormField(Customer customer) {
    return TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return 'Not Empty';
        }
        return null;
      },
      decoration: InputDecoration(
        hintText: 'Mother Maiden Name'
      ),
      onSaved: (String value) {
        customer.setMotherMaidenName = value;
      },
    );
  }

  static Widget getUsernameFormField(User user) {
    return TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return 'Not Empty';
        }
        return null;
      },
      decoration: InputDecoration(
        hintText: 'username'
      ),
      onSaved: (String value) {
        user.setEmail = value;
      },
    );
  }

  static Widget getPasswordFormField(User user) {
    return TextFormField(
      obscureText: true,
      validator: (value) {
        if (value.isEmpty) {
          return 'Not Empty';
        }
        return null;
      },
      decoration: InputDecoration(
        hintText: 'password',
      ),
      onSaved: (String value) {
        user.setPassword = value;
      },
    );
  }

  static List<DropdownMenuItem> get genderList {
    List<DropdownMenuItem> dropdownList = <String>['MALE', 'FEMALE']
      .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem(value: value, child: Text(value.substring(0, 1) + value.substring(1, value.length).toLowerCase()),);
      }).toList();
      return dropdownList;
  }

  static List<DropdownMenuItem> get idTypeList {
    List<DropdownMenuItem> dropdownList = <String>['KTP', 'SIM', 'KITAS']
      .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem(value: value, child: Text(value),);
      }).toList();
      return dropdownList;
  }
}