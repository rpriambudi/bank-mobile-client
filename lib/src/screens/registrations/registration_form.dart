import 'package:bank_mobile_client/src/blocs/registration_bloc/registration_bloc.dart';
import 'package:bank_mobile_client/src/models/customer.dart';
import 'package:bank_mobile_client/src/models/registration.dart';
import 'package:bank_mobile_client/src/models/user.dart';
import 'package:bank_mobile_client/src/screens/registrations/registration_form_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class RegistrationFormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RegistrationFormScreenState();
}

class RegistrationFormScreenState extends State<RegistrationFormScreen> {
  final _formKey = GlobalKey<FormState>();
  final Customer customer = Customer();
  final User user = User();

  String idTypeValue;
  String genderValue;
  DateTime selectedDate = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final datePicked = await showDatePicker(
      context: context, 
      initialDate: selectedDate, 
      firstDate: DateTime(1970, 1), 
      lastDate: DateTime.now()
    );

    if (datePicked != null && datePicked != selectedDate) {
      setState(() {
        selectedDate = datePicked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle _sectionTitleStyle = new TextStyle(fontSize: 25, fontWeight: FontWeight.bold);

    return BlocBuilder<RegistrationBloc, RegistrationState>(
      builder: (context, state) {
        if (state.isLoading) {
          if (state.saga != null) {
            BlocProvider.of<RegistrationBloc>(context).add(RegistrationEvent.processingSaga());
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        }

        if (state.isFailed) {
          WidgetsBinding.instance.addPostFrameCallback((_) => showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Registration Failed'),
                content: Text(state.errorMessage),
                actions: <Widget>[
                  FlatButton(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.of(context).pushReplacementNamed('/register');
                    },
                  )
                ],
              );
            }
          ));
        }

        if (state.isSuccess) {
          WidgetsBinding.instance.addPostFrameCallback((_) => showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Registration success'),
                content: Text('Head to Login page for logging in.'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.of(context).pushReplacementNamed('/');
                    },
                  )
                ],
              );
            }
          ));
        }
        
        return Form(
          key: _formKey,
          child: Container(
            margin: new EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: new EdgeInsets.symmetric(vertical: 5.0),
                    child: Text('User Information', style: _sectionTitleStyle,),
                  ),
                  RegistrationFormWidget.getUsernameFormField(user),
                  RegistrationFormWidget.getPasswordFormField(user),
                  Padding(
                    padding: new EdgeInsets.symmetric(vertical: 5.0),
                    child: Text('Customer Information', style: _sectionTitleStyle,),
                  ),
                  RegistrationFormWidget.getFullNameFormField(customer),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: DropdownButton<String>(
                      value: genderValue,
                      icon: Icon(Icons.arrow_downward),
                      iconSize: 24,
                      elevation: 16,
                      onChanged: (String newValue) {
                        setState(() {
                          genderValue = newValue;
                        });
                      },
                      hint: Text('Gender'),
                      items: RegistrationFormWidget.genderList,
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: <Widget>[
                        Text('Date of Birth: '),
                        Text('${selectedDate.toLocal()}'.split(' ')[0]),
                        SizedBox(height: 20.0,),
                        RaisedButton(
                          onPressed: () => _selectDate(context),
                          child: Text('Select Date'),
                        )
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: DropdownButton<String>(
                      value: idTypeValue,
                      icon: Icon(Icons.arrow_downward),
                      iconSize: 24,
                      elevation: 16,
                      onChanged: (String newValue) {
                        setState(() {
                          idTypeValue = newValue;
                        });
                      },
                      hint: Text('ID Type'),
                      items: RegistrationFormWidget.idTypeList,
                    ),
                  ),
                  RegistrationFormWidget.getIdNumberFormField(customer),
                  RegistrationFormWidget.getMotherMaidenNameFormField(customer),
                  RegistrationFormWidget.getAddressFormField(customer),
                  Padding(
                    padding: new EdgeInsets.symmetric(vertical: 10.0),
                    child: Builder(
                      builder: (buttonContext) => RaisedButton(
                        child: Text('Submit'),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();

                            customer.setBirthDate = new DateFormat('yyyy-MM-ddThh:mm:ss').format(selectedDate);
                            customer.setIdType = idTypeValue;
                            customer.setGender = genderValue;
                            
                            final Registration registration = Registration(customer: customer, user: user);
                            BlocProvider.of<RegistrationBloc>(context).add(RegistrationEvent.submittingForm(registration));
                          }
                        },
                      ),
                    )
                  ),
                ],
              ),
            )
          )
        );
      },
    );
  }
}