import 'package:bank_mobile_client/src/blocs/registration_bloc/registration_bloc.dart';
import 'package:bank_mobile_client/src/screens/registrations/registration_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegistrationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('User Registration'),
      ),
      body: BlocProvider<RegistrationBloc>(
        create: (BuildContext context) => RegistrationBloc(),
        child: RegistrationFormScreen(),
      ),
    );
  }
  
}