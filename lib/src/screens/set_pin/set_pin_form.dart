import 'package:bank_mobile_client/src/application/set_pin/set_pin_bloc.dart';
import 'package:bank_mobile_client/src/screens/set_pin/set_pin_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SetPinFormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SetPinFormScreenState();
}

class SetPinFormScreenState extends State<SetPinFormScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _pinController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _pinController.addListener(() {
      BlocProvider.of<SetPinBloc>(context).add(PinChanged(_pinController.text));
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SetPinBloc, SetPinState>(
      builder: (context, state) {
        if (state.isSubmitted) {
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

        if (state.isSuccess) {
          return AlertDialog(
            content: Text('PIN Berhasil Disimpan'),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.pushReplacementNamed(context, '/');
                },
              )
            ],
          );
        }

        if (state.isFailed && state.errorMessage != '') {
          return AlertDialog(
            content: Text(state.errorMessage),
            actions: <Widget>[
              FlatButton(
                child: Text('Kembali'),
                onPressed: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => SetPinPage()));
                },
              )
            ],
          );
        }

        return Form(
          key: _formKey,
          child: Container(
            margin: new EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Center(
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: _pinController,
                      decoration: InputDecoration(
                        icon: Icon(Icons.lock),
                        labelText: 'PIN',
                      ),
                      keyboardType: TextInputType.phone,
                      autovalidate: true,
                      autocorrect: false,
                      validator: (_) {
                        return !state.isPinValid ? 'Invalid PIN Format. Must be numeric and max 4 digit': null;
                      },
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: RaisedButton(
                        child: Text('Submit'),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            final String pin = _pinController.text;
                            BlocProvider.of<SetPinBloc>(context).add(SubmittingForm(pin));
                          }
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}