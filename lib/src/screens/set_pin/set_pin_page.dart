import 'package:bank_mobile_client/src/application/set_pin/set_pin_bloc.dart';
import 'package:bank_mobile_client/src/screens/set_pin/set_pin_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SetPinPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Set Pin'),
      ),
      body: BlocProvider(
        create: (context) => SetPinBloc(),
        child: SetPinFormScreen(),
      ),
    );
  }
}