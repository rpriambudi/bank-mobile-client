import 'package:bank_mobile_client/src/application/login_form/login_bloc.dart';
import 'package:bank_mobile_client/src/screens/login/login_form_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: BlocProvider(
        create: (context) => LoginBloc(),
        child: LoginFormScreen(),
      ),
    );
  }
}
