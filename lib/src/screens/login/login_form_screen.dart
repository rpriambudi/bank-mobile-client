import 'package:bank_mobile_client/src/application/auth_bloc.dart';
import 'package:bank_mobile_client/src/application/login_form/login_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:flutter/material.dart';


class LoginFormScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginBloc, LoginState>(
      listener: (context, state) {
        state.maybeMap(
          authSuccessful: (s) {
            context.bloc<AuthBloc>().add(CheckUserSignedIn());
            Navigator.pushReplacementNamed(context, '/home');
          },
          orElse: () {}
        );
      },
      builder: (context, state) {
        return state.maybeWhen(
          formNotInitialized: () {
            return _webviewNotInitialized(context);
          }, 
          formInitialized: (s) {
            return _authorizationUrlRetrieved(context, s);
          },
          orElse: () {
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        );
      }
    );
  }

  Widget _webviewNotInitialized(BuildContext context) {
    context.bloc<LoginBloc>().add(LoginEvent.initializeForm());
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _authorizationUrlRetrieved(BuildContext context, String url) {
    context.bloc<LoginBloc>().add(RequestAuthentication());
    return new WebviewScaffold(
      url: url,
    );
  }
}
// class LoginFormScreen extends StatefulWidget {
//   final String _authorizationUrl;
  
//   LoginFormScreen(String authorizationUrl): _authorizationUrl = authorizationUrl;

//   @override
//   State<StatefulWidget> createState() => LoginFormScreenState(_authorizationUrl);
// }

// class LoginFormScreenState extends State<LoginFormScreen> {
//   final String _authorizationUrl;
//   String _webviewUrl = '';

//   LoginFormScreenState(this._authorizationUrl);

//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     // final webview = FlutterWebviewPlugin();
//     // webview.onUrlChanged.listen((String url) {
//     //   setState(() {
//     //     _webviewUrl = url;
//     //   });
//     // });

//     // if (_client != null) {
//     //   webview.close();
//     //   BlocProvider.of<LoginBloc>(context).add(LoginAuthenticationSuccess());
//     // }

//     // if (_webviewUrl.contains(_constants.authCallbackUrl.toString())) {
//     //   final codeUri = Uri.parse(_webviewUrl);
//     //   if (codeUri.queryParameters['code'] != null) {
//     //     setState(() {
//     //       _getClientFromCode(codeUri.queryParameters['code']);
//     //       _webviewUrl = '';
//     //     });
//     //   }
//     // }
    
//     // if (_authorizationUrl == null) {
//     //   _authorizationUrl = _grant.getAuthorizationUrl(_constants.authCallbackUrl).toString();
//     // }

//     return new WebviewScaffold(
//       url: _authorizationUrl,
//       appBar: AppBar(
//         title: Text('Login'),
//       ),
//     );
//   }

//   Future<void> _getClientFromCode(String code) async {
//     // if (_client != null) {
//     //   return;
//     // }

//     // try {
//     //   await _authProvider.setClientFromCode(code);
//     //   setState(() {
//     //     _client = _authProvider.getClient();
//     //   });
//     // } catch (error) {
//     //   print(error);
//     // }
//   }
// }