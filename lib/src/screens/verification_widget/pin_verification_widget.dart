import 'package:bank_mobile_client/src/application/pin_verification/pin_verification_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PinVerificationWidget {
  static Future<bool> showPinWidget(BuildContext mainContext, PinVerificationBloc verificationBloc) {
    verificationBloc.add(PinVerificationEvent.showForm());

    return showDialog(
      context: mainContext,
      barrierDismissible: false,
      builder: (BuildContext context) {
        final TextEditingController _pinController = TextEditingController();

        return BlocBuilder(
          bloc: verificationBloc,
          builder: (context, state) {
            if (state.isFormShown) {
              return AlertDialog(
                title: Text('PIN'),
                content: TextField(
                  controller: _pinController,
                  maxLength: 4,
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Cancel'),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                  ),
                  FlatButton(
                    child: Text('OK'),
                    onPressed: () {
                      verificationBloc.add(PinVerificationEvent.submitForm(_pinController.text));
                    },
                  )
                ],
              );
            }

            if (state.isFailed) {
              return AlertDialog(
                title: Text('PIN Validation failed'),
                content: Text(state.errorMessage),
                actions: <Widget>[
                  FlatButton(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                  )
                ],
              );
            }

            if (state.isSuccess) {
              Navigator.of(context).pop(true);
            }

            return Center(
              child: CircularProgressIndicator(),
            );
          },
        );
      }
    );
  }
}