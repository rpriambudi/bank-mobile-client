import 'package:bank_mobile_client/src/application/otp_verification/otp_verification_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OtpVerificationWidget {
  static Future<bool> showOtpWidget(BuildContext mainContext, OtpVerificationBloc verificationBloc) {
    verificationBloc.add(OtpVerificationEvent.requestingToken());

    return showDialog<bool>(
      context: mainContext,
      barrierDismissible: false,
      builder: (BuildContext context) {
        final TextEditingController _otpController = TextEditingController();

        return BlocBuilder(
          bloc: verificationBloc,
          builder: (context, state) {
            if (state.isFormShown) {
              return AlertDialog(
                title: Text('OTP'),
                content: Column(
                  children: <Widget>[
                    Text('Please insert the OTP code which is sent to you'),
                    TextField(
                      controller: _otpController,
                      keyboardType: TextInputType.phone,
                    ),
                  ],
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Cancel'),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                  ),
                  FlatButton(
                    child: Text('OK'),
                    onPressed: () {
                      verificationBloc.add(OtpVerificationEvent.submittingChallenge(_otpController.text));
                    },
                  )
                ],
              );
            }

            if (state.isFailed) {
              return AlertDialog(
                title: Text('OTP Validation failed'),
                content: Text(state.errorMessage),
                actions: <Widget>[
                  FlatButton(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                  )
                ],
              );
            }

            if (state.isSuccess) {
              Navigator.of(context).pop(true);
            }

            return AlertDialog(
              content: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator()
                  )
                ],
              ),
            );
          },
        );
      }
    );
  }
}