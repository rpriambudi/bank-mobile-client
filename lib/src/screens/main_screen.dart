
import 'package:bank_mobile_client/src/application/auth_bloc.dart';
import 'package:bank_mobile_client/src/router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => AuthBloc()..add(CheckUserSignedIn()),
        )
      ], 
      child: MaterialApp(
        onGenerateRoute: Router.generateRoute,
        initialRoute: '/',
      )
    );
  }
}
