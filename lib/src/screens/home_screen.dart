import 'package:bank_mobile_client/src/application/auth_bloc.dart';
import 'package:bank_mobile_client/src/models/token.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      builder: (context, state) {
        return state.maybeMap(
          authenticated: (s) {
            final Token userData = s.userData;
            return WillPopScope(
              child: Scaffold(
                appBar: AppBar(
                  title: Text('Home'),
                ),
                body: Center(
                  child: Column(
                    children: <Widget>[
                      Text('Welcome, ${userData.username}'),
                      RaisedButton(
                        onPressed: () {
                          context.bloc<AuthBloc>().add(SignedOut());
                          Navigator.pushReplacementNamed(context, '/');
                        },
                      )
                    ],
                  ),
                ),
              ),
              onWillPop: () {
                context.bloc<AuthBloc>().add(CloseClient());
                return Navigator.pushReplacementNamed(context, '/');
              }
            );
          },
          orElse: () {
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        );
      }, 
      listener: (context, state) {
        state.maybeMap(
          unauthenticated: (s) {
            Navigator.pushReplacementNamed(context, '/');
          },
          orElse: () {}
        );
      }
    );
  }
  
}