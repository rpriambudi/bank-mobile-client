import 'package:bank_mobile_client/src/application/otp_verification/otp_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/pin_verification/pin_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/transfer/transfer_bloc.dart';
import 'package:bank_mobile_client/src/screens/transactions/transfer_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransferPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<PinVerificationBloc>(
          create: (BuildContext context) => PinVerificationBloc(),
        ),
        BlocProvider<OtpVerificationBloc>(
          create: (BuildContext context) => OtpVerificationBloc(),
        ),
        BlocProvider<TransferBloc>(
          create: (BuildContext context) => TransferBloc()..add(TransferEvent.initialization()),
        )
      ],
      child: TransferForm(),
    );
  }

}