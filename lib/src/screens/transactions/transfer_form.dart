import 'package:bank_mobile_client/src/application/otp_verification/otp_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/pin_verification/pin_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/transfer/transfer_bloc.dart';
import 'package:bank_mobile_client/src/models/account.dart';
import 'package:bank_mobile_client/src/models/transaction.dart';
import 'package:bank_mobile_client/src/screens/transactions/transfer_page.dart';
import 'package:bank_mobile_client/src/screens/verification_widget/otp_verification_widget.dart';
import 'package:bank_mobile_client/src/screens/verification_widget/pin_verification_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransferForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TransferFormState();
}

class TransferFormState extends State<TransferForm> {
  String _selectedAccount;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _destinationAcctController = TextEditingController();
  final TextEditingController _amountController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransferBloc, TransferState>(
      builder: (context, state) {
        if (state.isLoading) {
          if (state.saga != null) {
            BlocProvider.of<TransferBloc>(context).add(TransferEvent.processTransfer());
          }

          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

        if (state.isFailed) {
          return AlertDialog(
            title: Text('Transfer failed'),
            content: Text(state.errorMessage),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => TransferPage()));
                },
              )
            ],
          );
        }

        if (state.isSuccess) {
          return AlertDialog(
            title: Text('Transfer success'),
            content: Text('Your fund has been successfully transferred'),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => TransferPage()));
                },
              )
            ],
          );
        }

        return Scaffold(
          appBar: AppBar(
            title: Text('Transfer Page'),
          ),
          body: Form(
            key: _formKey,
            child: Align(
              alignment: Alignment.topLeft,
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: <Widget>[
                        Text('Your Account'),
                        Padding(
                          padding: EdgeInsets.only(left: 65.0),
                        ),
                        Flexible(
                          child: DropdownButtonFormField(
                            value: _selectedAccount,
                            items: state.accounts.map<DropdownMenuItem<String>>((Account account) {
                              return DropdownMenuItem<String>(
                                value: account.accountNo,
                                child: Text(account.accountNo),
                              );
                            }).toList(),
                            onChanged: (String accountNo) {
                              setState(() {
                                _selectedAccount = accountNo;
                              });
                            },
                            validator: (value) => value == null ? 'Cannot be empty' : null,
                          ),
                          fit: FlexFit.tight,
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: <Widget>[
                        Text('Destination Account'),
                        Padding(
                          padding: EdgeInsets.only(left: 30.0),
                        ),
                        Flexible(
                          child: TextFormField(
                            controller: _destinationAcctController,
                            keyboardType: TextInputType.number,
                            validator: (value) => value.isEmpty ? 'Cannot be empty' : null,
                          ),
                          fit: FlexFit.tight,
                        )
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: <Widget>[
                        Text('Amount'),
                        Padding(
                          padding: EdgeInsets.only(left: 95.0),
                        ),
                        Flexible(
                          child: TextFormField(
                            controller: _amountController,
                            keyboardType: TextInputType.number,
                            validator: (value) => value.isEmpty ? 'Cannot be empty' : null,
                          ),
                          fit: FlexFit.tight,
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: <Widget>[
                        Text('Description'),
                        Padding(
                          padding: EdgeInsets.only(left: 75.0),
                        ),
                        Flexible(
                          child: TextFormField(
                            controller: _descriptionController,
                            keyboardType: TextInputType.text,
                            maxLines: 4,
                          ),
                          fit: FlexFit.tight,
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: RaisedButton(
                      child: Text('Submit'),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          if (state.verificationType == 'PIN') {
                            final pinBloc = context.bloc<PinVerificationBloc>();
                            PinVerificationWidget.showPinWidget(context, pinBloc)
                            .then((bool isValidated) {
                              if (isValidated) {
                                Transaction transaction = Transaction(
                                  amount: int.parse(_amountController.text),
                                  requesterAcctNo: _selectedAccount,
                                  destinationAcctNo: _destinationAcctController.text,
                                  description: _descriptionController.text,
                                );
                                BlocProvider.of<TransferBloc>(context).add(TransferEvent.submitTransfer(transaction));
                              }
                            });
                          }

                          if (state.verificationType == 'OTP') {
                            final otpBloc = context.bloc<OtpVerificationBloc>();
                            OtpVerificationWidget.showOtpWidget(context, otpBloc)
                            .then((bool validate) {
                              if (validate) {
                                Transaction transaction = Transaction(
                                  amount: int.parse(_amountController.text),
                                  requesterAcctNo: _selectedAccount,
                                  destinationAcctNo: _destinationAcctController.text,
                                  description: _descriptionController.text,
                                );
                                BlocProvider.of<TransferBloc>(context).add(TransferEvent.submitTransfer(transaction));
                              }
                            });
                          }
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}