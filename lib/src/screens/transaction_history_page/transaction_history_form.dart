import 'package:bank_mobile_client/src/application/otp_verification/otp_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/pin_verification/pin_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/transaction_history/transaction_history_bloc.dart';
import 'package:bank_mobile_client/src/screens/transaction_history_page/transaction_history_page.dart';
import 'package:bank_mobile_client/src/screens/verification_widget/otp_verification_widget.dart';
import 'package:bank_mobile_client/src/screens/verification_widget/pin_verification_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransactionHistoryForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TransactionHistoryFormState();
}

class TransactionHistoryFormState extends State<TransactionHistoryForm> {
  final List<String> dummylist = ['asdsad', 'asdasdasd'];
  String _selectedType;
  
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransactionHistoryBloc, TransactionHistoryState>(
      builder: (context, state) {
        if (state.isLoading) {
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

        if (state.isFailed) {
          return AlertDialog(
            title: Text('Get History'),
            content: Text(state.errorMessage),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => TransactionHistoryPage()));
                },
              )
            ],
          );
        }

        return Scaffold(
          appBar: AppBar(
            title: Text('Transaction Histories Page'),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                  child: Row(
                    children: <Widget>[
                      Text('Transaction Type'),
                      Padding(
                        padding: EdgeInsets.only(left: 50.0),
                      ),
                      DropdownButton(
                        value: _selectedType,
                        items: [
                          DropdownMenuItem(value: 'DB', child: Text('Debit'),),
                          DropdownMenuItem(value: 'CR', child: Text('Credit'),),
                          DropdownMenuItem(value: 'TR', child: Text('Transfer'),)
                        ],
                        onChanged: (String value) {
                          setState(() {
                            _selectedType = value;
                          });
                        },
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: RaisedButton(
                    child: Text('Submit'),
                    onPressed: () {
                      if (state.verificationType == 'PIN') {
                        final pinBloc = context.bloc<PinVerificationBloc>();
                        PinVerificationWidget.showPinWidget(context, pinBloc)
                        .then((bool isValidated) {
                          if (isValidated) {
                            BlocProvider.of<TransactionHistoryBloc>(context).add(TransactionHistoryEvent.requestHistory(_selectedType == null ? '': _selectedType));
                          }
                        });
                      }

                      if (state.verificationType == 'OTP') {
                        final otpBloc = context.bloc<OtpVerificationBloc>();
                        OtpVerificationWidget.showOtpWidget(context, otpBloc)
                        .then((bool isValidated) {
                          if (isValidated) {
                            BlocProvider.of<TransactionHistoryBloc>(context).add(TransactionHistoryEvent.requestHistory(_selectedType == null ? '' : _selectedType));
                          }
                        });
                      }
                    },
                  ),
                ),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: state.transactions.length,
                  itemBuilder: (context, index) {
                    if (state.transactions.length == 0) {
                      return ListTile(subtitle: Text('No Data found.'));
                    }

                    final transaction = state.transactions[index];
                    if (transaction.transactionType == 'TRF') {
                      return ListTile(
                        leading: Text(transaction.transactionType),
                        title: Text('Receiver: ${transaction.destinationAcctNo} ; Amount: ${transaction.amount}'),
                        subtitle: Text('Status [${transaction.transactionStatus}]'),
                      );
                    } else {
                      return ListTile(
                        leading: Text(transaction.transactionType),
                        title: Text('Amount: ${transaction.amount}'),
                        subtitle: Text('Status [${transaction.transactionStatus}]'),
                      );
                    }
                  }
                ),
              ],
            ),
          )
        );
      },
    );
  }
}