import 'package:bank_mobile_client/src/application/otp_verification/otp_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/pin_verification/pin_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/transaction_history/transaction_history_bloc.dart';
import 'package:bank_mobile_client/src/screens/transaction_history_page/transaction_history_form.dart';
import 'package:bank_mobile_client/src/screens/transactions/transfer_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransactionHistoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<PinVerificationBloc>(
          create: (BuildContext context) => PinVerificationBloc(),
        ),
        BlocProvider<OtpVerificationBloc>(
          create: (BuildContext context) => OtpVerificationBloc(),
        ),
        BlocProvider<TransactionHistoryBloc>(
          create: (BuildContext context) => TransactionHistoryBloc()..add(TransactionHistoryEvent.initialization()),
        )
      ],
      child: TransactionHistoryForm(),
    );
  }

}