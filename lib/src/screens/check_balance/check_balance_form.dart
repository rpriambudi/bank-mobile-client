import 'package:bank_mobile_client/src/application/check_balance/check_balance_bloc.dart';
import 'package:bank_mobile_client/src/application/otp_verification/otp_verification_bloc.dart';
import 'package:bank_mobile_client/src/application/pin_verification/pin_verification_bloc.dart';
import 'package:bank_mobile_client/src/models/account.dart';
import 'package:bank_mobile_client/src/screens/check_balance/check_balance_page.dart';
import 'package:bank_mobile_client/src/screens/verification_widget/otp_verification_widget.dart';
import 'package:bank_mobile_client/src/screens/verification_widget/pin_verification_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CheckBalanceForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => CheckBalanceFormState();
}

class CheckBalanceFormState extends State<CheckBalanceForm> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CheckBalanceBloc, CheckBalanceState>(
      builder: (context, state) {
        if (state.isLoading) {
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

        if (state.isFailed) {
          return AlertDialog(
            title: Text('Check Balance'),
            content: Text(state.errorMessage),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => CheckBalancePage()));
                },
              )
            ],
          );
        }

        if (state.verificationType == 'PIN') {
          final pinBloc = context.bloc<PinVerificationBloc>();
          PinVerificationWidget.showPinWidget(context, pinBloc)
          .then((bool isValidated) {
            if (isValidated) {
              BlocProvider.of<CheckBalanceBloc>(context).add(CheckBalanceEvent.requestBalance());
            }
          });
        }

        if (state.verificationType == 'OTP') {
          final otpBloc = context.bloc<OtpVerificationBloc>();
          WidgetsBinding.instance.addPostFrameCallback((_) => OtpVerificationWidget.showOtpWidget(context, otpBloc)
          .then((bool isValidated) {
            if (isValidated) {
              BlocProvider.of<CheckBalanceBloc>(context).add(CheckBalanceEvent.requestBalance());
            }
          }));
        }

        return Scaffold(
          appBar: AppBar(
            title: Text('Balance Page'),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Text('Account Information'),
                ),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: state.accounts.length,
                  itemBuilder: (context, index) {
                    final Account account = state.accounts[index];
                    return ListTile(
                      title: Text(account.accountNo),
                      subtitle: Text('Balance: ${account.balance}'),
                    );
                  }
                ),
              ],
            ),
          )
        );
      },
    );
  }
}