class Constants {
  final _authEndpointUrl = Uri.parse('http://192.168.43.115/auth/realms/nest-bank-api/protocol/openid-connect/auth');
  final _authTokenUrl = Uri.parse('http://192.168.43.115/auth/realms/nest-bank-api/protocol/openid-connect/token');
  final _authCallbackUrl = Uri.parse('http://192.168.43.115/auth/code');
  final _authLogoutCallbackUrl = Uri.parse('http://192.168.43.115/auth/logout');
  final _authLogoutUrl = Uri.parse('http://192.168.43.115/auth/realms/nest-bank-api/protocol/openid-connect/logout');
  final _authClientId = 'nest-bank-api';
  final _authClientSecret = '163ca589-c68d-43c9-b5c6-f694f074af5d';
  final _authCredentialFile = '~/.bank_mobile_client/credentials.json';

  Uri get authEndpointUrl => _authEndpointUrl;
  Uri get authTokenUrl => _authTokenUrl;
  Uri get authCallbackUrl => _authCallbackUrl;
  Uri get authLogoutUri => _authLogoutUrl;
  Uri get authLogoutCallbackUrl => _authLogoutCallbackUrl;
  String get authClientId => _authClientId;
  String get authClientSecret => _authClientSecret;
  String get authCredentialFile => _authCredentialFile;
}