import 'package:bank_mobile_client/src/models/account.dart';

abstract class AccountRepository {
  Future<List<Account>> getAccounts();
}