import 'package:bank_mobile_client/src/models/forms/pin.dart';

abstract class VerificationRepository {
  Future<dynamic> submitPin(PinForm pinForm);
  Future<void> validatePin(String pin);
  Future<void> setVerification(String verificationType);
  Future<void> generateOtpToken();
  Future<void> validateOtp(String otpToken);
  Future<String> getVerificationType();
}