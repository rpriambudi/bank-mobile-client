import 'dart:convert';

import 'package:bank_mobile_client/src/models/registration.dart';
import 'package:http/http.dart' as http;
import 'package:bank_mobile_client/src/models/saga.dart';

abstract class RegistrationRepository {
  Future<Saga> submitRegistration(Registration registrationData);

  Future<Saga> getRegistrationState(int registrationId);
}