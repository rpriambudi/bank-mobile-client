import 'package:bank_mobile_client/src/models/saga.dart';
import 'package:bank_mobile_client/src/models/transaction.dart';

abstract class TransactionRepository {
  Future<Saga> requestTransfer(Transaction transaction);
  Future<List<Transaction>> getHistories({String transactionType});
  Future<Saga> checkTransferSaga(int sagaId);
}