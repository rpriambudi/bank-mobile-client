import 'dart:convert';

import 'package:bank_mobile_client/src/models/registration.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/registration_repository.dart';
import 'package:http/http.dart' as http;
import 'package:bank_mobile_client/src/models/saga.dart';

class RegistrationRepositoryImpl extends RegistrationRepository {
  Future<Saga> submitRegistration(Registration registrationData) async {
    final String serviceUrl = 'http://192.168.1.116:3001/api/registration';
    final body = registrationData.toJson();
    final response = await http.post(serviceUrl, body: jsonEncode(body), headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    });
    final responseBody = jsonDecode(response.body);

    if (response.statusCode == 201) {
      return Saga.fromJson(responseBody);
    }
  
    throw Exception('Failed to start registration: ' + responseBody['message']);
  }

  Future<Saga> getRegistrationState(int registrationId) async {
    final response = await http.get('http://192.168.1.116:3001/api/registration/state/' + registrationId.toString());
    final responseBody = jsonDecode(response.body);

    if (response.statusCode == 200) {
      return Saga.fromJson(responseBody);
    }

    throw Exception('Failed to get state: ' + responseBody['message']);
  }
}