import 'dart:convert';

import 'package:bank_mobile_client/src/models/account.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/account_repository.dart';
import 'package:http/http.dart' as http;

class AccountRepositoryImpl implements AccountRepository {
  @override
  Future<List<Account>> getAccounts() async {
    final String serviceUrl = 'http://192.168.1.116:3004/api/accounts/customer/balance';
    final String token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJYnhnbXJVVkM1aVU5RGN3YkNsd1RTay1RRVUzdVZ3SXA5Rm8xaWszUGJZIn0.eyJqdGkiOiJlODJhOTliYS1mMTBiLTQxZmMtOTgyMi0wZjI2ZWU4NzgwYTYiLCJleHAiOjE1ODc2NDM4OTQsIm5iZiI6MCwiaWF0IjoxNTg3NjA3OTAwLCJpc3MiOiJodHRwOi8vMTkyLjE2OC45OS4xMDA6ODA4MC9hdXRoL3JlYWxtcy9uZXN0LWJhbmstYXBpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjJmMWZmMWE0LTgyMGUtNDRhNS05NTI4LWY3ZGY3NDM2Mzg3MyIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5lc3QtYmFuay1hcGkiLCJhdXRoX3RpbWUiOjE1ODc2MDc4OTYsInNlc3Npb25fc3RhdGUiOiI5NWI2MDhiYy1hZjA1LTQyODEtOThiMC03MWY2MGI0ZGNhMjEiLCJhY3IiOiIwIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJuZXN0LWJhbmstYXBpIjp7InJvbGVzIjpbIkN1c3RvbWVyIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImNpZk5vIjoiOTk5MzIxMDAwMDAwMDAwMDAwMDA4MSIsInByZWZlcnJlZF91c2VybmFtZSI6IjA4MTIwMDAwMDAzNTEiLCJ1c2VySWQiOjkxLCJlbWFpbCI6IjA4MTIwMDAwMDAzNTEifQ.n2VlkOVh_0uKOWOffwPpynetHEQBUXPes1EUERCU2BLmQrS7Mdox0TCK4M6cNm2VCDMhkDARCUeXtBK9Gc9-WI3jpc56RP6sAknF-rGwH1H82SOkIi5zR1ysC-CxOFkkhwyaGVIdFOLCCN9nS2Ah-8pheDoJ57_P5nCD8PlCsT2sQCIXW3d3N664uWQBFrtJUDZLj_jD9MLbZhAhap6MrcXEQ7dshHdpzsr_7HoRFAAWf3LT301YydFmojpsycvJjI5mJGnit04k-O5-Y3YFSxrYCuozyq3fgC05NKv__IjXSJxtm9FsLpO0zbtS1kMK3KmvaRig6Xy0czALf2O97A';
    final response = await http.get(serviceUrl, headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${token}'
    });
    
    final List<Account> accounts = [];
    if (response.statusCode == 200) {
      final List<dynamic> responseBody = jsonDecode(response.body);
      for (var i = 0; i < responseBody.length; i++) {
        accounts.add(Account.fromJson(responseBody[i]));
      }

      return accounts;
    }

    final responseBody = jsonDecode(response.body);
    throw Exception('Failed to start registration: ' + responseBody['message']);
  }
  
}