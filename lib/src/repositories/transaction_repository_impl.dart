import 'dart:convert';

import 'package:bank_mobile_client/src/models/saga.dart';
import 'package:bank_mobile_client/src/models/transaction.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/transaction_repository.dart';
import 'package:http/http.dart' as http;

class TransactionRepositoryImpl implements TransactionRepository{
  @override
  Future<Saga> requestTransfer(Transaction transaction) async {
    final String serviceUrl = 'http://192.168.1.116:3005/api/transfer/customer';
    final String token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJYnhnbXJVVkM1aVU5RGN3YkNsd1RTay1RRVUzdVZ3SXA5Rm8xaWszUGJZIn0.eyJqdGkiOiJlODJhOTliYS1mMTBiLTQxZmMtOTgyMi0wZjI2ZWU4NzgwYTYiLCJleHAiOjE1ODc2NDM4OTQsIm5iZiI6MCwiaWF0IjoxNTg3NjA3OTAwLCJpc3MiOiJodHRwOi8vMTkyLjE2OC45OS4xMDA6ODA4MC9hdXRoL3JlYWxtcy9uZXN0LWJhbmstYXBpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjJmMWZmMWE0LTgyMGUtNDRhNS05NTI4LWY3ZGY3NDM2Mzg3MyIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5lc3QtYmFuay1hcGkiLCJhdXRoX3RpbWUiOjE1ODc2MDc4OTYsInNlc3Npb25fc3RhdGUiOiI5NWI2MDhiYy1hZjA1LTQyODEtOThiMC03MWY2MGI0ZGNhMjEiLCJhY3IiOiIwIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJuZXN0LWJhbmstYXBpIjp7InJvbGVzIjpbIkN1c3RvbWVyIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImNpZk5vIjoiOTk5MzIxMDAwMDAwMDAwMDAwMDA4MSIsInByZWZlcnJlZF91c2VybmFtZSI6IjA4MTIwMDAwMDAzNTEiLCJ1c2VySWQiOjkxLCJlbWFpbCI6IjA4MTIwMDAwMDAzNTEifQ.n2VlkOVh_0uKOWOffwPpynetHEQBUXPes1EUERCU2BLmQrS7Mdox0TCK4M6cNm2VCDMhkDARCUeXtBK9Gc9-WI3jpc56RP6sAknF-rGwH1H82SOkIi5zR1ysC-CxOFkkhwyaGVIdFOLCCN9nS2Ah-8pheDoJ57_P5nCD8PlCsT2sQCIXW3d3N664uWQBFrtJUDZLj_jD9MLbZhAhap6MrcXEQ7dshHdpzsr_7HoRFAAWf3LT301YydFmojpsycvJjI5mJGnit04k-O5-Y3YFSxrYCuozyq3fgC05NKv__IjXSJxtm9FsLpO0zbtS1kMK3KmvaRig6Xy0czALf2O97A';
    final body = transaction.toJson();
    final response = await http.post(serviceUrl, body: jsonEncode({ 'transfer': body }), headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${token}'
    });

    final responseBody = jsonDecode(response.body);
    if (response.statusCode == 201) {
      return Saga.fromJson(responseBody);
    }
  
    throw Exception('Failed to start registration: ' + responseBody['message']);
  }

  @override
  Future<Saga> checkTransferSaga(int sagaId) async {
    final String serviceUrl = 'http://192.168.1.116:3005/api/saga/state/${sagaId}';
    final response = await http.get(serviceUrl, headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    });

    final responseBody = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return Saga.fromJson(responseBody);
    }
  
    throw Exception('Failed to start registration: ' + responseBody['message']);
  }

  @override
  Future<List<Transaction>> getHistories({String transactionType}) async {
    String serviceUrl = 'http://192.168.1.116:3005/api/transactions/customer';
    if (transactionType != null) {
      serviceUrl += '?transactionType=${transactionType}';
    }

    final String token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJYnhnbXJVVkM1aVU5RGN3YkNsd1RTay1RRVUzdVZ3SXA5Rm8xaWszUGJZIn0.eyJqdGkiOiJjMjZmNmVkMS1kNThjLTQ2ZjgtODYzNC1lMmI5N2UxYmNmMDgiLCJleHAiOjE1ODc2ODExNTUsIm5iZiI6MCwiaWF0IjoxNTg3NjQ1MTU2LCJpc3MiOiJodHRwOi8vMTkyLjE2OC45OS4xMDA6ODA4MC9hdXRoL3JlYWxtcy9uZXN0LWJhbmstYXBpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjJmMWZmMWE0LTgyMGUtNDRhNS05NTI4LWY3ZGY3NDM2Mzg3MyIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5lc3QtYmFuay1hcGkiLCJhdXRoX3RpbWUiOjE1ODc2NDUxNTUsInNlc3Npb25fc3RhdGUiOiJlNDY3NmM2My0wZTliLTRmZWUtYjdjNi1jZGI0Zjg1ZjIyNWIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJuZXN0LWJhbmstYXBpIjp7InJvbGVzIjpbIkN1c3RvbWVyIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImNpZk5vIjoiOTk5MzIxMDAwMDAwMDAwMDAwMDA4MSIsInByZWZlcnJlZF91c2VybmFtZSI6IjA4MTIwMDAwMDAzNTEiLCJ1c2VySWQiOjkxLCJlbWFpbCI6IjA4MTIwMDAwMDAzNTEifQ.Z46KJoUiOUMfI5PaNDkrLiIP3WJY5rYOIsQLAXLXu7PyuwQPjae5y9N8a5OXqOacagcU6vXq3BuOrh_C56TpTyoSxFv8r4SghJ64c5k5LyMXZPFi0gWc2IYygz2Uuf96cEJ1W2kgaIrPowTLvaMF-8NnYWvVq9QXueMqf5JebwoEM3dukKAuC_ghmFNpQdv8FqKPk2lt8ip9vwRTMzZMaogHNPE2NOgpxbKxPyeNWkLk9SCNJxNvGZPCOId3EU8YJ54RO_4QAfkW5Ids_8GAjF0KKf18lxanb5OwVZampO6yaItk8Abz0s0W6Tshee3YSIj7tkWdfQM33ayrEYmY1A';
    final response = await http.get(serviceUrl, headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${token}'
    });

    
    if (response.statusCode == 200) {
      final List<Transaction> transactions = [];
      final List<dynamic> responseBody = jsonDecode(response.body);
      for (var i = 0; i < responseBody.length; i++) {
        transactions.add(Transaction.fromJson(responseBody[i]));
      }
      return transactions;
    }
    final responseBody = jsonDecode(response.body);
    throw Exception('Failed to get history: ${responseBody['message']}');
  }

}