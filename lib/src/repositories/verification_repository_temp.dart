import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:bank_mobile_client/src/models/forms/pin.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/verification_repository.dart';

class VerificationRepositoryTemp extends VerificationRepository {
  @override
  Future submitPin(PinForm pinForm) async {
    final String serviceUrl = 'http://192.168.1.116:3002/api/users/verification/pin';
    final body = pinForm.toJson();
    body['username'] = '0812000000351';
    body['credentialType'] = 'PHONE';
    final token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJYnhnbXJVVkM1aVU5RGN3YkNsd1RTay1RRVUzdVZ3SXA5Rm8xaWszUGJZIn0.eyJqdGkiOiJjMjZmNmVkMS1kNThjLTQ2ZjgtODYzNC1lMmI5N2UxYmNmMDgiLCJleHAiOjE1ODc2ODExNTUsIm5iZiI6MCwiaWF0IjoxNTg3NjQ1MTU2LCJpc3MiOiJodHRwOi8vMTkyLjE2OC45OS4xMDA6ODA4MC9hdXRoL3JlYWxtcy9uZXN0LWJhbmstYXBpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjJmMWZmMWE0LTgyMGUtNDRhNS05NTI4LWY3ZGY3NDM2Mzg3MyIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5lc3QtYmFuay1hcGkiLCJhdXRoX3RpbWUiOjE1ODc2NDUxNTUsInNlc3Npb25fc3RhdGUiOiJlNDY3NmM2My0wZTliLTRmZWUtYjdjNi1jZGI0Zjg1ZjIyNWIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJuZXN0LWJhbmstYXBpIjp7InJvbGVzIjpbIkN1c3RvbWVyIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImNpZk5vIjoiOTk5MzIxMDAwMDAwMDAwMDAwMDA4MSIsInByZWZlcnJlZF91c2VybmFtZSI6IjA4MTIwMDAwMDAzNTEiLCJ1c2VySWQiOjkxLCJlbWFpbCI6IjA4MTIwMDAwMDAzNTEifQ.Z46KJoUiOUMfI5PaNDkrLiIP3WJY5rYOIsQLAXLXu7PyuwQPjae5y9N8a5OXqOacagcU6vXq3BuOrh_C56TpTyoSxFv8r4SghJ64c5k5LyMXZPFi0gWc2IYygz2Uuf96cEJ1W2kgaIrPowTLvaMF-8NnYWvVq9QXueMqf5JebwoEM3dukKAuC_ghmFNpQdv8FqKPk2lt8ip9vwRTMzZMaogHNPE2NOgpxbKxPyeNWkLk9SCNJxNvGZPCOId3EU8YJ54RO_4QAfkW5Ids_8GAjF0KKf18lxanb5OwVZampO6yaItk8Abz0s0W6Tshee3YSIj7tkWdfQM33ayrEYmY1A';
    final response = await http.post(serviceUrl, body: jsonEncode(body), headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${token}'
    });

    final responseBody = jsonDecode(response.body);
    if (response.statusCode != 201) {
      throw Exception('Failed to set pin: ' + responseBody['message']);
    }
  }

  @override
  Future<void> validatePin(String pin) async {
    final String serviceUrl = 'http://192.168.1.116:3002/api/users/verification/pin/verify';
    final Map<String, String> body = { 'challenge': pin };
    final token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJYnhnbXJVVkM1aVU5RGN3YkNsd1RTay1RRVUzdVZ3SXA5Rm8xaWszUGJZIn0.eyJqdGkiOiJjMjZmNmVkMS1kNThjLTQ2ZjgtODYzNC1lMmI5N2UxYmNmMDgiLCJleHAiOjE1ODc2ODExNTUsIm5iZiI6MCwiaWF0IjoxNTg3NjQ1MTU2LCJpc3MiOiJodHRwOi8vMTkyLjE2OC45OS4xMDA6ODA4MC9hdXRoL3JlYWxtcy9uZXN0LWJhbmstYXBpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjJmMWZmMWE0LTgyMGUtNDRhNS05NTI4LWY3ZGY3NDM2Mzg3MyIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5lc3QtYmFuay1hcGkiLCJhdXRoX3RpbWUiOjE1ODc2NDUxNTUsInNlc3Npb25fc3RhdGUiOiJlNDY3NmM2My0wZTliLTRmZWUtYjdjNi1jZGI0Zjg1ZjIyNWIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJuZXN0LWJhbmstYXBpIjp7InJvbGVzIjpbIkN1c3RvbWVyIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImNpZk5vIjoiOTk5MzIxMDAwMDAwMDAwMDAwMDA4MSIsInByZWZlcnJlZF91c2VybmFtZSI6IjA4MTIwMDAwMDAzNTEiLCJ1c2VySWQiOjkxLCJlbWFpbCI6IjA4MTIwMDAwMDAzNTEifQ.Z46KJoUiOUMfI5PaNDkrLiIP3WJY5rYOIsQLAXLXu7PyuwQPjae5y9N8a5OXqOacagcU6vXq3BuOrh_C56TpTyoSxFv8r4SghJ64c5k5LyMXZPFi0gWc2IYygz2Uuf96cEJ1W2kgaIrPowTLvaMF-8NnYWvVq9QXueMqf5JebwoEM3dukKAuC_ghmFNpQdv8FqKPk2lt8ip9vwRTMzZMaogHNPE2NOgpxbKxPyeNWkLk9SCNJxNvGZPCOId3EU8YJ54RO_4QAfkW5Ids_8GAjF0KKf18lxanb5OwVZampO6yaItk8Abz0s0W6Tshee3YSIj7tkWdfQM33ayrEYmY1A';
    final response = await http.post(serviceUrl, body: jsonEncode(body), headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${token}'
    });

    if (response.statusCode != 201) {
      final responseBody = jsonDecode(response.body);
      throw Exception('Failed to validate: ' + responseBody['message']);
    }
    return;
  }

  @override
  Future<void> setVerification(String verificationType) async {
    final String serviceUrl = 'http://192.168.1.116:3002/api/users/set/verification';
    final Map<String, String> body = { 'verificationType': verificationType };
    final token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJYnhnbXJVVkM1aVU5RGN3YkNsd1RTay1RRVUzdVZ3SXA5Rm8xaWszUGJZIn0.eyJqdGkiOiJjMjZmNmVkMS1kNThjLTQ2ZjgtODYzNC1lMmI5N2UxYmNmMDgiLCJleHAiOjE1ODc2ODExNTUsIm5iZiI6MCwiaWF0IjoxNTg3NjQ1MTU2LCJpc3MiOiJodHRwOi8vMTkyLjE2OC45OS4xMDA6ODA4MC9hdXRoL3JlYWxtcy9uZXN0LWJhbmstYXBpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjJmMWZmMWE0LTgyMGUtNDRhNS05NTI4LWY3ZGY3NDM2Mzg3MyIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5lc3QtYmFuay1hcGkiLCJhdXRoX3RpbWUiOjE1ODc2NDUxNTUsInNlc3Npb25fc3RhdGUiOiJlNDY3NmM2My0wZTliLTRmZWUtYjdjNi1jZGI0Zjg1ZjIyNWIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJuZXN0LWJhbmstYXBpIjp7InJvbGVzIjpbIkN1c3RvbWVyIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImNpZk5vIjoiOTk5MzIxMDAwMDAwMDAwMDAwMDA4MSIsInByZWZlcnJlZF91c2VybmFtZSI6IjA4MTIwMDAwMDAzNTEiLCJ1c2VySWQiOjkxLCJlbWFpbCI6IjA4MTIwMDAwMDAzNTEifQ.Z46KJoUiOUMfI5PaNDkrLiIP3WJY5rYOIsQLAXLXu7PyuwQPjae5y9N8a5OXqOacagcU6vXq3BuOrh_C56TpTyoSxFv8r4SghJ64c5k5LyMXZPFi0gWc2IYygz2Uuf96cEJ1W2kgaIrPowTLvaMF-8NnYWvVq9QXueMqf5JebwoEM3dukKAuC_ghmFNpQdv8FqKPk2lt8ip9vwRTMzZMaogHNPE2NOgpxbKxPyeNWkLk9SCNJxNvGZPCOId3EU8YJ54RO_4QAfkW5Ids_8GAjF0KKf18lxanb5OwVZampO6yaItk8Abz0s0W6Tshee3YSIj7tkWdfQM33ayrEYmY1A';
    final response = await http.post(serviceUrl, body: jsonEncode(body), headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${token}'
    });

    if (response.statusCode != 201) {
      final responseBody = jsonDecode(response.body);
      throw Exception('Failed to validate: ' + responseBody['message']);
    }
    return;
  }

  @override
  Future<void> generateOtpToken() async {
    final String serviceUrl = 'http://192.168.1.116:3002/api/users/verification/otp/token';
    final String token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJYnhnbXJVVkM1aVU5RGN3YkNsd1RTay1RRVUzdVZ3SXA5Rm8xaWszUGJZIn0.eyJqdGkiOiJjMjZmNmVkMS1kNThjLTQ2ZjgtODYzNC1lMmI5N2UxYmNmMDgiLCJleHAiOjE1ODc2ODExNTUsIm5iZiI6MCwiaWF0IjoxNTg3NjQ1MTU2LCJpc3MiOiJodHRwOi8vMTkyLjE2OC45OS4xMDA6ODA4MC9hdXRoL3JlYWxtcy9uZXN0LWJhbmstYXBpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjJmMWZmMWE0LTgyMGUtNDRhNS05NTI4LWY3ZGY3NDM2Mzg3MyIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5lc3QtYmFuay1hcGkiLCJhdXRoX3RpbWUiOjE1ODc2NDUxNTUsInNlc3Npb25fc3RhdGUiOiJlNDY3NmM2My0wZTliLTRmZWUtYjdjNi1jZGI0Zjg1ZjIyNWIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJuZXN0LWJhbmstYXBpIjp7InJvbGVzIjpbIkN1c3RvbWVyIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImNpZk5vIjoiOTk5MzIxMDAwMDAwMDAwMDAwMDA4MSIsInByZWZlcnJlZF91c2VybmFtZSI6IjA4MTIwMDAwMDAzNTEiLCJ1c2VySWQiOjkxLCJlbWFpbCI6IjA4MTIwMDAwMDAzNTEifQ.Z46KJoUiOUMfI5PaNDkrLiIP3WJY5rYOIsQLAXLXu7PyuwQPjae5y9N8a5OXqOacagcU6vXq3BuOrh_C56TpTyoSxFv8r4SghJ64c5k5LyMXZPFi0gWc2IYygz2Uuf96cEJ1W2kgaIrPowTLvaMF-8NnYWvVq9QXueMqf5JebwoEM3dukKAuC_ghmFNpQdv8FqKPk2lt8ip9vwRTMzZMaogHNPE2NOgpxbKxPyeNWkLk9SCNJxNvGZPCOId3EU8YJ54RO_4QAfkW5Ids_8GAjF0KKf18lxanb5OwVZampO6yaItk8Abz0s0W6Tshee3YSIj7tkWdfQM33ayrEYmY1A';
    final response = await http.get(serviceUrl, headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${token}'
    });

    final responseBody = jsonDecode(response.body);
    if (response.statusCode != 200) {
      throw Exception('Failed to get otp token: ' + responseBody['message']);
    }
  }

  @override
  Future<void> validateOtp(String otpToken) async {
    final String serviceUrl = 'http://192.168.1.116:3002/api/users/verification/otp/verify';
    final Map<String, String> body = { 'challenge': otpToken };
    final String token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJYnhnbXJVVkM1aVU5RGN3YkNsd1RTay1RRVUzdVZ3SXA5Rm8xaWszUGJZIn0.eyJqdGkiOiJjMjZmNmVkMS1kNThjLTQ2ZjgtODYzNC1lMmI5N2UxYmNmMDgiLCJleHAiOjE1ODc2ODExNTUsIm5iZiI6MCwiaWF0IjoxNTg3NjQ1MTU2LCJpc3MiOiJodHRwOi8vMTkyLjE2OC45OS4xMDA6ODA4MC9hdXRoL3JlYWxtcy9uZXN0LWJhbmstYXBpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjJmMWZmMWE0LTgyMGUtNDRhNS05NTI4LWY3ZGY3NDM2Mzg3MyIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5lc3QtYmFuay1hcGkiLCJhdXRoX3RpbWUiOjE1ODc2NDUxNTUsInNlc3Npb25fc3RhdGUiOiJlNDY3NmM2My0wZTliLTRmZWUtYjdjNi1jZGI0Zjg1ZjIyNWIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJuZXN0LWJhbmstYXBpIjp7InJvbGVzIjpbIkN1c3RvbWVyIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImNpZk5vIjoiOTk5MzIxMDAwMDAwMDAwMDAwMDA4MSIsInByZWZlcnJlZF91c2VybmFtZSI6IjA4MTIwMDAwMDAzNTEiLCJ1c2VySWQiOjkxLCJlbWFpbCI6IjA4MTIwMDAwMDAzNTEifQ.Z46KJoUiOUMfI5PaNDkrLiIP3WJY5rYOIsQLAXLXu7PyuwQPjae5y9N8a5OXqOacagcU6vXq3BuOrh_C56TpTyoSxFv8r4SghJ64c5k5LyMXZPFi0gWc2IYygz2Uuf96cEJ1W2kgaIrPowTLvaMF-8NnYWvVq9QXueMqf5JebwoEM3dukKAuC_ghmFNpQdv8FqKPk2lt8ip9vwRTMzZMaogHNPE2NOgpxbKxPyeNWkLk9SCNJxNvGZPCOId3EU8YJ54RO_4QAfkW5Ids_8GAjF0KKf18lxanb5OwVZampO6yaItk8Abz0s0W6Tshee3YSIj7tkWdfQM33ayrEYmY1A';
    final response = await http.post(serviceUrl, body: jsonEncode(body), headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${token}'
    });

    if (response.statusCode != 201) {
      final responseBody = jsonDecode(response.body);
      throw Exception('Failed to validate: ' + responseBody['message']);
    }
    return;
  }

  @override
  Future<String> getVerificationType() async {
    final String serviceUrl = 'http://192.168.1.116:3002/api/users/detail';
    final String token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJYnhnbXJVVkM1aVU5RGN3YkNsd1RTay1RRVUzdVZ3SXA5Rm8xaWszUGJZIn0.eyJqdGkiOiJjMjZmNmVkMS1kNThjLTQ2ZjgtODYzNC1lMmI5N2UxYmNmMDgiLCJleHAiOjE1ODc2ODExNTUsIm5iZiI6MCwiaWF0IjoxNTg3NjQ1MTU2LCJpc3MiOiJodHRwOi8vMTkyLjE2OC45OS4xMDA6ODA4MC9hdXRoL3JlYWxtcy9uZXN0LWJhbmstYXBpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjJmMWZmMWE0LTgyMGUtNDRhNS05NTI4LWY3ZGY3NDM2Mzg3MyIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5lc3QtYmFuay1hcGkiLCJhdXRoX3RpbWUiOjE1ODc2NDUxNTUsInNlc3Npb25fc3RhdGUiOiJlNDY3NmM2My0wZTliLTRmZWUtYjdjNi1jZGI0Zjg1ZjIyNWIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJuZXN0LWJhbmstYXBpIjp7InJvbGVzIjpbIkN1c3RvbWVyIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImNpZk5vIjoiOTk5MzIxMDAwMDAwMDAwMDAwMDA4MSIsInByZWZlcnJlZF91c2VybmFtZSI6IjA4MTIwMDAwMDAzNTEiLCJ1c2VySWQiOjkxLCJlbWFpbCI6IjA4MTIwMDAwMDAzNTEifQ.Z46KJoUiOUMfI5PaNDkrLiIP3WJY5rYOIsQLAXLXu7PyuwQPjae5y9N8a5OXqOacagcU6vXq3BuOrh_C56TpTyoSxFv8r4SghJ64c5k5LyMXZPFi0gWc2IYygz2Uuf96cEJ1W2kgaIrPowTLvaMF-8NnYWvVq9QXueMqf5JebwoEM3dukKAuC_ghmFNpQdv8FqKPk2lt8ip9vwRTMzZMaogHNPE2NOgpxbKxPyeNWkLk9SCNJxNvGZPCOId3EU8YJ54RO_4QAfkW5Ids_8GAjF0KKf18lxanb5OwVZampO6yaItk8Abz0s0W6Tshee3YSIj7tkWdfQM33ayrEYmY1A';
    final response = await http.get(serviceUrl, headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${token}'
    });

    final responseBody = jsonDecode(response.body);
    if (response.statusCode != 200) {
      throw Exception('Failed to get verification type: ' + responseBody['message']);
    }
    
    return responseBody['verificationType'];
  }
}