import 'package:bank_mobile_client/src/repositories/abstracts/account_repository.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/registration_repository.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/transaction_repository.dart';
import 'package:bank_mobile_client/src/repositories/abstracts/verification_repository.dart';
import 'package:bank_mobile_client/src/repositories/account_repository_impl.dart';
import 'package:bank_mobile_client/src/repositories/registration_repository_impl.dart';
import 'package:bank_mobile_client/src/repositories/transaction_repository_impl.dart';
import 'package:bank_mobile_client/src/repositories/verification_repository_temp.dart';
import 'package:get_it/get_it.dart';

final GetIt injector = GetIt.instance;

void setup() {
  injector.registerFactory<VerificationRepository>(() => VerificationRepositoryTemp());
  injector.registerFactory<AccountRepository>(() => AccountRepositoryImpl());
  injector.registerFactory<TransactionRepository>(() => TransactionRepositoryImpl());
  injector.registerFactory<RegistrationRepository>(() => RegistrationRepositoryImpl());
}