import 'package:meta/meta.dart';

class PinForm {
  final String username;
  final String verificationValue;

  PinForm({@required this.username, @required this.verificationValue});

  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'credentialType': 'EMAIL',
      'verificationValue': verificationValue
    };
  }
}