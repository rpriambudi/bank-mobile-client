class Token {
  final String username;
  final String email;
  final String cifNo;
  final int userId;

  Token({this.username, this.email, this.cifNo, this.userId});

  factory Token.fromJson(Map<String, dynamic> json) {
    return Token(
      username: json['preferred_username'],
      email: json['email'],
      cifNo: json['cifNo'],
      userId: json['userId']
    );
  }
}