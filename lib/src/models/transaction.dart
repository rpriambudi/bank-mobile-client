import 'package:meta/meta.dart';

class Transaction {
  final String requesterAcctNo;
  final String destinationAcctNo;
  final int amount;
  final String description;
  final String transactionStatus;
  final String transactionType;
  final String clientType = 'MB_CLIENT';

  Transaction({
    @required this.requesterAcctNo, 
    @required this.amount,
    this.destinationAcctNo,
    this.description,
    this.transactionStatus,
    this.transactionType
  });

  Map<String, dynamic> toJson() {
    return {
      'requesterAcctNo': requesterAcctNo,
      'destinationAcctNo': destinationAcctNo,
      'amount': amount,
      'description': description,
      'clientType': clientType
    };
  }

  factory Transaction.fromJson(Map<String, dynamic> json) {
    return Transaction(
      transactionType: json['transactionType'],
      transactionStatus: json['transactionStatus'],
      requesterAcctNo: json['requesterAcctNo'],
      destinationAcctNo: json['destinationAcctNo'],
      amount: int.parse(json['amount']),
      description: json['description'],
    );
  }
}