class Customer {
  String _name;
  String _address;
  String _birthDate;
  String _idType;
  String _idNumber;
  String _gender;
  String _motherMaidenName;

  Customer();

  String get name => _name;
  String get gender => _gender;
  String get address => _address;
  String get birthDate => _birthDate;
  String get idType => _idType;
  String get idNumber => _idNumber;
  String get motherMaidenName => _motherMaidenName;

  set setName(String name) {
    _name = name;
  }

  set setGender(String gender) {
    _gender = gender;
  }

  set setAddress(String address) {
    _address = address;
  }

  set setBirthDate(String birthDate) {
    _birthDate = birthDate;
  }

  set setIdType(String idType) {
    _idType = idType;
  }

  set setIdNumber(String idNumber) {
    _idNumber = idNumber;
  }

  set setMotherMaidenName(String motherMaidenName) {
    _motherMaidenName = motherMaidenName;
  }

  Map<String, dynamic> toJson() {
    return {
      'name': _name,
      'gender': _gender,
      'birthDate': _birthDate,
      'motherMaidenName': _motherMaidenName,
      'idType': _idType,
      'idNumber': _idNumber,
      'address': _address
    };
  }
}