class Account {
  String accountNo;
  int balance;

  Account({this.accountNo, this.balance});


  factory Account.fromJson(Map<String, dynamic> json) {
    return Account(accountNo: json['accountNo'], balance: int.parse(json['balance']));
  }
}