class Saga {
  final int id;
  final String state;
  final String description;

  Saga({this.id, this.state, this.description});

  factory Saga.fromJson(Map<String, dynamic> json) {
    return Saga(
      id: json['id'],
      state: json['state'],
      description: json['description']
    );
  }
}