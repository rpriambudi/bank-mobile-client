class User {
  final String _clientType = 'IB_CLIENT';
  final String _credentialType = 'EMAIL';
  String _email;
  String _password;

  Registration(
    String email,
    String password
  ) {
    _email = email;
    _password = password;
  }

  String get email => _email;
  String get password => _password;
  String get clientType => _clientType;
  String get credentialType => _credentialType;
  
  set setEmail(String email) {
    _email = email;
  }

  set setPassword(String password) {
    _password = password;
  }

  Map<String, dynamic> toJson() {
    return {
      'email': _email,
      'password': _password,
      'clientType': _clientType,
      'credentialType': _credentialType
    };
  }
}