import 'package:bank_mobile_client/src/models/customer.dart';
import 'package:bank_mobile_client/src/models/user.dart';

class Registration {
  final User user;
  final Customer customer;

  Registration({ this.user, this.customer });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> userJson = user.toJson();
    Map<String, dynamic> customerJson = customer.toJson();

    return {
      'user': userJson,
      'customer': customerJson
    };
  }
}