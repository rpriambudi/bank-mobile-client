import 'dart:convert';

import 'package:bank_mobile_client/src/models/token.dart';
import 'package:bank_mobile_client/src/shared/constants.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:oauth2/oauth2.dart' as oauth2;

class AuthProvider {
  static final AuthProvider _authProvider = AuthProvider._internal();
  final Constants _constants = Constants();
  oauth2.Client _client;
  oauth2.AuthorizationCodeGrant _grant;
  String _authorizationUrl;

  factory AuthProvider() {
    return _authProvider;
  }


  void initializeGrant() {
    final grant = new oauth2.AuthorizationCodeGrant(
      _constants.authClientId, 
      _constants.authEndpointUrl, 
      _constants.authTokenUrl,
      secret: _constants.authClientSecret
    );
    _grant = grant;
    _client = null;
  }

  oauth2.Client getClient() {
    return _client;
  }

  Future<void> setClientFromCode(String code) async {
    _client = await _grant.handleAuthorizationCode(code);
  }

  String getAuthorizationUrl() {
    _authorizationUrl = _grant.getAuthorizationUrl(_constants.authCallbackUrl).toString();
    return _authorizationUrl;
  }

  Future<Token> getTokenData() async {
    if (_client == null) {
      return null;
    }

    if (_client.credentials.isExpired) {
      await _client.refreshCredentials();
    }

    final parts = _client.credentials.accessToken.split('.');
    if (parts.length != 3)
      return null;


    final payload = parts[1];
    var normalized = base64Url.normalize(payload);
    normalized = utf8.decode(base64Url.decode(normalized));

    return Token.fromJson(jsonDecode(normalized));
  }

  signOff() async {
    final webview = FlutterWebviewPlugin();
    String logoutUri = _constants.authLogoutUri.toString();
    logoutUri += '?clientId=' + _constants.authClientId;
    logoutUri += '&redirect_uri=' + _constants.authLogoutCallbackUrl.toString();

    webview.launch(logoutUri);
    await for (String url in webview.onUrlChanged) {
      if (url.contains(_constants.authLogoutCallbackUrl.toString())) {
        webview.close();
        close();
      }
    }
  }

  close() {
    _client.close();
  }

  AuthProvider._internal();
}